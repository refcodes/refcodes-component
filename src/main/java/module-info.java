module org.refcodes.component {
	requires org.refcodes.data;
	requires transitive org.refcodes.controlflow;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.generator;
	requires transitive org.refcodes.mixin;

	exports org.refcodes.component;
}
