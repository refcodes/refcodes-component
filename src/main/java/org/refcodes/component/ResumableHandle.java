package org.refcodes.component;

import org.refcodes.component.Resumable.ResumeAutomaton;
import org.refcodes.component.ResumeException.ResumeRuntimeException;

/**
 * The {@link ResumableHandle} interface defines those methods related to the
 * handle based resume life-cycle.
 * <p>
 * The handle reference requires the {@link Resumable} interface to be
 * implemented.
 *
 * @param <H> The type of the handle.
 */
public interface ResumableHandle<H> {

	/**
	 * Determines whether the handle reference is resumable by implementing the
	 * {@link Resumable} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException in case the handle is unknown.
	 */
	boolean hasResumable( H aHandle );

	/**
	 * Resumes the component identified by the given handle.
	 *
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws ResumeException in case resuming fails.
	 * @throws UnknownHandleRuntimeException in case the given handle is
	 *         unknown.
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 * @throws IllegalHandleStateChangeRuntimeException Thrown in case a state
	 *         change is not possible due to the current state the referenced
	 *         component is in.
	 */
	void resume( H aHandle ) throws ResumeException;

	/**
	 * Resumes the component by calling {@link #resume(Object)} without you to
	 * require catching an {@link ResumeException}.
	 * 
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws ResumeRuntimeException encapsulates the aCause and is thrown upon
	 *         encountering a {@link ResumeException} exception
	 */
	default void resumeUnchecked( H aHandle ) {
		try {
			resume( aHandle );
		}
		catch ( ResumeException e ) {
			throw new ResumeRuntimeException( e );
		}
	}

	/**
	 * The {@link ResumeAutomatonHandle} interface defines those methods related
	 * to the handle based resume life-cycle. The handle reference requires the
	 * {@link ResumeAutomaton} interface to be implemented.
	 * 
	 * @param <H> The type of the handle.
	 */
	public interface ResumeAutomatonHandle<H> extends RunningHandle<H>, ResumableHandle<H> {

		/**
		 * Determines whether the handle reference is resumable by implementing
		 * the {@link ResumeAutomaton} interface.
		 * 
		 * @param aHandle The handle to test whether the reference provides the
		 *        according functionality.
		 * 
		 * @return True in case the reference provides the according
		 *         functionality.
		 * 
		 * @throws UnknownHandleRuntimeException in case the handle is unknown.
		 */
		boolean hasResumeAutomaton( H aHandle );

		/**
		 * Determines whether the component identified by the given handle may
		 * get resumed.
		 *
		 * @param aHandle The handle identifying the component.
		 * 
		 * @return True if {@link #resume(Object)} is possible.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isResumable( H aHandle );
	}
}
