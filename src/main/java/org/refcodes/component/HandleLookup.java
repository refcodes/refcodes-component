package org.refcodes.component;

/**
 * Any system in this framework being able to work with handles provides an
 * implementation of the handle lookup interface. This enables a unified access
 * to the system wide handles.
 *
 * @param <H> The type of the handles.
 * @param <REF> The type of the objects to which the handles reference to.
 */
public interface HandleLookup<H, REF> {

	/**
	 * Used to test if the given handle is known by an implementing system.
	 * 
	 * @param aHandle The handle for which to find out if there is any related
	 *        data.
	 * 
	 * @return True if the handle is known.
	 */
	boolean hasHandle( H aHandle );

	/**
	 * Gets the object associated to a given handle.
	 *
	 * @param aHandle The handle for which to get the related data.
	 * 
	 * @return The object associated to the given handle.
	 * 
	 * @throws UnknownHandleRuntimeException the unknown handle runtime
	 *         exception
	 */
	REF lookupHandle( H aHandle );

	/**
	 * Gets the descriptor for a given handle.
	 *
	 * @param aHandle The handle for which to get the descriptor.
	 * 
	 * @return The descriptor for the given handle.
	 * 
	 * @throws UnknownHandleRuntimeException the unknown handle runtime
	 *         exception
	 */
	REF removeHandle( H aHandle );

}
