// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * Provides an accessor for a running property.
 */
public interface RunningAccessor {

	/**
	 * Retrieves the is-running property from the running property. Determines
	 * whether the component is running. A component is running after being
	 * started or being resumed as of {@link Startable#start()} or
	 * {@link Resumable#resume()}.
	 * 
	 * @return True in case of being resumed or started (returns the is-running
	 *         property stored by the running property).
	 */
	boolean isRunning();

	/**
	 * Provides a mutator for a running property.
	 */
	public interface RunningMutator {

		/**
		 * Sets the is-running property for the running property.
		 * 
		 * @param isRunning The running property to be stored by the running
		 *        property.
		 */
		void setRunning( boolean isRunning );
	}

	/**
	 * Provides a running property.
	 */
	public interface RunningProperty extends RunningAccessor, RunningMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setRunning(boolean)} and returns the very same value
		 * (getter).
		 * 
		 * @param aRunning The boolean to set (via
		 *        {@link #setRunning(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letRunning( boolean aRunning ) {
			setRunning( aRunning );
			return aRunning;
		}
	}
}
