// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import java.io.Flushable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.refcodes.component.CloseException.CloseRuntimeException;
import org.refcodes.component.DecomposeException.DecomposeRuntimeException;
import org.refcodes.component.DestroyException.DestroyRuntimeException;
import org.refcodes.component.DisposeException.DisposeRuntimeException;
import org.refcodes.component.ResetException.ResetRuntimeException;
import org.refcodes.controlflow.ControlFlowUtility;
import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.mixin.Disposable;
import org.refcodes.mixin.Resetable;

/**
 * The Class ComponentUtility.
 */
public final class ComponentUtility {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final CallableFactory<Initializable> _initializeCallableFactoryImpl = new CallableFactory<Initializable>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Callable<Void> toCallable( Initializable aComponent, Object... aArguments ) {
			return new InitializeCallable( aComponent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<Initializable> getType() {
			return Initializable.class;
		}
	};

	@SuppressWarnings("rawtypes")
	private static final CallableFactory<Configurable> _configureCallableFactoryImpl = new CallableFactory<Configurable>() {

		@SuppressWarnings({ "unchecked" })
		@Override
		public Callable<Void> toCallable( Configurable aComponent, Object... aArguments ) {
			return new ConfigureCallable( aComponent, aArguments[0] );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<Configurable> getType() {
			return Configurable.class;
		}
	};

	private static final CallableFactory<Startable> _startCallableFactoryImpl = new CallableFactory<Startable>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Callable<Void> toCallable( Startable aComponent, Object... aArguments ) {
			return new StartCallable( aComponent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<Startable> getType() {
			return Startable.class;
		}
	};

	private static final CallableFactory<Pausable> _pauseCallableFactoryImpl = new CallableFactory<Pausable>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Callable<Void> toCallable( Pausable aComponent, Object... aArguments ) {
			return new PauseCallable( aComponent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<Pausable> getType() {
			return Pausable.class;
		}
	};

	private static final CallableFactory<Resumable> _resumeCallableFactoryImpl = new CallableFactory<Resumable>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Callable<Void> toCallable( Resumable aComponent, Object... aArguments ) {
			return new ResumeCallable( aComponent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<Resumable> getType() {
			return Resumable.class;
		}
	};

	private static final CallableFactory<Stoppable> _stopCallableFactoryImpl = new CallableFactory<Stoppable>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Callable<Void> toCallable( Stoppable aComponent, Object... aArguments ) {
			return new StopCallable( aComponent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<Stoppable> getType() {
			return Stoppable.class;
		}
	};

	private static final CallableFactory<Destroyable> _destroyCallableFactoryImpl = new CallableFactory<Destroyable>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Callable<Void> toCallable( Destroyable aComponent, Object... aArguments ) {
			return new DestroyCallable( aComponent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<Destroyable> getType() {
			return Destroyable.class;
		}
	};

	private static final CallableFactory<Decomposable> _decomposeCallableFactoryImpl = new CallableFactory<Decomposable>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Callable<Void> toCallable( Decomposable aComponent, Object... aArguments ) {
			return new DecomposeCallable( aComponent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<Decomposable> getType() {
			return Decomposable.class;
		}
	};

	private static final CallableFactory<Disposable> _disposeCallableFactoryImpl = new CallableFactory<Disposable>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Callable<Void> toCallable( Disposable aComponent, Object... aArguments ) {
			return new DisposeCallable( aComponent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<Disposable> getType() {
			return Disposable.class;
		}
	};

	private static final CallableFactory<Openable> _openCallableFactoryImpl = new CallableFactory<Openable>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Callable<Void> toCallable( Openable aComponent, Object... aArguments ) {
			return new OpenCallable( aComponent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<Openable> getType() {
			return Openable.class;
		}
	};

	@SuppressWarnings("rawtypes")
	private static final CallableFactory<ConnectionOpenable> _connectCallableFactoryImpl = new CallableFactory<ConnectionOpenable>() {

		@SuppressWarnings("unchecked")
		@Override
		public Callable<Void> toCallable( ConnectionOpenable aComponent, Object... aArguments ) {
			return new ConnectCallable( aComponent, aArguments[0] );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<ConnectionOpenable> getType() {
			return ConnectionOpenable.class;
		}
	};

	@SuppressWarnings("rawtypes")
	private static final CallableFactory<BidirectionalConnectionOpenable> _ioConnectCallableFactoryImpl = new CallableFactory<BidirectionalConnectionOpenable>() {

		@SuppressWarnings("unchecked")
		@Override
		public Callable<Void> toCallable( BidirectionalConnectionOpenable aComponent, Object... aArguments ) {
			return new IoConnectCallable( aComponent, aArguments[0], aArguments[1] );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<BidirectionalConnectionOpenable> getType() {
			return BidirectionalConnectionOpenable.class;
		}
	};

	private static final CallableFactory<Closable> _closeCallableFactoryImpl = new CallableFactory<Closable>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Callable<Void> toCallable( Closable aComponent, Object... aArguments ) {
			return new CloseCallable( aComponent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<Closable> getType() {
			return Closable.class;
		}
	};

	private static final CallableFactory<Flushable> _flushCallableFactoryImpl = new CallableFactory<Flushable>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Callable<Void> toCallable( Flushable aComponent, Object... aArguments ) {
			return new FlushCallable( aComponent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<Flushable> getType() {
			return Flushable.class;
		}
	};

	private static final CallableFactory<Resetable> _resetCallableFactoryImpl = new CallableFactory<Resetable>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Callable<Void> toCallable( Resetable aComponent, Object... aArguments ) {
			return new ResetCallable( aComponent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Class<Resetable> getType() {
			return Resetable.class;
		}
	};

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Private empty constructor to prevent instantiation as of being a utility
	 * with just static public methods.
	 */
	private ComponentUtility() {}

	// /////////////////////////////////////////////////////////////////////////
	// INITIALIZE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, initializes the given {@link Component} in case it is
	 * {@link Initializable}.
	 * 
	 * @param aComponent The {@link Component} to initialize.
	 * 
	 * @throws InitializeException Thrown in case initialization failed.
	 */
	public static void initialize( Object aComponent ) throws InitializeException {
		if ( aComponent instanceof Initializable ) {
			( (Initializable) aComponent ).initialize();
		}
	}

	/**
	 * Helper method for initializing all {@link Initializable}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when initializing
	 *        the {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to initialize (in case them implement
	 *        the {@link Initializable} interface).
	 * 
	 * @throws InitializeException in case initialization of at least one
	 *         {@link Initializable} instance failed.
	 */
	public static void initialize( ExecutionStrategy aComponentExecutionStrategy, Collection<?> aComponents ) throws InitializeException {
		initialize( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aComponents );
	}

	/**
	 * Helper method for initializing all {@link Initializable}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}. An
	 * {@link ExecutorService} can be provided in case some EJB container is to
	 * use a managed {@link ExecutorService} provided by an EJB server.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when initializing
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws InitializeException in case initialization of at least one
	 *         {@link Initializable} instance failed.
	 */
	public static void initialize( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Collection<?> aComponents ) throws InitializeException {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _initializeCallableFactoryImpl, aComponents, (Object[]) null );
		}
		catch ( InitializeException e ) {
			throw e;
		}
		catch ( Exception e ) {
			throw new InitializeException( e );
		}
	}

	/**
	 * Helper method for initializing all {@link Initializable}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when initializing
	 *        the {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to initialize (in case them implement
	 *        the {@link Initializable} interface).
	 * 
	 * @throws InitializeException in case initialization of at least one
	 *         {@link Initializable} instance failed.
	 */
	@SafeVarargs
	public static void initialize( ExecutionStrategy aComponentExecutionStrategy, Object... aComponents ) throws InitializeException {
		initialize( aComponentExecutionStrategy, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for initializing all {@link Initializable}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}. An
	 * {@link ExecutorService} can be provided in case some EJB container is to
	 * use a managed {@link ExecutorService} provided by an EJB server.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when initializing
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws InitializeException in case initialization of at least one
	 *         {@link Initializable} instance failed.
	 */
	@SafeVarargs
	public static void initialize( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Object... aComponents ) throws InitializeException {
		initialize( aComponentExecutionStrategy, aExecutorService, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONFIGURE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, configuring the given {@link Component} in case it is
	 * {@link Configurable}.
	 *
	 * @param <CTX> the generic type
	 * @param aComponent The {@link Component} to configure.
	 * @param aContext the context
	 * 
	 * @throws ConfigureException the configure exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <CTX> void initialize( Object aComponent, CTX aContext ) throws InitializeException {
		if ( aComponent instanceof Configurable<?> ) {
			( (Configurable) aComponent ).initialize( aContext );
		}
	}

	/**
	 * Helper method for configuring all {@link Configurable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 *
	 * @param <CTX> the generic type
	 * @param aComponentExecutionStrategy The strategy to use when initializing
	 *        the {@link Component} instances.
	 * @param aContext the context
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to initialize (in case them implement
	 *        the {@link Configurable} interface).
	 * 
	 * @throws ConfigureException the configure exception
	 */
	public static <CTX> void initialize( ExecutionStrategy aComponentExecutionStrategy, CTX aContext, Collection<?> aComponents ) throws ConfigureException {
		initialize( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aContext, aComponents );
	}

	/**
	 * Helper method for configuring all {@link Configurable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 *
	 * @param <CTX> the generic type
	 * @param aComponentExecutionStrategy The strategy to use when initializing
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aContext the context
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws ConfigureException the configure exception
	 */
	public static <CTX> void initialize( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, CTX aContext, Collection<?> aComponents ) throws ConfigureException {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _configureCallableFactoryImpl, aComponents, aContext );
		}
		catch ( ConfigureException e ) {
			throw e;
		}
		catch ( Exception e ) {
			throw new ConfigureException( aContext, e );
		}
	}

	/**
	 * Helper method for configuring all {@link ConfigureException}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}.
	 *
	 * @param <CTX> the generic type
	 * @param aComponentExecutionStrategy The strategy to use when initializing
	 *        the {@link Component} instances.
	 * @param aContext the context
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to initialize (in case them implement
	 *        the {@link ConfigureException} interface).
	 * 
	 * @throws ConfigureException the configure exception
	 */
	@SafeVarargs
	public static <CTX> void initialize( ExecutionStrategy aComponentExecutionStrategy, CTX aContext, Object... aComponents ) throws ConfigureException {
		initialize( aComponentExecutionStrategy, aContext, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for configuring all {@link ConfigureException}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}. An
	 * {@link ExecutorService} can be provided in case some EJB container is to
	 * use a managed {@link ExecutorService} provided by an EJB server.
	 *
	 * @param <CTX> the generic type
	 * @param aComponentExecutionStrategy The strategy to use when initializing
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aContext the context
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws ConfigureException the configure exception
	 */
	@SafeVarargs
	public static <CTX> void initialize( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, CTX aContext, Object... aComponents ) throws ConfigureException {
		initialize( aComponentExecutionStrategy, aExecutorService, aContext, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// START:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, starts the given {@link Component} in case it is
	 * {@link Startable}.
	 * 
	 * @param aComponent The {@link Component} to start.
	 * 
	 * @throws StartException Thrown in case initialization failed.
	 */
	public static void start( Object aComponent ) throws StartException {
		if ( aComponent instanceof Startable ) {
			( (Startable) aComponent ).start();
		}
	}

	/**
	 * Helper method for starting all {@link Startable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when starting the
	 *        {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to start (in case them implement the
	 *        {@link Startable} interface).
	 * 
	 * @throws StartException in case initialization of at least one
	 *         {@link Startable} instance failed.
	 */
	public static void start( ExecutionStrategy aComponentExecutionStrategy, Collection<?> aComponents ) throws StartException {
		start( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aComponents );
	}

	/**
	 * Helper method for starting all {@link Startable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when starting the
	 *        {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws StartException in case initialization of at least one
	 *         {@link Startable} instance failed.
	 */
	public static void start( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Collection<?> aComponents ) throws StartException {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _startCallableFactoryImpl, aComponents, (Object[]) null );
		}
		catch ( StartException e ) {
			throw e;
		}
		catch ( Exception e ) {
			throw new StartException( e );
		}
	}

	/**
	 * Helper method for starting all {@link Startable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when starting the
	 *        {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to start (in case them implement the
	 *        {@link Startable} interface).
	 * 
	 * @throws StartException in case initialization of at least one
	 *         {@link Startable} instance failed.
	 */
	@SafeVarargs
	public static void start( ExecutionStrategy aComponentExecutionStrategy, Object... aComponents ) throws StartException {
		start( aComponentExecutionStrategy, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for starting all {@link Startable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when starting the
	 *        {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws StartException in case initialization of at least one
	 *         {@link Startable} instance failed.
	 */
	@SafeVarargs
	public static void start( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Object... aComponents ) throws StartException {
		start( aComponentExecutionStrategy, aExecutorService, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// PAUSE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, pauses the given {@link Component} in case it is
	 * {@link Pausable}.
	 * 
	 * @param aComponent The {@link Component} to pause.
	 * 
	 * @throws PauseException Thrown in case initialization failed.
	 */
	public static void pause( Object aComponent ) throws PauseException {
		if ( aComponent instanceof Pausable ) {
			( (Pausable) aComponent ).pause();
		}
	}

	/**
	 * Helper method for pausing all {@link Pausable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when pausing the
	 *        {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to pause (in case them implement the
	 *        {@link Pausable} interface).
	 * 
	 * @throws PauseException in case initialization of at least one
	 *         {@link Pausable} instance failed.
	 */
	public static void pause( ExecutionStrategy aComponentExecutionStrategy, Collection<?> aComponents ) throws PauseException {
		pause( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aComponents );
	}

	/**
	 * Helper method for pausing all {@link Pausable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when pausing the
	 *        {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws PauseException in case initialization of at least one
	 *         {@link Pausable} instance failed.
	 */
	public static void pause( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Collection<?> aComponents ) throws PauseException {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _pauseCallableFactoryImpl, aComponents, (Object[]) null );
		}
		catch ( PauseException e ) {
			throw e;
		}
		catch ( Exception e ) {
			throw new PauseException( e );
		}
	}

	/**
	 * Helper method for pausing all {@link Pausable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when pausing the
	 *        {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to pause (in case them implement the
	 *        {@link Pausable} interface).
	 * 
	 * @throws PauseException in case initialization of at least one
	 *         {@link Pausable} instance failed.
	 */
	@SafeVarargs
	public static void pause( ExecutionStrategy aComponentExecutionStrategy, Object... aComponents ) throws PauseException {
		pause( aComponentExecutionStrategy, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for pausing all {@link Pausable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when pausing the
	 *        {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws PauseException in case initialization of at least one
	 *         {@link Pausable} instance failed.
	 */
	@SafeVarargs
	public static void pause( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Object... aComponents ) throws PauseException {
		pause( aComponentExecutionStrategy, aExecutorService, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// RESUME:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, resumes the given {@link Component} in case it is
	 * {@link Resumable}.
	 * 
	 * @param aComponent The {@link Component} to resume.
	 * 
	 * @throws ResumeException Thrown in case initialization failed.
	 */
	public static void resume( Object aComponent ) throws ResumeException {
		if ( aComponent instanceof Resumable ) {
			( (Resumable) aComponent ).resume();
		}
	}

	/**
	 * Helper method for resuming all {@link Resumable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when resuming the
	 *        {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to resume (in case them implement the
	 *        {@link Resumable} interface).
	 * 
	 * @throws ResumeException in case initialization of at least one
	 *         {@link Resumable} instance failed.
	 */
	public static void resume( ExecutionStrategy aComponentExecutionStrategy, Collection<?> aComponents ) throws ResumeException {
		resume( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aComponents );
	}

	/**
	 * Helper method for resuming all {@link Resumable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when resuming the
	 *        {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws ResumeException in case initialization of at least one
	 *         {@link Resumable} instance failed.
	 */
	public static void resume( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Collection<?> aComponents ) throws ResumeException {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _resumeCallableFactoryImpl, aComponents, (Object[]) null );
		}
		catch ( ResumeException e ) {
			throw e;
		}
		catch ( Exception e ) {
			throw new ResumeException( e );
		}
	}

	/**
	 * Helper method for resuming all {@link Resumable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when resuming the
	 *        {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to resume (in case them implement the
	 *        {@link Resumable} interface).
	 * 
	 * @throws ResumeException in case initialization of at least one
	 *         {@link Resumable} instance failed.
	 */
	@SafeVarargs
	public static void resume( ExecutionStrategy aComponentExecutionStrategy, Object... aComponents ) throws ResumeException {
		resume( aComponentExecutionStrategy, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for resuming all {@link Resumable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when resuming the
	 *        {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws ResumeException in case initialization of at least one
	 *         {@link Resumable} instance failed.
	 */
	@SafeVarargs
	public static void resume( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Object... aComponents ) throws ResumeException {
		resume( aComponentExecutionStrategy, aExecutorService, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// STOP:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, stops the given {@link Component} in case it is
	 * {@link Stoppable}.
	 * 
	 * @param aComponent The {@link Component} to stop.
	 * 
	 * @throws StopException Thrown in case initialization failed.
	 */
	public static void stop( Object aComponent ) throws StopException {
		if ( aComponent instanceof Stoppable ) {
			( (Stoppable) aComponent ).stop();
		}
	}

	/**
	 * Helper method for stopping all {@link Stoppable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when stoping the
	 *        {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to stop (in case them implement the
	 *        {@link Stoppable} interface).
	 * 
	 * @throws StopException in case initialization of at least one
	 *         {@link Stoppable} instance failed.
	 */
	public static void stop( ExecutionStrategy aComponentExecutionStrategy, Collection<?> aComponents ) throws StopException {
		stop( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aComponents );
	}

	/**
	 * Helper method for stopping all {@link Stoppable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when stoping the
	 *        {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws StopException in case initialization of at least one
	 *         {@link Stoppable} instance failed.
	 */
	public static void stop( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Collection<?> aComponents ) throws StopException {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _stopCallableFactoryImpl, aComponents, (Object[]) null );
		}
		catch ( StopException e ) {
			throw e;
		}
		catch ( Exception e ) {
			throw new StopException( e );
		}
	}

	/**
	 * Helper method for stoping all {@link Stoppable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when stoping the
	 *        {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to stop (in case them implement the
	 *        {@link Stoppable} interface).
	 * 
	 * @throws StopException in case initialization of at least one
	 *         {@link Stoppable} instance failed.
	 */
	@SafeVarargs
	public static void stop( ExecutionStrategy aComponentExecutionStrategy, Object... aComponents ) throws StopException {
		stop( aComponentExecutionStrategy, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for stoping all {@link Stoppable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when stoping the
	 *        {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws StopException in case initialization of at least one
	 *         {@link Stoppable} instance failed.
	 */
	@SafeVarargs
	public static void stop( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Object... aComponents ) throws StopException {
		stop( aComponentExecutionStrategy, aExecutorService, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// DESTROY:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, destroys the given {@link Component} in case it is
	 * {@link Destroyable}.
	 *
	 * @param aComponent The {@link Component} to destroy.
	 */
	public static void destroy( Object aComponent ) {
		if ( aComponent instanceof Destroyable ) {
			( (Destroyable) aComponent ).destroy();
		}
	}

	/**
	 * Helper method for destroying all {@link Destroyable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when destroying
	 *        the {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to destroy (in case them implement the
	 *        {@link Destroyable} interface).
	 */
	public static void destroy( ExecutionStrategy aComponentExecutionStrategy, Collection<?> aComponents ) {
		destroy( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aComponents );
	}

	/**
	 * Helper method for destroying all {@link Destroyable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when destroying
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 */
	public static void destroy( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Collection<?> aComponents ) {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _destroyCallableFactoryImpl, aComponents, (Object[]) null );
		}
		catch ( Exception e ) {
			throw new DestroyRuntimeException( e );
		}
	}

	/**
	 * Helper method for destroying all {@link Destroyable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when destroying
	 *        the {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to destroy (in case them implement the
	 *        {@link Destroyable} interface).
	 */
	@SafeVarargs
	public static void destroy( ExecutionStrategy aComponentExecutionStrategy, Object... aComponents ) {
		destroy( aComponentExecutionStrategy, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for destroying all {@link Destroyable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when destroying
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 */
	@SafeVarargs
	public static void destroy( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Object... aComponents ) {
		destroy( aComponentExecutionStrategy, aExecutorService, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// DECOMPOSE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, decomposes the given {@link Component} in case it is
	 * {@link Decomposable}.
	 *
	 * @param aComponent The {@link Component} to decompose.
	 */
	public static void decompose( Object aComponent ) {
		if ( aComponent instanceof Decomposable ) {
			( (Decomposable) aComponent ).decompose();
		}
	}

	/**
	 * Helper method for decomposing all {@link Decomposable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to decompose (in case them implement
	 *        the {@link Decomposable} interface).
	 */
	public static void decompose( ExecutionStrategy aComponentExecutionStrategy, Collection<?> aComponents ) {
		decompose( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aComponents );
	}

	/**
	 * Helper method for decomposing all {@link Decomposable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 */
	public static void decompose( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Collection<?> aComponents ) {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _decomposeCallableFactoryImpl, aComponents, (Object[]) null );
		}
		catch ( Exception e ) {
			throw new DecomposeRuntimeException( e );
		}
	}

	/**
	 * Helper method for decomposing all {@link Decomposable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to decompose (in case them implement
	 *        the {@link Decomposable} interface).
	 */
	@SafeVarargs
	public static void decompose( ExecutionStrategy aComponentExecutionStrategy, Object... aComponents ) {
		decompose( aComponentExecutionStrategy, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for decomposing all {@link Decomposable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 */
	@SafeVarargs
	public static void decompose( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Object... aComponents ) {
		decompose( aComponentExecutionStrategy, aExecutorService, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// DISPOSE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, disposes the given {@link Component} in case it is
	 * {@link Disposable}.
	 *
	 * @param aComponent The {@link Component} to dispose.
	 */
	public static void dispose( Object aComponent ) {
		if ( aComponent instanceof Disposable ) {
			( (Disposable) aComponent ).dispose();
		}
	}

	/**
	 * Helper method for decomposing all {@link Disposable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to dispose (in case them implement the
	 *        {@link Disposable} interface).
	 */
	public static void dispose( ExecutionStrategy aComponentExecutionStrategy, Collection<?> aComponents ) {
		dispose( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aComponents );
	}

	/**
	 * Helper method for decomposing all {@link Disposable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 */
	public static void dispose( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Collection<?> aComponents ) {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _disposeCallableFactoryImpl, aComponents, (Object[]) null );
		}
		catch ( Exception e ) {
			throw new DisposeRuntimeException( e );
		}
	}

	/**
	 * Helper method for decomposing all {@link Disposable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to dispose (in case them implement the
	 *        {@link Disposable} interface).
	 */
	@SafeVarargs
	public static void dispose( ExecutionStrategy aComponentExecutionStrategy, Object... aComponents ) {
		dispose( aComponentExecutionStrategy, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for decomposing all {@link Disposable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 */
	@SafeVarargs
	public static void dispose( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Object... aComponents ) {
		dispose( aComponentExecutionStrategy, aExecutorService, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// FLUSH:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, flushs the given {@link Component} in case it is
	 * {@link Flushable}.
	 * 
	 * @param aComponent The {@link Component} to flush.
	 * 
	 * @throws IOException Thrown in case initialization failed.
	 */
	public static void flush( Object aComponent ) throws IOException {
		if ( aComponent instanceof Flushable ) {
			try {
				( (Flushable) aComponent ).flush();
			}
			catch ( IOException e ) {
				throw new IOException( "Unable to flush component <" + aComponent + ">.", e );
			}
		}
	}

	/**
	 * Helper method for flushing all {@link Flushable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when flushing the
	 *        {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to flush (in case them implement the
	 *        {@link Flushable} interface).
	 * 
	 * @throws IOException in case initialization of at least one
	 *         {@link Flushable} instance failed.
	 */
	public static void flush( ExecutionStrategy aComponentExecutionStrategy, Collection<?> aComponents ) throws IOException {
		flush( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aComponents );
	}

	/**
	 * Helper method for flushing all {@link Flushable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when flushing the
	 *        {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws IOException in case initialization of at least one
	 *         {@link Flushable} instance failed.
	 */
	public static void flush( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Collection<?> aComponents ) throws IOException {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _flushCallableFactoryImpl, aComponents, (Object[]) null );
		}
		catch ( Exception e ) {
			throw new IOException( "Unable to flush component(s).", e );
		}
	}

	/**
	 * Helper method for flushing all {@link Flushable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when flushing the
	 *        {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to flush (in case them implement the
	 *        {@link Flushable} interface).
	 * 
	 * @throws IOException in case initialization of at least one
	 *         {@link Flushable} instance failed.
	 */
	@SafeVarargs
	public static void flush( ExecutionStrategy aComponentExecutionStrategy, Object... aComponents ) throws IOException {
		flush( aComponentExecutionStrategy, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for flushing all {@link Flushable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when flushing the
	 *        {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws IOException in case initialization of at least one
	 *         {@link Flushable} instance failed.
	 */
	@SafeVarargs
	public static void flush( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Object... aComponents ) throws IOException {
		flush( aComponentExecutionStrategy, aExecutorService, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// RESET:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, resets the given {@link Component} in case it is
	 * {@link Resetable}.
	 *
	 * @param aComponent The {@link Component} to reset.
	 */
	public static void reset( Object aComponent ) {
		if ( aComponent instanceof Resetable ) {
			( (Resetable) aComponent ).reset();
		}
	}

	/**
	 * Helper method for decomposing all {@link Resetable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to reset (in case them implement the
	 *        {@link Resetable} interface).
	 */
	public static void reset( ExecutionStrategy aComponentExecutionStrategy, Collection<?> aComponents ) {
		reset( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aComponents );
	}

	/**
	 * Helper method for decomposing all {@link Resetable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 */
	public static void reset( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Collection<?> aComponents ) {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _resetCallableFactoryImpl, aComponents, (Object[]) null );
		}
		catch ( Exception e ) {
			throw new ResetRuntimeException( e );
		}
	}

	/**
	 * Helper method for decomposing all {@link Resetable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to reset (in case them implement the
	 *        {@link Resetable} interface).
	 */
	@SafeVarargs
	public static void reset( ExecutionStrategy aComponentExecutionStrategy, Object... aComponents ) {
		reset( aComponentExecutionStrategy, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for decomposing all {@link Resetable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 */
	@SafeVarargs
	public static void reset( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Object... aComponents ) {
		reset( aComponentExecutionStrategy, aExecutorService, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// OPEN:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, opens the given {@link Component} in case it is
	 * {@link Openable}.
	 * 
	 * @param aComponent The {@link Component} to open.
	 * 
	 * @throws IOException Thrown in case initialization failed.
	 */
	public static void open( Object aComponent ) throws IOException {
		if ( aComponent instanceof Openable ) {
			( (Openable) aComponent ).open();
		}
	}

	/**
	 * Helper method for opening all {@link Openable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when opening the
	 *        {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to open (in case them implement the
	 *        {@link Openable} interface).
	 * 
	 * @throws IOException in case initialization of at least one
	 *         {@link Openable} instance failed.
	 */
	public static void open( ExecutionStrategy aComponentExecutionStrategy, Collection<?> aComponents ) throws IOException {
		open( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aComponents );
	}

	/**
	 * Helper method for opening all {@link Openable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when opening the
	 *        {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws IOException in case initialization of at least one
	 *         {@link Openable} instance failed.
	 */
	public static void open( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Collection<?> aComponents ) throws IOException {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _openCallableFactoryImpl, aComponents, (Object[]) null );
		}
		catch ( IOException e ) {
			throw e;
		}
		catch ( Exception e ) {
			throw new IOException( e );
		}
	}

	/**
	 * Helper method for opening all {@link Openable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when opening the
	 *        {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to open (in case them implement the
	 *        {@link Openable} interface).
	 * 
	 * @throws IOException in case initialization of at least one
	 *         {@link Openable} instance failed.
	 */
	@SafeVarargs
	public static void open( ExecutionStrategy aComponentExecutionStrategy, Object... aComponents ) throws IOException {
		open( aComponentExecutionStrategy, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for opening all {@link Openable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 * 
	 * @param aComponentExecutionStrategy The strategy to use when opening the
	 *        {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws IOException in case initialization of at least one
	 *         {@link Openable} instance failed.
	 */
	@SafeVarargs
	public static void open( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Object... aComponents ) throws IOException {
		open( aComponentExecutionStrategy, aExecutorService, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONNECT:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, configuring the given {@link Component} in case it is
	 * {@link ConnectionOpenable}.
	 *
	 * @param <CON> the generic type
	 * @param aComponent The {@link Component} to configure.
	 * @param aConnection the connection
	 * 
	 * @throws IOException Thrown in case connecting failed.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <CON> void open( Object aComponent, CON aConnection ) throws IOException {
		if ( aComponent instanceof ConnectionOpenable<?> ) {
			( (ConnectionOpenable) aComponent ).open( aConnection );
		}
	}

	/**
	 * Helper method for configuring all {@link ConnectionOpenable}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}.
	 *
	 * @param <CON> the generic type
	 * @param aComponentExecutionStrategy The strategy to use when connecting
	 *        the {@link Component} instances.
	 * @param aConnection the connection
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to connect (in case them implement the
	 *        {@link ConnectionOpenable} interface).
	 * 
	 * @throws IOException in case connecting of at least one
	 *         {@link ConnectionOpenable} instance failed.
	 */
	public static <CON> void open( ExecutionStrategy aComponentExecutionStrategy, CON aConnection, Collection<?> aComponents ) throws IOException {
		open( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aConnection, aComponents );
	}

	/**
	 * Helper method for configuring all {@link ConnectionOpenable}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}. An
	 * {@link ExecutorService} can be provided in case some EJB container is to
	 * use a managed {@link ExecutorService} provided by an EJB server.
	 *
	 * @param <CON> the generic type
	 * @param aComponentExecutionStrategy The strategy to use when connecting
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aConnection the connection
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws IOException in case connecting of at least one
	 *         {@link ConnectionOpenable} instance failed.
	 */
	public static <CON> void open( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, CON aConnection, Collection<?> aComponents ) throws IOException {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _connectCallableFactoryImpl, aComponents, aConnection );
		}
		catch ( IOException e ) {
			throw e;
		}
		catch ( Exception e ) {
			throw new IOException( e );
		}
	}

	/**
	 * Helper method for configuring all {@link ConfigureException}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}.
	 *
	 * @param <CON> the generic type
	 * @param aComponentExecutionStrategy The strategy to use when connecting
	 *        the {@link Component} instances.
	 * @param aConnection the connection
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to connect (in case them implement the
	 *        {@link ConfigureException} interface).
	 * 
	 * @throws IOException in case connecting of at least one
	 *         {@link ConfigureException} instance failed.
	 */
	@SafeVarargs
	public static <CON> void open( ExecutionStrategy aComponentExecutionStrategy, CON aConnection, Object... aComponents ) throws IOException {
		open( aComponentExecutionStrategy, aConnection, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for configuring all {@link ConfigureException}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}. An
	 * {@link ExecutorService} can be provided in case some EJB container is to
	 * use a managed {@link ExecutorService} provided by an EJB server.
	 *
	 * @param <CON> the generic type
	 * @param aComponentExecutionStrategy The strategy to use when connecting
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aConnection the connection
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws IOException in case connecting of at least one
	 *         {@link ConfigureException} instance failed.
	 */
	@SafeVarargs
	public static <CON> void open( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, CON aConnection, Object... aComponents ) throws IOException {
		open( aComponentExecutionStrategy, aExecutorService, aConnection, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// I/O CONNECT:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, configuring the given {@link Component} in case it is
	 * {@link BidirectionalConnectionOpenable}.
	 *
	 * @param <INPUT> the generic type
	 * @param <OUTPUT> the generic type
	 * @param aComponent The {@link Component} to configure.
	 * @param aInputConnection the input connection
	 * @param aOutputConnection the output connection
	 * 
	 * @throws IOException Thrown in case connecting failed.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static <INPUT, OUTPUT> void open( Object aComponent, INPUT aInputConnection, OUTPUT aOutputConnection ) throws IOException {
		if ( aComponent instanceof BidirectionalConnectionOpenable<?, ?> ) {
			( (BidirectionalConnectionOpenable) aComponent ).open( aInputConnection, aOutputConnection );
		}
	}

	/**
	 * Helper method for configuring all {@link BidirectionalConnectionOpenable}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}.
	 *
	 * @param <INPUT> the generic type
	 * @param <OUTPUT> the generic type
	 * @param aComponentExecutionStrategy The strategy to use when connecting
	 *        the {@link Component} instances.
	 * @param aInputConnection the input connection
	 * @param aOutputConnection the output connection
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to connect (in case them implement the
	 *        {@link BidirectionalConnectionOpenable} interface).
	 * 
	 * @throws IOException in case connecting of at least one
	 *         {@link BidirectionalConnectionOpenable} instance failed.
	 */
	public static <INPUT, OUTPUT> void open( ExecutionStrategy aComponentExecutionStrategy, INPUT aInputConnection, OUTPUT aOutputConnection, Collection<?> aComponents ) throws IOException {
		open( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aInputConnection, aOutputConnection, aComponents );
	}

	/**
	 * Helper method for configuring all {@link BidirectionalConnectionOpenable}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}. An
	 * {@link ExecutorService} can be provided in case some EJB container is to
	 * use a managed {@link ExecutorService} provided by an EJB server.
	 *
	 * @param <INPUT> the generic type
	 * @param <OUTPUT> the generic type
	 * @param aComponentExecutionStrategy The strategy to use when connecting
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aInputConnection the input connection
	 * @param aOutputConnection the output connection
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws IOException in case connecting of at least one
	 *         {@link BidirectionalConnectionOpenable} instance failed.
	 */
	public static <INPUT, OUTPUT> void open( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, INPUT aInputConnection, OUTPUT aOutputConnection, Collection<?> aComponents ) throws IOException {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _ioConnectCallableFactoryImpl, aComponents, aInputConnection, aOutputConnection );
		}
		catch ( IOException e ) {
			throw e;
		}
		catch ( Exception e ) {
			throw new IOException( e );
		}
	}

	/**
	 * Helper method for configuring all {@link ConfigureException}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}.
	 *
	 * @param <INPUT> the generic type
	 * @param <OUTPUT> the generic type
	 * @param aComponentExecutionStrategy The strategy to use when connecting
	 *        the {@link Component} instances.
	 * @param aInputConnection the input connection
	 * @param aOutputConnection the output connection
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to connect (in case them implement the
	 *        {@link ConfigureException} interface).
	 * 
	 * @throws IOException in case connecting of at least one
	 *         {@link ConfigureException} instance failed.
	 */
	@SafeVarargs
	public static <INPUT, OUTPUT> void open( ExecutionStrategy aComponentExecutionStrategy, INPUT aInputConnection, OUTPUT aOutputConnection, Object... aComponents ) throws IOException {
		open( aComponentExecutionStrategy, aInputConnection, aOutputConnection, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for configuring all {@link ConfigureException}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}. An
	 * {@link ExecutorService} can be provided in case some EJB container is to
	 * use a managed {@link ExecutorService} provided by an EJB server.
	 *
	 * @param <INPUT> the generic type
	 * @param <OUTPUT> the generic type
	 * @param aComponentExecutionStrategy The strategy to use when connecting
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aInputConnection the input connection
	 * @param aOutputConnection the output connection
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * 
	 * @throws IOException in case connecting of at least one
	 *         {@link ConfigureException} instance failed.
	 */
	@SafeVarargs
	public static <INPUT, OUTPUT> void open( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, INPUT aInputConnection, OUTPUT aOutputConnection, Object... aComponents ) throws IOException {
		open( aComponentExecutionStrategy, aExecutorService, aInputConnection, aOutputConnection, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// CLOSE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method, closes the given {@link Component} in case it is
	 * {@link Closable}.
	 * 
	 * @param aComponent The {@link Component} to close.
	 * 
	 * @throws IOException Thrown in case closing or pre-closing (flushing)
	 *         fails.
	 */
	public static void close( Object aComponent ) throws IOException {
		if ( aComponent instanceof Closable ) {
			( (Closable) aComponent ).close();
		}
	}

	/**
	 * Helper method for decomposing all {@link Closable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to close (in case them implement the
	 *        {@link Closable} interface).
	 */
	public static void close( ExecutionStrategy aComponentExecutionStrategy, Collection<?> aComponents ) {
		close( aComponentExecutionStrategy, ControlFlowUtility.createCachedExecutorService( true ), aComponents );
	}

	/**
	 * Helper method for decomposing all {@link Closable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 */
	public static void close( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Collection<?> aComponents ) {
		try {
			execute( aComponentExecutionStrategy, aExecutorService, _closeCallableFactoryImpl, aComponents, (Object[]) null );
		}
		catch ( Exception e ) {
			throw new CloseRuntimeException( e );
		}
	}

	/**
	 * Helper method for decomposing all {@link Closable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to close (in case them implement the
	 *        {@link Closable} interface).
	 */
	@SafeVarargs
	public static void close( ExecutionStrategy aComponentExecutionStrategy, Object... aComponents ) {
		close( aComponentExecutionStrategy, Arrays.asList( aComponents ) );
	}

	/**
	 * Helper method for decomposing all {@link Closable} {@link Component}
	 * instances found in the provided {@link Collection}. The strategy with
	 * which the {@link Component} instances are processed is defined with the
	 * provided {@link ExecutionStrategy}. An {@link ExecutorService} can be
	 * provided in case some EJB container is to use a managed
	 * {@link ExecutorService} provided by an EJB server.
	 *
	 * @param aComponentExecutionStrategy The strategy to use when decomposing
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 */
	@SafeVarargs
	public static void close( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, Object... aComponents ) {
		close( aComponentExecutionStrategy, aExecutorService, Arrays.asList( aComponents ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method for initializing all {@link Initializable}
	 * {@link Component} instances found in the provided {@link Collection}. The
	 * strategy with which the {@link Component} instances are processed is
	 * defined with the provided {@link ExecutionStrategy}. An
	 * {@link ExecutorService} can be provided in case some EJB container is to
	 * use a managed {@link ExecutorService} provided by an EJB server.
	 *
	 * @param <T> the generic type
	 * @param aComponentExecutionStrategy The strategy to use when initializing
	 *        the {@link Component} instances.
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads, required in case an {@link ExecutionStrategy#PARALLEL} or
	 *        {@link ExecutionStrategy#JOIN} is used.
	 * @param aCallableFactory the callable factory
	 * @param aComponents The {@link Collection} containing the
	 *        {@link Component} instances to process (in case them implement the
	 *        required interface).
	 * @param aArguments An additional argument such as the connection CON or
	 *        the context CTX required in https://www.metacodes.pro cases.
	 * 
	 * @throws Exception in case processing of at least one {@link Component}
	 *         instance failed.
	 */
	@SuppressWarnings("unchecked")
	public static <T> void execute( ExecutionStrategy aComponentExecutionStrategy, ExecutorService aExecutorService, CallableFactory<T> aCallableFactory, Collection<?> aComponents, Object... aArguments ) throws Exception {
		// SEQUENTIAL:
		if ( aComponentExecutionStrategy == ExecutionStrategy.SEQUENTIAL ) {
			for ( Object e : aComponents ) {
				if ( aCallableFactory.getType().isAssignableFrom( e.getClass() ) ) {
					final Callable<Void> theCallable = aCallableFactory.toCallable( (T) e, aArguments );
					theCallable.call();
				}
			}
		}
		// CONCURRENT:
		else if ( aComponentExecutionStrategy == ExecutionStrategy.PARALLEL || aComponentExecutionStrategy == ExecutionStrategy.JOIN ) {
			final Set<Callable<Void>> theCallables = new HashSet<>();
			for ( Object e : aComponents ) {
				if ( aCallableFactory.getType().isAssignableFrom( e.getClass() ) ) {
					final Callable<Void> theCallable = aCallableFactory.toCallable( (T) e, aArguments );
					theCallables.add( theCallable );
				}
			}
			// PARALLEL:
			if ( aComponentExecutionStrategy == ExecutionStrategy.PARALLEL ) {
				try {
					aExecutorService.invokeAll( theCallables );
				}
				catch ( InterruptedException ignored ) {}
			}
			// JOIN:
			else if ( aComponentExecutionStrategy == ExecutionStrategy.JOIN ) {
				final Set<Future<?>> theFutures = new HashSet<>();
				for ( Callable<?> eCallable : theCallables ) {
					theFutures.add( aExecutorService.submit( eCallable ) );
				}
				ControlFlowUtility.waitForFutures( theFutures );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER INTERFACES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * A factory passed to the
	 * {@link ComponentUtility#execute(ExecutionStrategy, ExecutorService, Class, Collection)}
	 * method for generating {@link Callable} instances of dedicated types as of
	 * the context where the
	 * {@link ComponentUtility#execute(ExecutionStrategy, ExecutorService, Class, Collection)}
	 * is invoked.
	 * 
	 * @param <T> The type of the {@link Component} supported.
	 */
	private static interface CallableFactory<T> {

		/**
		 * Creates a {@link Callable} from the given {@link Component}.
		 * 
		 * @param aComponent The {@link Component} for which to create a
		 *        {@link Callable}.
		 * @param aArguments Some {@link Callable} implementations require an
		 *        additional argument such as the context CTX or the connection
		 *        CON for configuration.
		 * 
		 * @return A {@link Callable} created from the provided argument.
		 */
		Callable<Void> toCallable( T aComponent, Object... aArguments );

		/**
		 * Returns the type of the {@link Component} to be processed.
		 * 
		 * @return The type of the {@link Component}.
		 */
		Class<T> getType();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Implementation of a INITIALIZE daemon.
	 */
	private static class InitializeCallable implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Initializable _component;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 * 
		 * @param aComponent The component to be encapsulated.
		 */
		private InitializeCallable( Initializable aComponent ) {
			_component = aComponent;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.initialize();
			return null;
		}
	}

	/**
	 * Implementation of a CONFIGURE daemon.
	 *
	 * @param <CTX> the generic type
	 */
	private static class ConfigureCallable<CTX> implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Configurable<CTX> _component;
		private final CTX _context;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 *
		 * @param aComponent The component to be encapsulated.
		 * @param aContext the context
		 */
		private ConfigureCallable( Configurable<CTX> aComponent, CTX aContext ) {
			_component = aComponent;
			_context = aContext;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.initialize( _context );
			return null;
		}
	}

	/**
	 * Implementation of a START daemon.
	 */
	private static class StartCallable implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Startable _component;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 * 
		 * @param aComponent The component to be encapsulated.
		 */
		private StartCallable( Startable aComponent ) {
			_component = aComponent;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.start();
			return null;
		}
	}

	/**
	 * Implementation of a PAUSE daemon.
	 */
	private static class PauseCallable implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Pausable _component;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 * 
		 * @param aComponent The component to be encapsulated.
		 */
		private PauseCallable( Pausable aComponent ) {
			_component = aComponent;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.pause();
			return null;
		}
	}

	/**
	 * Implementation of a RESUME daemon.
	 */
	private static class ResumeCallable implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Resumable _component;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 * 
		 * @param aComponent The component to be encapsulated.
		 */
		private ResumeCallable( Resumable aComponent ) {
			_component = aComponent;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.resume();
			return null;
		}
	}

	/**
	 * Implementation of a STOP daemon.
	 */
	private static class StopCallable implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Stoppable _component;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 * 
		 * @param aComponent The component to be encapsulated.
		 */
		private StopCallable( Stoppable aComponent ) {
			_component = aComponent;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.stop();
			return null;
		}
	}

	/**
	 * Implementation of a DESTROY daemon.
	 */
	private static class DestroyCallable implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Destroyable _component;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 * 
		 * @param aComponent The component to be encapsulated.
		 */
		private DestroyCallable( Destroyable aComponent ) {
			_component = aComponent;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.destroy();
			return null;
		}
	}

	/**
	 * Implementation of a DECOMPOSE daemon.
	 */
	private static class DecomposeCallable implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Decomposable _component;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 * 
		 * @param aComponent The component to be encapsulated.
		 */
		private DecomposeCallable( Decomposable aComponent ) {
			_component = aComponent;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.decompose();
			return null;
		}
	}

	/**
	 * Implementation of a DISPOSE daemon.
	 */
	private static class DisposeCallable implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Disposable _component;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 * 
		 * @param aComponent The component to be encapsulated.
		 */
		private DisposeCallable( Disposable aComponent ) {
			_component = aComponent;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.dispose();
			return null;
		}
	}

	/**
	 * Implementation of a FLUSH daemon.
	 */
	private static class FlushCallable implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Flushable _component;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 * 
		 * @param aComponent The component to be encapsulated.
		 */
		private FlushCallable( Flushable aComponent ) {
			_component = aComponent;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.flush();
			return null;
		}
	}

	/**
	 * Implementation of a RESET daemon.
	 */
	private static class ResetCallable implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Resetable _component;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 * 
		 * @param aComponent The component to be encapsulated.
		 */
		private ResetCallable( Resetable aComponent ) {
			_component = aComponent;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.reset();
			return null;
		}
	}

	/**
	 * Implementation of a OPEN daemon.
	 */
	private static class OpenCallable implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Openable _component;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 * 
		 * @param aComponent The component to be encapsulated.
		 */
		private OpenCallable( Openable aComponent ) {
			_component = aComponent;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.open();
			return null;
		}
	}

	/**
	 * Implementation of a CONNECT daemon.
	 *
	 * @param <CON> the generic type
	 */
	private static class ConnectCallable<CON> implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final ConnectionOpenable<CON> _component;
		private final CON _connection;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 *
		 * @param aComponent The component to be encapsulated.
		 * @param aConnection the connection
		 */
		private ConnectCallable( ConnectionOpenable<CON> aComponent, CON aConnection ) {
			_component = aComponent;
			_connection = aConnection;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.open( _connection );
			return null;
		}
	}

	/**
	 * Implementation of a I/O CONNECT daemon.
	 *
	 * @param <INPUT> the generic type
	 * @param <OUTPUT> the generic type
	 */
	private static class IoConnectCallable<INPUT, OUTPUT> implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final BidirectionalConnectionOpenable<INPUT, OUTPUT> _component;
		private final INPUT _inputConnection;
		private final OUTPUT _outputConnection;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 *
		 * @param aComponent The component to be encapsulated.
		 * @param aInputConnection the input connection
		 * @param aOutputConnection the output connection
		 */
		private IoConnectCallable( BidirectionalConnectionOpenable<INPUT, OUTPUT> aComponent, INPUT aInputConnection, OUTPUT aOutputConnection ) {
			_component = aComponent;
			_inputConnection = aInputConnection;
			_outputConnection = aOutputConnection;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.open( _inputConnection, _outputConnection );
			return null;
		}
	}

	/**
	 * Implementation of a CLOSE daemon.
	 */
	private static class CloseCallable implements Callable<Void> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Closable _component;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Encapsulates the given component in a daemon.
		 * 
		 * @param aComponent The component to be encapsulated.
		 */
		private CloseCallable( Closable aComponent ) {
			_component = aComponent;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Call.
		 *
		 * @return the void
		 * 
		 * @throws Exception the exception
		 */
		@Override
		public Void call() throws Exception {
			_component.close();
			return null;
		}
	}
}
