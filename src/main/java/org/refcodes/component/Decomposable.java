// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * This mixin might be implemented by a component in order to provide
 * decomposition facilities. No exception is thrown as decomposition must work
 * always!
 * <p>
 * The "decompose()" method {@link Decomposable#decompose()} differs from the
 * "destroy()" method {@link Destroyable#destroy()} in that "destroy()" shuts
 * down the component in memory, whereas "decompose()" also tears down external
 * resources such as files or DB schemas. This means that with "decompose()" all
 * external data will be lost, as with "destroy()" external data will be kept
 * (in terms that it makes sense for the actual implementation).
 */
public interface Decomposable {

	/**
	 * Decomposes the component. External resources might get deleted (such as
	 * files or DB schemas)!
	 */
	void decompose();

	/**
	 * The {@link DecomposeAutomaton} interface defines those methods related to
	 * the decompose life-cycle.
	 */
	public interface DecomposeAutomaton extends Decomposable {

		/**
		 * Determines whether the component may get decomposed.
		 * 
		 * @return True if {@link #decompose()} is possible.
		 */
		boolean isDecomposable();

		/**
		 * Determines whether the component is decomposed.
		 * 
		 * @return True in case of being decomposed, else false.
		 */
		boolean isDecomposed();
	}
}
