package org.refcodes.component;

import java.io.IOException;

import org.refcodes.exception.RuntimeIOException;

/**
 * The {@link FlushHandle} interface defines those methods related to the handle
 * based flush operation.
 * <p>
 * The handle reference requires the {@link Flushable} interface to be
 * implemented.
 *
 * @param <H> The type of the handle.
 */
public interface FlushHandle<H> {

	/**
	 * Determines whether the handle reference provides flushting by
	 * implementing the {@link Flushable} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException Thrown in case the handle is
	 *         unknown (there is none reference for this handle).
	 */
	boolean hasFlush( H aHandle );

	/**
	 * Flushes the component identified by the given handle.
	 *
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws IOException in case flushing fails.
	 * @throws UnknownHandleRuntimeException Thrown in case the handle is
	 *         unknown (there is none reference for this handle).
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 * @throws IllegalHandleStateChangeRuntimeException Thrown in case a state
	 *         change is not possible due to the current state the referenced
	 *         component is in.
	 */
	void flush( H aHandle ) throws IOException;

	/**
	 * Flushes the component by calling {@link #flush(Object)} without you to
	 * require catching a {@link IOException}.
	 * 
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws RuntimeIOException encapsulates the aCause and is thrown upon
	 *         encountering a {@link IOException} exception
	 */
	default void flushUnchecked( H aHandle ) {
		try {
			flush( aHandle );
		}
		catch ( IOException e ) {
			throw new RuntimeIOException( e );
		}
	}
}
