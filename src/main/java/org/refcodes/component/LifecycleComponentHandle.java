package org.refcodes.component;

/**
 * The {@link LifecycleComponentHandle} manages various {@link LifecycleStatus}
 * states for {@link LifecycleComponent} instances each related to a handle.
 * Operations manipulating on the {@link LifecycleStatus} are invoked by this
 * {@link LifecycleComponentHandle} with a handle identifying the according
 * referenced {@link LifecycleComponent}.
 * <p>
 * The {@link LifecycleComponent} contains the business-logic where as the
 * {@link LifecycleComponentHandle} provides the frame for managing this
 * business-logic. The {@link LifecycleAutomatonHandle} takes care of the
 * correct life-cycle applied on a {@link LifecycleComponent}.
 *
 * @param <H> The type of the handles.
 */
public interface LifecycleComponentHandle<H> extends InitializableHandle<H>, StartableHandle<H>, PausableHandle<H>, ResumableHandle<H>, StoppableHandle<H>, DestroyableHandle<H> {

	/**
	 * The {@link LifecycleAutomatonHandle} is an automaton managing various
	 * {@link LifecycleStatus} states for {@link Component} instances each
	 * related to a handle. Operations manipulating on the
	 * {@link LifecycleStatus} are invoked by this
	 * {@link LifecycleAutomatonHandle} with a handle identifying the according
	 * referenced {@link Component}. The {@link LifecycleComponent} contains the
	 * business-logic where as the {@link LifecycleAutomatonHandle} provides the
	 * frame for managing this business-logic. The
	 * {@link LifecycleAutomatonHandle} takes care of the correct life-cycle
	 * applied on a {@link LifecycleComponent}.
	 * 
	 * @param <H> The type of the handle.
	 */
	public interface LifecycleAutomatonHandle<H> extends LifecycleComponentHandle<H>, InitializeAutomatonHandle<H>, StartAutomatonHandle<H>, PauseAutomatonHandle<H>, ResumeAutomatonHandle<H>, StopAutomatonHandle<H>, DestroyAutomatonHandle<H>, LifecycleStatusHandle<H> {}
}
