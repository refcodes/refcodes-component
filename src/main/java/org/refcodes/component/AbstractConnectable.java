// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import java.io.IOException;

/**
 * The {@link AbstractConnectable} provides base functionality required by
 * connection related components.
 */
public class AbstractConnectable {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String PIPE = "pipe".toLowerCase();
	private static final String SOCKET = "socket".toLowerCase();
	private static final String CLOSED = "closed".toLowerCase();

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Tests whether the provided exception is thrown as of something already
	 * being closed. In some cases, when something is already closed and some
	 * (wrapper) method tries to close it (again), then no exception should be
	 * propagated. This is very implementation specific regarding the underlying
	 * JDK as we expect something like "Pipe closed", "Connection closed" or the
	 * like to be contained in the aMessage.
	 * 
	 * @param aException The {@link IOException} to be tested.
	 * 
	 * @return True in case we have an exception as of something to be closed
	 *         was already caused, else false.
	 */
	protected static boolean isThrownAsOfAlreadyClosed( IOException aException ) {
		// if ( IOException.class.isAssignableFrom( aException.getClass() ) && aException.getClass().isAssignableFrom( IOException.class ) ) {
		if ( aException.getMessage().toLowerCase().contains( CLOSED ) ) {
			if ( aException.getMessage().toLowerCase().contains( SOCKET ) ) {
				return true;
			}
			if ( aException.getMessage().toLowerCase().contains( PIPE ) ) {
				return true;
			}
		}
		// }
		return false;
	}
}
