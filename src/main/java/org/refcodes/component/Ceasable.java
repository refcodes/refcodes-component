// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.component.CeaseException.CeaseRuntimeException;

/**
 * This mixin might be implemented by a component in order to provide cease
 * facilities. Cease is the zombie between stop and destroy. A processed
 * sequence may be ceased instead of being stopped. Taken an animation; an
 * animation sequence is stopped so it stands still ("freezes") to be resumed
 * again (e.g. via a "start()" method). It may also get ceased instead of being
 * stopped, in that case it may get faded out or (if it is a sprite) it may
 * explode - so to gracefully finish off that animation sequence.
 */
public interface Ceasable {

	/**
	 * Ceases the component.
	 * 
	 * @throws CeaseException in case ceasing fails.
	 */
	void cease() throws CeaseException;

	/**
	 * Ceases the component by calling {@link #cease()} without you to require
	 * catching an {@link CeaseException}.
	 * 
	 * @throws CeaseRuntimeException encapsulates the aCause and is thrown upon
	 *         encountering a {@link CeaseException} exception
	 */
	default void ceaseUnchecked() {
		try {
			cease();
		}
		catch ( CeaseException e ) {
			throw new CeaseRuntimeException( e );
		}
	}

	/**
	 * The {@link CeaseAutomaton} interface defines those methods related to the
	 * cease life-cycle.
	 */
	public interface CeaseAutomaton extends Ceasable {

		/**
		 * Determines whether the component may get ceased.
		 * 
		 * @return True if {@link #cease()} is possible.
		 */
		boolean isCeasable();

		/**
		 * Determines whether the component is ceased.
		 * 
		 * @return True in case of being ceased, else false.
		 */
		boolean isCeased();
	}

	/**
	 * To enable the {@link Ceasable} functionality to be invoked in a builder
	 * chain.
	 *
	 * @param <B> The instance to be returned on which to apply succeeding
	 *        builder operations.
	 */
	public interface CeaseBuilder<B extends CeaseBuilder<B>> {

		/**
		 * Builder method for the {@link Ceasable#cease()} method.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws CeaseException Thrown in case ceasing fails.
		 */
		B withCease() throws CeaseException;

		/**
		 * Ceases the component by calling {@link #withCease()} without you to
		 * require catching an {@link CeaseException}.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws CeaseRuntimeException encapsulates the aCause and is thrown
		 *         upon encountering a {@link CeaseException} exception
		 */
		default B withCeaseUnchecked() {
			try {
				return withCease();
			}
			catch ( CeaseException e ) {
				throw new CeaseRuntimeException( e );
			}
		}
	}

	/**
	 * See {@link Ceasable} without any checked exception being declared.
	 */
	public interface UncheckedCeasable extends Ceasable {

		/**
		 * Same {@link Ceasable#cease()} without any checked exception being
		 * declared. {@inheritDoc}
		 */
		@Override
		void cease();
	}
}
