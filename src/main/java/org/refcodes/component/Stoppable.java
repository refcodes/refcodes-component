// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.component.StopException.StopRuntimeException;

/**
 * This mixin might be implemented by a component in order to provide stop
 * facilities.
 */
public interface Stoppable {

	/**
	 * Stops the component.
	 * 
	 * @throws StopException Thrown in case stopping fails.
	 */
	void stop() throws StopException;

	/**
	 * Stops the component by calling {@link #stop()} without you to require
	 * catching an {@link StopException}.
	 * 
	 * @throws StopRuntimeException encapsulates the aCause and is thrown upon
	 *         encountering a {@link StopException} exception
	 */
	default void stopUnchecked() {
		try {
			stop();
		}
		catch ( StopException e ) {
			throw new StopRuntimeException( e );
		}
	}

	/**
	 * The {@link StopAutomaton} interface defines those methods related to the
	 * stop life-cycle.
	 */
	public interface StopAutomaton extends Stoppable {

		/**
		 * Determines whether the component may get stopped.
		 * 
		 * @return True if {@link #stop()} is possible.
		 */
		boolean isStoppable();

		/**
		 * Determines whether the component is stopped.
		 * 
		 * @return True in case of being stopped, else false.
		 */
		boolean isStopped();
	}

	/**
	 * To enable the {@link Stoppable} functionality to be invoked in a builder
	 * chain.
	 *
	 * @param <B> The instance to be returned on which to apply succeeding
	 *        builder operations.
	 */
	public interface StopBuilder<B extends StopBuilder<B>> {

		/**
		 * Builder method for the {@link Stoppable#stop()} method.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws StopException Thrown in case stopping fails.
		 */
		B withStop() throws StopException;

		/**
		 * Stops the component by calling {@link #withStop()} without you to
		 * require catching an {@link StopException}.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws StopRuntimeException encapsulates the aCause and is thrown
		 *         upon encountering a {@link StopException} exception
		 */
		default B withStopUnchecked() {
			try {
				return withStop();
			}
			catch ( StopException e ) {
				throw new StopRuntimeException( e );
			}
		}
	}

	/**
	 * See {@link Stoppable} without any checked exception being declared.
	 */
	public interface UncheckedStoppable extends Stoppable {

		/**
		 * {@inheritDoc} Same as {@link Stoppable#stop()} without any checked
		 * exception being declared.
		 */
		@Override
		void stop();
	}
}
