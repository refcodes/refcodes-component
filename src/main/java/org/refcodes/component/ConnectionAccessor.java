// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Provides an accessor for a connection property for e.g. an
 * {@link InputStream} or an {@link OutputStream}.
 *
 * @param <CON> The type of the connection to be used.
 */
public interface ConnectionAccessor<CON> {

	/**
	 * Retrieves the connection from the connection property.
	 * 
	 * @return The connection stored by the connection property.
	 */
	CON getConnection();

	/**
	 * Provides a mutator for a connection property for e.g. an
	 * {@link InputStream} or an {@link OutputStream}.
	 * 
	 * @param <CON> The type of the connection property.
	 */
	public interface ConnectionMutator<CON> {

		/**
		 * Sets the connection for the connection property.
		 * 
		 * @param aConnection The connection to be stored by the connection
		 *        property.
		 */
		void setConnection( CON aConnection );
	}

	/**
	 * Provides a connection property for e.g. an {@link InputStream} or an
	 * {@link OutputStream}.
	 * 
	 * @param <CON> The type of the connection property.
	 */
	public interface ConnectionProperty<CON> extends ConnectionAccessor<CON>, ConnectionMutator<CON> {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given connection (setter) as
		 * of {@link #setConnection(Object)} and returns the very same value
		 * (getter).
		 * 
		 * @param aConnection The connection to set (via
		 *        {@link #setConnection(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default CON letConnection( CON aConnection ) {
			setConnection( aConnection );
			return aConnection;
		}
	}
}
