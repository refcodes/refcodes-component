package org.refcodes.component;

/**
 * The {@link LinkComponentHandle} manages various {@link ConnectionStatus}
 * states for {@link LinkComponent} instances each related to a handle.
 * Operations manipulating on the {@link ConnectionStatus} are invoked by this
 * {@link LinkComponentHandle} with a handle identifying the according
 * referenced {@link Component}.
 * <p>
 * The {@link LinkComponent} contains the business-logic where as the
 * {@link LinkComponentHandle} provides the frame for managing this
 * business-logic. The {@link LinkAutomatonHandle} takes care of the correct
 * open/close-cycle applied on a {@link LinkComponent}.
 *
 * @param <H> The type of the handles.
 */
public interface LinkComponentHandle<H> extends OpenableHandle<H>, ClosableHandle<H> {

	/**
	 * The {@link LinkAutomatonHandle} is an automaton managing various
	 * {@link ConnectionStatus} states for {@link Component} instances each
	 * related to a handle. Operations manipulating on the
	 * {@link ConnectionStatus} are invoked by this {@link LinkAutomatonHandle}
	 * with a handle identifying the according referenced {@link Component}. The
	 * {@link LinkComponent} contains the business-logic where as the
	 * {@link LinkAutomatonHandle} provides the frame for managing this
	 * business-logic. The {@link LinkAutomatonHandle} takes care of the correct
	 * life-cycle applied on a {@link LinkComponent}.
	 *
	 * @param <H> the generic type
	 */
	public interface LinkAutomatonHandle<H> extends LinkComponentHandle<H>, OpenableHandle<H>, ClosableHandle<H>, ConnectionStatusHandle<H> {}
}
