// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.component.InitializeException.InitializeRuntimeException;

/**
 * This mixin might be implemented by a component in order to provide
 * initializing facilities.
 * <p>
 * In case a context is to be provided to the {@link #initialize()} method, you
 * may use the {@link Configurable} interface with its
 * {@link Configurable#initialize(Object)} method, which provides an argument
 * specifying the context to be passed to the {@link Component}.
 */
public interface Initializable {

	/**
	 * Initialize the component.
	 * 
	 * @throws InitializeException Thrown in case initializing fails.
	 */
	void initialize() throws InitializeException;

	/**
	 * Initialize the component by calling {@link #initialize()} without you to
	 * require catching an {@link InitializeException}.
	 * 
	 * @throws InitializeRuntimeException encapsulates the aCause and is thrown
	 *         upon encountering a {@link InitializeException} exception
	 */
	default void initializeUnchecked() {
		try {
			initialize();
		}
		catch ( InitializeException e ) {
			throw new InitializeRuntimeException( e );
		}
	}

	/**
	 * The {@link InitializeAutomaton} interface defines those methods related
	 * to the initialize life-cycle.
	 */
	public interface InitializeAutomaton extends Initializable, InitializedAccessor {

		/**
		 * Determines whether the component may get initialized.
		 * 
		 * @return True if {@link #initialize()} is possible.
		 */
		boolean isInitalizable();
	}

	/**
	 * To enable the {@link Initializable} functionality to be invoked in a
	 * builder chain.
	 *
	 * @param <B> The instance to be returned on which to apply succeeding
	 *        builder operations.
	 */
	public interface InitializeBuilder<B extends InitializeBuilder<B>> {

		/**
		 * Builder method for the {@link Initializable#initialize()} method.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws InitializeException Thrown in case initializing fails.
		 */
		B withInitialize() throws InitializeException;

		/**
		 * Initialize the component by calling {@link #withInitialize()} without
		 * you to require catching an {@link InitializeException}.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws InitializeRuntimeException encapsulates the aCause and is
		 *         thrown upon encountering a {@link InitializeException}
		 *         exception
		 */
		default B withInitializeUnchecked() {
			try {
				return withInitialize();
			}
			catch ( InitializeException e ) {
				throw new InitializeRuntimeException( e );
			}
		}
	}

	/**
	 * See {@link Initializable} without any checked exception being declared.
	 */
	public interface UncheckedInitializable extends Initializable {

		/**
		 * {@inheritDoc} Same as {@link Initializable#initialize()} without any
		 * checked exception being declared.
		 */
		@Override
		void initialize();
	}
}
