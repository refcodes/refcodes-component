// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.component.LifecycleException.LifecycleRuntimeException;

/**
 * Thrown in case destroying a component caused problems. Usually a method
 * similar to "destroy()" throws such an exception. This is a
 * {@link RuntimeException} as destroying a component should always succeed and
 * a {@link DestroyException} should only be thrown in case of obvious
 * malfunctioning and the philosophy of "fail early!" without forcing a method
 * to declare an according checked exception!
 */
public class DestroyException extends LifecycleRuntimeException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public DestroyException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public DestroyException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public DestroyException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 */
	public DestroyException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public DestroyException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public DestroyException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * Unchecked exception with the same semantics as the
	 * {@link DestroyException}.
	 */
	public static class DestroyRuntimeException extends LifecycleRuntimeException {

		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * Instantiates a new according exception.
		 *
		 * @param aMessage The aMessage describing this exception.
		 * @param aErrorCode The error code identifying this exception.
		 */
		public DestroyRuntimeException( String aMessage, String aErrorCode ) {
			super( aMessage, aErrorCode );
		}

		/**
		 * Instantiates a new according exception.
		 *
		 * @param aMessage The aMessage describing this exception.
		 * @param aCause The {@link Throwable} ({@link Exception}) causing this
		 *        exception.
		 * @param aErrorCode The error code identifying this exception.
		 */
		public DestroyRuntimeException( String aMessage, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
		}

		/**
		 * Instantiates a new according exception.
		 *
		 * @param aMessage The aMessage describing this exception.
		 * @param aCause The {@link Throwable} ({@link Exception}) causing this
		 *        exception.
		 */
		public DestroyRuntimeException( String aMessage, Throwable aCause ) {
			super( aMessage, aCause );
		}

		/**
		 * Instantiates a new according exception.
		 *
		 * @param aMessage The aMessage describing this exception.
		 */
		public DestroyRuntimeException( String aMessage ) {
			super( aMessage );
		}

		/**
		 * Instantiates a new according exception.
		 *
		 * @param aCause The {@link Throwable} ({@link Exception}) causing this
		 *        exception.
		 * @param aErrorCode The error code identifying this exception.
		 */
		public DestroyRuntimeException( Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
		}

		/**
		 * Instantiates a new according exception.
		 *
		 * @param aCause The {@link Throwable} ({@link Exception}) causing this
		 *        exception.
		 */
		public DestroyRuntimeException( Throwable aCause ) {
			super( aCause );
		}
	}
}
