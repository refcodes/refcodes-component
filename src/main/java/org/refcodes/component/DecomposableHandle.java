package org.refcodes.component;

import org.refcodes.component.Decomposable.DecomposeAutomaton;

/**
 * The {@link DecomposableHandle} interface defines those methods related to the
 * handle based decompose life-cycle.
 * <p>
 * The handle reference requires the {@link Decomposable} interface to be
 * implemented.
 *
 * @param <H> The type of the handle.
 */
public interface DecomposableHandle<H> {

	/**
	 * Determines whether the handle reference is decomposable by implementing
	 * the {@link Decomposable} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException in case the handle is unknown.
	 */
	boolean hasDecomposable( H aHandle );

	/**
	 * Decomposes the component identified by the given handle.
	 *
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws UnknownHandleRuntimeException in case the given handle is
	 *         unknown.
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 */
	void decompose( H aHandle );

	/**
	 * The {@link DecomposeAutomatonHandle} interface defines those methods
	 * related to the handle based decompose life-cycle. The handle reference
	 * requires the {@link DecomposeAutomaton} interface to be implemented.
	 * 
	 * @param <H> The type of the handle.
	 */
	public interface DecomposeAutomatonHandle<H> extends DecomposableHandle<H> {

		/**
		 * Determines whether the handle reference is decomposable by
		 * implementing the {@link DecomposeAutomaton} interface.
		 * 
		 * @param aHandle The handle to test whether the reference provides the
		 *        according functionality.
		 * 
		 * @return True in case the reference provides the according
		 *         functionality.
		 * 
		 * @throws UnknownHandleRuntimeException in case the handle is unknown.
		 */
		boolean hasDecomposeAutomaton( H aHandle );

		/**
		 * Determines whether the component identified by the given handle may
		 * get decomposed.
		 *
		 * @param aHandle The handle identifying the component.
		 * 
		 * @return True if {@link #decompose(Object)} is possible.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isDecomposable( H aHandle );

		/**
		 * Decomposes the component identified by the given handle.
		 *
		 * @param aHandle The handle identifying the component.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		@Override
		void decompose( H aHandle );
	}
}
