package org.refcodes.component;

import java.io.IOException;

import org.refcodes.component.ConnectionOpenable.ConnectionOpenAutomaton;
import org.refcodes.exception.RuntimeIOException;

/**
 * The {@link ConnectionOpenableHandle} interface defines those methods related
 * to the handle based open/connect life-cycle.
 * <p>
 * The handle reference requires the {@link ConnectionOpenable} interface to be
 * implemented.
 *
 * @param <H> The type of the handle.
 * @param <CON> The connection used to open the referenced instance.
 */
public interface ConnectionOpenableHandle<H, CON> {

	/**
	 * Determines whether the handle reference is {@link ConnectionOpenable} by
	 * implementing the {@link ConnectionOpenable} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException in case the handle is unknown.
	 */
	boolean hasConnectionOpenable( H aHandle );

	/**
	 * Open/connect the component identified by the given handle.
	 *
	 * @param aHandle The handle identifying the component.
	 * @param aConnection The connection to be passed to the implementing
	 *        instance.
	 * 
	 * @throws IOException in case opening/connecting fails.
	 * @throws UnknownHandleRuntimeException in case the given handle is
	 *         unknown.
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 * @throws IllegalHandleStateChangeRuntimeException Thrown in case a state
	 *         change is not possible due to the current state the referenced
	 *         component is in.
	 */
	void open( H aHandle, CON aConnection ) throws IOException;

	/**
	 * Opens the component by calling {@link #open(Object, Object)} without you
	 * to require catching an {@link IOException}.
	 * 
	 * @param aHandle The handle identifying the component.
	 * @param aConnection The connection to be passed to the implementing
	 *        instance.
	 * 
	 * @throws RuntimeIOException encapsulates the aCause and is thrown upon
	 *         encountering a {@link IOException} exception
	 */
	default void openUnchecked( H aHandle, CON aConnection ) {
		try {
			open( aHandle, aConnection );
		}
		catch ( IOException e ) {
			throw new RuntimeIOException( e );
		}
	}

	/**
	 * The {@link ConnectionOpenAutomatonHandle} interface defines those methods
	 * related to the handle based open/connect life-cycle. The handle reference
	 * requires the {@link ConnectionOpenAutomaton} interface to be implemented.
	 * 
	 * @param <H> The type of the handle.
	 * @param <CON> the connection used to initialize the referenced instance.
	 */
	public interface ConnectionOpenAutomatonHandle<H, CON> extends ConnectionOpenableHandle<H, CON>, OpenedHandle<H> {

		/**
		 * Determines whether the handle reference is configurable by
		 * implementing the {@link ConnectionOpenAutomaton} interface.
		 * 
		 * @param aHandle The handle to test whether the reference provides the
		 *        according functionality.
		 * 
		 * @return True in case the reference provides the according
		 *         functionality.
		 * 
		 * @throws UnknownHandleRuntimeException in case the handle is unknown.
		 */
		boolean hasConnectionOpenAutomaton( H aHandle );

		/**
		 * Determines whether the component identified by the given handle may
		 * get opened/connected.
		 *
		 * @param aHandle The handle identifying the component.
		 * @param aConnection The connection to be passed to the implementing
		 *        instance.
		 * 
		 * @return True if {@link #open(Object, Object)} is possible.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isOpenable( H aHandle, CON aConnection );
	}
}
