// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * Provides an accessor for a closed property.
 */
public interface ClosedAccessor {

	/**
	 * Retrieves the is-closed status. Determines whether the component's
	 * connection is closed. A component's connection is closed after being
	 * closed as of {@link Closable#close()}.
	 * 
	 * @return True in case of being closed.
	 */
	boolean isClosed();

	/**
	 * Provides a mutator for a closed property.
	 */
	public interface ClosedMutator {

		/**
		 * Sets the is-closed property for the closed property.
		 * 
		 * @param isClosed The closed property to be stored by the closed
		 *        property.
		 */
		void setClosed( boolean isClosed );
	}

	/**
	 * Provides a closed property.
	 */
	public interface ClosedProperty extends ClosedAccessor, ClosedMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given status (setter) as of
		 * {@link #setClosed(boolean)} and returns the very same value (getter).
		 * 
		 * @param isClosed The status to set (via {@link #setClosed(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letClosed( boolean isClosed ) {
			setClosed( isClosed );
			return isClosed;
		}
	}
}
