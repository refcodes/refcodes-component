// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.component.LinkComponentHandle.LinkAutomatonHandle;

/**
 * A component implementing the {@link LinkComponent} interface supports
 * establishing a connection (not necessarily a network connection). I.e. such a
 * component may be instructed open or close a connection:
 * <p>
 * "open" - "close"
 * <p>
 * In case a connection is to be provided to the {@link #open()} method, you may
 * use the {@link ConnectionComponent} interface with its
 * {@link ConnectionComponent#open(Object)} method, which provides an argument
 * specifying the connection to be passed to the {@link Component}.
 */
public interface LinkComponent extends Openable, Closable {

	/**
	 * Similar to the {@link LinkComponent} with additional according builder
	 * methods.
	 *
	 * @param <B> The instance to be returned on which to apply succeeding
	 *        builder operations.
	 */
	public interface LinkComponentBuilder<B extends LinkComponentBuilder<B>> extends LinkComponent, OpenBuilder<LinkComponentBuilder<B>>, CloseBuilder<LinkComponentBuilder<B>> {}

	/**
	 * A system implementing the {@link LinkAutomaton} interface supports
	 * managing {@link LinkComponent} instances and takes care that the
	 * open/close statuses are invoked in the correct order by throwing
	 * according exceptions in case the open/close-cycle is invoked in the wrong
	 * order. A {@link LinkAutomaton} may be used to wrap a
	 * {@link LinkComponent} by a {@link LinkAutomatonHandle} for managing
	 * {@link LinkAutomaton} instances. The {@link LinkComponent} contains the
	 * business-logic where as the {@link LinkAutomatonHandle} provides the
	 * frame for managing this business-logic. The {@link LinkAutomaton} takes
	 * care of the correct open/close-cycle applied on a {@link LinkComponent}.
	 */
	public interface LinkAutomaton extends LinkComponent, OpenAutomaton, CloseAutomaton, ConnectionStatusAccessor {}
}