package org.refcodes.component;

import org.refcodes.component.Ceasable.CeaseAutomaton;

/**
 * The {@link CeasableHandle} interface defines those methods related to the
 * handle based cease life-cycle.
 * <p>
 * The handle reference requires the {@link Ceasable} interface to be
 * implemented.
 *
 * @param <H> The type of the handle.
 */
public interface CeasableHandle<H> {

	/**
	 * Determines whether the handle reference is ceasable by implementing the
	 * {@link Ceasable} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException in case the handle is unknown.
	 */
	boolean hasCeasable( H aHandle );

	/**
	 * Ceases the component identified by the given handle.
	 *
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws CeaseException in case ceasing fails.
	 * @throws UnknownHandleRuntimeException in case the given handle is
	 *         unknown.
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 * @throws IllegalHandleStateChangeRuntimeException Thrown in case a state
	 *         change is not possible due to the current state the referenced
	 *         component is in.
	 */
	void cease( H aHandle ) throws CeaseException;

	/**
	 * The {@link CeaseAutomatonHandle} interface defines those methods related
	 * to the handle based cease life-cycle. The handle reference requires the
	 * {@link CeaseAutomaton} interface to be implemented.
	 * 
	 * @param <H> The type of the handle.
	 */
	public interface CeaseAutomatonHandle<H> extends CeasableHandle<H> {

		/**
		 * Determines whether the handle reference is ceasable by implementing
		 * the {@link CeaseAutomaton} interface.
		 * 
		 * @param aHandle The handle to test whether the reference provides the
		 *        according functionality.
		 * 
		 * @return True in case the reference provides the according
		 *         functionality.
		 * 
		 * @throws UnknownHandleRuntimeException in case the handle is unknown.
		 */
		boolean hasCeaseAutomaton( H aHandle );

		/**
		 * Determines whether the component identified by the given handle may
		 * get ceased.
		 *
		 * @param aHandle The handle identifying the component.
		 * 
		 * @return True if {@link #cease(Object)} is possible.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isCeasable( H aHandle );

		/**
		 * Determines whether the component identified by the given handle is
		 * ceased.
		 *
		 * @param aHandle The handle identifying the component.
		 * 
		 * @return True in case of being ceased, else false.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isCeased( H aHandle );
	}
}
