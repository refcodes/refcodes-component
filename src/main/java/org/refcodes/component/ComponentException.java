// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.exception.AbstractException;

/**
 * This exception is the base exception for the component package.
 */
public abstract class ComponentException extends AbstractException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public ComponentException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ComponentException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ComponentException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public ComponentException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public ComponentException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ComponentException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * This exception is the base exception for connection related exceptions.
	 */
	@SuppressWarnings("rawtypes")
	abstract static class ComponentConnectionException extends ComponentException implements ConnectionAccessor {

		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////////

		private final Object _connection;

		// /////////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 * 
		 * @param aConnection the connection
		 */
		public ComponentConnectionException( String aMessage, Object aConnection, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_connection = aConnection;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aConnection the connection
		 */
		public ComponentConnectionException( String aMessage, Object aConnection, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_connection = aConnection;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aConnection the connection
		 */
		public ComponentConnectionException( String aMessage, Object aConnection, Throwable aCause ) {
			super( aMessage, aCause );
			_connection = aConnection;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aConnection the connection
		 */
		public ComponentConnectionException( String aMessage, Object aConnection ) {
			super( aMessage );
			_connection = aConnection;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aConnection the connection
		 */
		public ComponentConnectionException( Object aConnection, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_connection = aConnection;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aConnection the connection
		 */
		public ComponentConnectionException( Object aConnection, Throwable aCause ) {
			super( aCause );
			_connection = aConnection;
		}

		// /////////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * Gets the connection.
		 *
		 * @return the connection
		 */
		@Override
		public Object getConnection() {
			return _connection;
		}
	}

	/**
	 * This exception is the base exception for connection related exceptions.
	 */
	@SuppressWarnings("rawtypes")
	abstract static class ComponentIoConnectionException extends ComponentException implements BidirectionalConnectionAccessor {

		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////////

		private final Object _inputConnection;
		private final Object _outputConnection;

		// /////////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 * 
		 * @param aInputConnection the input connection
		 * @param aOutputConnection the output connection
		 */
		public ComponentIoConnectionException( String aMessage, Object aInputConnection, Object aOutputConnection, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_inputConnection = aInputConnection;
			_outputConnection = aOutputConnection;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aInputConnection the input connection
		 * @param aOutputConnection the output connection
		 */
		public ComponentIoConnectionException( String aMessage, Object aInputConnection, Object aOutputConnection, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_inputConnection = aInputConnection;
			_outputConnection = aOutputConnection;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aInputConnection the input connection
		 * @param aOutputConnection the output connection
		 */
		public ComponentIoConnectionException( String aMessage, Object aInputConnection, Object aOutputConnection, Throwable aCause ) {
			super( aMessage, aCause );
			_inputConnection = aInputConnection;
			_outputConnection = aOutputConnection;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aInputConnection the input connection
		 * @param aOutputConnection the output connection
		 */
		public ComponentIoConnectionException( String aMessage, Object aInputConnection, Object aOutputConnection ) {
			super( aMessage );
			_inputConnection = aInputConnection;
			_outputConnection = aOutputConnection;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aInputConnection the input connection
		 * @param aOutputConnection the output connection
		 */
		public ComponentIoConnectionException( Object aInputConnection, Object aOutputConnection, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_inputConnection = aInputConnection;
			_outputConnection = aOutputConnection;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aInputConnection the input connection
		 * @param aOutputConnection the output connection
		 */
		public ComponentIoConnectionException( Object aInputConnection, Object aOutputConnection, Throwable aCause ) {
			super( aCause );
			_inputConnection = aInputConnection;
			_outputConnection = aOutputConnection;
		}

		// /////////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * Gets the input connection.
		 *
		 * @return the input connection
		 */
		@Override
		public Object getInputConnection() {
			return _inputConnection;
		}

		/**
		 * Gets the output connection.
		 *
		 * @return the output connection
		 */
		@Override
		public Object getOutputConnection() {
			return _outputConnection;
		}
	}

	/**
	 * This exception is the base exception for connection related exceptions.
	 */
	@SuppressWarnings("rawtypes")
	abstract static class ComponentContextException extends ComponentException implements ContextAccessor {

		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Object _context;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 * 
		 * @param aContext The context involved in this exception.
		 */
		public ComponentContextException( String aMessage, Object aContext, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_context = aContext;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aContext The context involved in this exception.
		 */
		public ComponentContextException( String aMessage, Object aContext, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_context = aContext;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aContext The context involved in this exception.
		 */
		public ComponentContextException( String aMessage, Object aContext, Throwable aCause ) {
			super( aMessage, aCause );
			_context = aContext;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aContext The context involved in this exception.
		 */
		public ComponentContextException( String aMessage, Object aContext ) {
			super( aMessage );
			_context = aContext;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aContext The context involved in this exception.
		 */
		public ComponentContextException( Object aContext, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_context = aContext;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aContext The context involved in this exception.
		 */
		public ComponentContextException( Object aContext, Throwable aCause ) {
			super( aCause );
			_context = aContext;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object getContext() {
			return _context;
		}
	}
}
