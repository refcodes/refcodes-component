// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * A component implementing the {@link ConnectionComponent} interface supports a
 * connection. I.e. such a component may be instructed open or close a
 * connection: "open" - "close" In case no connection is to be provided to the
 * {@link #open(Object)} method (as it may have been passed via the
 * constructor), you may use the {@link LinkComponent} interface with its
 * {@link Openable#open()} method, which does not require any arguments
 * specifying a connection.
 *
 * @param <CON> The type of the connection to be used.
 */
public interface ConnectionComponent<CON> extends ConnectionOpenable<CON>, Closable {

	/**
	 * Similar to the {@link ConnectionComponent} with additional according
	 * builder methods.
	 *
	 * @param <CON> The type of the connection to be used.
	 * @param <B> The instance to be returned on which to apply succeeding
	 *        builder operations.
	 */
	public interface ConnectionComponentBuilder<CON, B extends ConnectionComponentBuilder<CON, B>> extends ConnectionComponent<CON>, ConnectionOpenBuilder<CON, ConnectionComponentBuilder<CON, B>>, CloseBuilder<ConnectionComponentBuilder<CON, B>> {}

	/**
	 * A system implementing the {@link ConnectionAutomaton} interface supports
	 * managing {@link ConnectionComponent} instances and takes care that the
	 * open/close statuses are invoked in the correct order by throwing
	 * according exceptions in case the open/close-cycle is invoked in the wrong
	 * order. A {@link ConnectionAutomaton} may be used to wrap a
	 * {@link ConnectionComponent} by a {@link ConnectionComponentHandle} for
	 * managing {@link ConnectionAutomaton} instances. The
	 * {@link ConnectionComponent} contains the business-logic where as the
	 * {@link ConnectionComponentHandle} provides the frame for managing this
	 * business-logic. The {@link ConnectionAutomaton} takes care of the correct
	 * open/close-cycle applied on a {@link ConnectionComponent}.
	 * 
	 * @param <CON> The type of the connection to be used.
	 */
	public interface ConnectionAutomaton<CON> extends ConnectionComponent<CON>, ConnectionOpenAutomaton<CON>, CloseAutomaton, ConnectionStatusAccessor {}

}
