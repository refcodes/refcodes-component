package org.refcodes.component;

import java.util.Collection;
import java.util.Set;

/**
 * The handle directory enables listing of all known data managed by the
 * implementing component.
 *
 * @param <H> The handle used by the {@link HandleDirectory}.
 * @param <REF> The type of the objects to which the handles reference to.
 */
public interface HandleDirectory<H, REF> extends HandleLookup<H, REF> {

	/**
	 * Retrieves a list of all known handles managed by the implementing
	 * component.
	 * 
	 * @return The list of the handles known by the implementing component.
	 */
	Set<H> handles();

	/**
	 * Retrieves a list of all known objects for which there is a handle.
	 *
	 * @return The list of the objects managed by a handle.
	 * 
	 * @param <T> As of convenience, the type of the reference returned.
	 *        CAUTION: The type <code>T</code> being a sub-type of
	 *        <code>REF</code> has the drawback that in case you specify a
	 *        sub-type of <code>REF</code> (<code>T</code>), you may end up with
	 *        a class cast exception in case you do not make sure that the
	 *        handle references the expected type <code>T</code>.
	 */
	<T extends REF> Collection<T> handleReferences();
}
