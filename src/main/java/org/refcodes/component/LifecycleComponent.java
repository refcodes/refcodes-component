// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.component.LifecycleComponentHandle.LifecycleAutomatonHandle;

/**
 * A component implementing the {@link LifecycleComponent} interface supports a
 * life-cycle. I.e. such a component may be instructed from the outside to run
 * through several stages from getting started till being destroyed. The valid
 * state changes are mainly as follows: "initialize" - "start" - "pause" -
 * "resume" - "stop" - "destroy" For example: "initialize" - "start" - "pause" -
 * "resume" - "pause" - "resume" - "stop" - "start" - "pause" - "resume" -
 * "stop" - "destroy" The {@link LifecycleAutomatonHandle} is a component
 * managing various {@link LifecycleComponent}s each identified by a dedicated
 * handle. Operations on the {@link LifecycleComponent} are invoked by this
 * {@link LifecycleAutomatonHandle} with a handle identifying the according
 * {@link LifecycleComponent}.
 * <p>
 * The {@link LifecycleComponent} contains the business-logic where as the
 * {@link LifecycleAutomatonHandle} provides the frame for managing this
 * business-logic. The {@link LifecycleAutomaton} takes care of the correct
 * life-cycle applied on a {@link LifecycleComponent}.
 */
public interface LifecycleComponent extends Initializable, Startable, Pausable, Resumable, Stoppable, Destroyable {

	/**
	 * A system implementing the {@link LifecycleAutomaton} interface supports
	 * managing {@link LifecycleComponent} instances and takes care that the
	 * life-cycle stages are invoked in the correct order by throwing according
	 * exceptions in case the life-cycle is invoked in the wrong order. A
	 * {@link LifecycleAutomaton} may be used to wrap a
	 * {@link LifecycleComponent} by a {@link LifecycleAutomatonHandle} for
	 * managing {@link LifecycleComponent} instances. The
	 * {@link LifecycleComponent} contains the business-logic where as the
	 * {@link LifecycleAutomatonHandle} provides the frame for managing this
	 * business-logic. The {@link LifecycleAutomaton} takes care of the correct
	 * life-cycle applied on a {@link LifecycleComponent}.
	 */
	public interface LifecycleAutomaton extends LifecycleComponent, InitializeAutomaton, StartAutomaton, PauseAutomaton, ResumeAutomaton, StopAutomaton, DestroyAutomaton, LifecycleStatusAccessor {}

	/**
	 * Same as the {@link LifecycleComponent} though without the need to
	 * try-catch any exceptions on the various life-cycle stages.
	 */
	public interface UncheckedLifecycleComponent extends LifecycleComponent, UncheckedInitializable, UncheckedStartable, UncheckedPausable, UncheckedResumable, UncheckedStoppable, Destroyable {}

}
