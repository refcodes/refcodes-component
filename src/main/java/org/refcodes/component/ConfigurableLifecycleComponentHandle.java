package org.refcodes.component;

import org.refcodes.component.LifecycleComponentHandle.LifecycleAutomatonHandle;

/**
 * The {@link ConfigurableLifecycleComponentHandle} manages various
 * {@link LifecycleStatus} states for {@link ConfigurableLifecycleComponent}
 * instances each related to a handle. Operations manipulating on the
 * {@link LifecycleStatus} are invoked by this
 * {@link ConfigurableLifecycleComponentHandle} with a handle identifying the
 * according referenced {@link ConfigurableLifecycleComponent}.
 * <p>
 * The {@link ConfigurableLifecycleComponent} contains the business-logic where
 * as the {@link ConfigurableLifecycleComponentHandle} provides the frame for
 * managing this business-logic. The {@link LifecycleAutomatonHandle} takes care
 * of the correct life-cycle applied on a {@link ConfigurableLifecycleComponent}
 * .
 *
 * @param <H> The type of the handles.
 * @param <CTX> The context used to initialize the referenced instance.
 */
public interface ConfigurableLifecycleComponentHandle<H, CTX> extends ConfigurableHandle<H, CTX>, StartableHandle<H>, PausableHandle<H>, ResumableHandle<H>, StoppableHandle<H>, DestroyableHandle<H> {

	/**
	 * The {@link ConfigurableLifecycleAutomatonHandle} is an automaton managing
	 * various {@link LifecycleStatus} states for {@link Component} instances
	 * each related to a handle. Operations manipulating on the
	 * {@link LifecycleStatus} are invoked by this
	 * {@link ConfigurableLifecycleAutomatonHandle} with a handle identifying
	 * the according referenced {@link Component}. The
	 * {@link LifecycleComponent} contains the business-logic where as the
	 * {@link ConfigurableLifecycleAutomatonHandle} provides the frame for
	 * managing this business-logic. The
	 * {@link ConfigurableLifecycleAutomatonHandle} takes care of the correct
	 * life-cycle applied on a {@link LifecycleComponent}.
	 * 
	 * @param <H> The type of the handle.
	 * @param <CTX> the context used to initialize the referenced instance.
	 */
	public interface ConfigurableLifecycleAutomatonHandle<H, CTX> extends ConfigurableLifecycleComponentHandle<H, CTX>, ConfigureAutomatonHandle<H, CTX>, StartAutomatonHandle<H>, PauseAutomatonHandle<H>, ResumeAutomatonHandle<H>, StopAutomatonHandle<H>, DestroyAutomatonHandle<H>, LifecycleComponentHandle<H>, LifecycleStatusHandle<H> {}

}
