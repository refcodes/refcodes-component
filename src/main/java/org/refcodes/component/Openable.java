// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import java.io.IOException;

import org.refcodes.component.Initializable.InitializeAutomaton;
import org.refcodes.exception.RuntimeIOException;

/**
 * This mixin might be implemented by a component in order to provide opening
 * connection(s) facilities. The semantics of this interface is very similar to
 * that of the {@link Initializable} interface. To clarify the context regarding
 * connections, the {@link Openable} interface has been introduced.
 * <p>
 * In case a connection is to be provided to the {@link #open()} method, you may
 * use the {@link ConnectionOpenable} interface with its
 * {@link ConnectionOpenable#open(Object)} method, which provides an argument
 * specifying the connection to be passed to the {@link Component}.
 */
public interface Openable {

	/**
	 * Open the component's connection(s).
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 */
	void open() throws IOException;

	/**
	 * Opens the component by calling {@link #open()} without you to require
	 * catching an {@link IOException}.
	 * 
	 * @throws RuntimeIOException encapsulates the aCause and is thrown upon
	 *         encountering a {@link IOException} exception
	 */
	default void openUnchecked() {
		try {
			open();
		}
		catch ( IOException e ) {
			throw new RuntimeIOException( e );
		}
	}

	/**
	 * To enable the {@link Startable} functionality to be invoked in a builder
	 * chain.
	 *
	 * @param <B> The instance to be returned on which to apply succeeding
	 *        builder operations.
	 */
	public interface OpenBuilder<B extends OpenBuilder<B>> {

		/**
		 * Builder method for the {@link Openable#open()} method.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws IOException Thrown in case opening or accessing an open line
		 *         (connection, junction, link) caused problems.
		 */
		B withOpen() throws IOException;

		/**
		 * Opens the component by calling {@link #withOpen()} without you to
		 * require catching an {@link IOException}.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws RuntimeIOException encapsulates the aCause and is thrown upon
		 *         encountering a {@link IOException} exception
		 */
		default B withOpenUnchecked() {
			try {
				return withOpen();
			}
			catch ( IOException e ) {
				throw new RuntimeIOException( e );
			}
		}
	}

	/**
	 * The {@link OpenAutomaton} interface defines those methods related to the
	 * opening of connection(s) life-cycle. The semantics of this interface is
	 * very similar to that of the {@link InitializeAutomaton} interface. To
	 * clarify the context regarding connections, the {@link OpenAutomaton}
	 * interface has been introduced.
	 */
	public interface OpenAutomaton extends Openable, OpenedAccessor {

		/**
		 * Determines whether the component's connection(s) may get opened.
		 * 
		 * @return True if {@link #open()} is possible.
		 */
		boolean isOpenable();
	}
}
