// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import java.io.IOException;

import org.refcodes.component.Configurable.ConfigureAutomaton;

/**
 * This mixin might be implemented by a component in order to provide opening
 * connection(s) facilities. The semantics of this interface is very similar to
 * that of the {@link Configurable} interface. To clarify the context regarding
 * connections, the {@link BidirectionalConnectionOpenable} interface has been
 * introduced.
 * <p>
 * In case a no connection is to be provided to the
 * {@link #open(Object, Object)} method (as it may have been passed via the
 * constructor), you may use the {@link Openable} interface with its
 * {@link Openable#open()} method, which does not require any arguments
 * specifying a connection.
 *
 * @param <INPUT> The type of the input connection to be used.
 * @param <OUTPUT> The type of the output connection to be used.
 */
public interface BidirectionalConnectionOpenable<INPUT, OUTPUT> {

	/**
	 * Opens the component with the given connection, the component opens a
	 * connection with the given connection.
	 * 
	 * @param aInputConnection The input connection used for opening the
	 *        connection.
	 * @param aOutputConnection The output connection used for opening the
	 *        connection.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 */
	void open( INPUT aInputConnection, OUTPUT aOutputConnection ) throws IOException;

	/**
	 * The {@link BidirectionalConnectionOpenAutomaton} interface defines those
	 * methods related to the opening of connection(s) life-cycle. The semantics
	 * of this interface is very similar to that of the
	 * {@link ConfigureAutomaton} interface. To clarify the context regarding
	 * connections, the {@link BidirectionalConnectionOpenAutomaton} interface
	 * has been introduced.
	 * 
	 * @param <INPUT> The type of the input connection to be used.
	 * @param <OUTPUT> The type of the output connection to be used.
	 */
	public interface BidirectionalConnectionOpenAutomaton<INPUT, OUTPUT> extends BidirectionalConnectionOpenable<INPUT, OUTPUT>, OpenedAccessor {

		/**
		 * Determines whether the given connection may get opened, if true then
		 * component may open a connection with the given connection via the
		 * {@link #open(Object, Object)} method. Usually no physical connection
		 * is established; usually criteria describing the provided connection
		 * are evaluated; for example the connection is tested against a black
		 * list, a white list or against well-formedness or whether the
		 * specified protocols are supported (in case of a connection being a
		 * String URL beginning with "http://", "ftp://" or similar).
		 * ---------------------------------------------------------------------
		 * CAUTION: Even in case true is returned, the actual opening of a
		 * connection may fail (e.g. due to network failure or authentication
		 * issues).
		 * ---------------------------------------------------------------------
		 * 
		 * @param aInputConnection The input connection used for opening the
		 *        connection.
		 * @param aOutputConnection The output connection used for opening the
		 *        connection.
		 * 
		 * @return True if {@link #open(Object, Object)} is theoretically
		 *         possible.
		 */
		boolean isOpenable( INPUT aInputConnection, OUTPUT aOutputConnection );
	}
}
