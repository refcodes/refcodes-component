package org.refcodes.component;

import org.refcodes.component.StartException.StartRuntimeException;
import org.refcodes.component.Startable.StartAutomaton;

/**
 * The {@link StartableHandle} interface defines those methods related to the
 * handle based start life-cycle.
 * <p>
 * The handle reference requires the {@link Startable} interface to be
 * implemented.
 *
 * @param <H> The type of the handle.
 */
public interface StartableHandle<H> {

	/**
	 * Determines whether the handle reference is startable by implementing the
	 * {@link Startable} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException in case the handle is unknown.
	 */
	boolean hasStartable( H aHandle );

	/**
	 * Starts the component identified by the given handle.
	 *
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws StartException in case starting fails.
	 * @throws UnknownHandleRuntimeException in case the given handle is
	 *         unknown.
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 * @throws IllegalHandleStateChangeRuntimeException Thrown in case a state
	 *         change is not possible due to the current state the referenced
	 *         component is in.
	 */
	void start( H aHandle ) throws StartException;

	/**
	 * Starts the component by calling {@link #start(Object)} without you to
	 * require catching an {@link StartException}.
	 * 
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws StartRuntimeException encapsulates the aCause and is thrown upon
	 *         encountering a {@link StartException} exception
	 */
	default void startUnchecked( H aHandle ) {
		try {
			start( aHandle );
		}
		catch ( StartException e ) {
			throw new StartRuntimeException( e );
		}
	}

	/**
	 * The {@link StartAutomatonHandle} interface defines those methods related
	 * to the handle based start life-cycle. The handle reference requires the
	 * {@link StartAutomaton} interface to be implemented.
	 * 
	 * @param <H> The type of the handle.
	 */
	public interface StartAutomatonHandle<H> extends RunningHandle<H>, StartableHandle<H> {

		/**
		 * Determines whether the handle reference is startable by implementing
		 * the {@link StartAutomaton} interface.
		 * 
		 * @param aHandle The handle to test whether the reference provides the
		 *        according functionality.
		 * 
		 * @return True in case the reference provides the according
		 *         functionality.
		 * 
		 * @throws UnknownHandleRuntimeException in case the handle is unknown.
		 */
		boolean hasStartAutomaton( H aHandle );

		/**
		 * Determines whether the component identified by the given handle may
		 * get started.
		 *
		 * @param aHandle The handle identifying the component.
		 * 
		 * @return True if {@link #start(Object)} is possible.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isStartable( H aHandle );

	}
}
