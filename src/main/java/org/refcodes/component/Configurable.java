// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.component.ConfigureException.ConfigureRuntimeException;
import org.refcodes.component.InitializeException.InitializeRuntimeException;

/**
 * This mixin might be implemented by a component in order to provide initialize
 * facilities wit a configuration provided via {@link #initialize(Object)}.
 * <p>
 * In case a no context is to be provided to the {@link #initialize(Object)}
 * method (as it may have been passed via the constructor), you may use the
 * {@link Initializable} interface with its {@link Initializable#initialize()}
 * method, which does not require any arguments specifying a context.
 *
 * @param <CTX> the context used to initialize the implementing instance.
 */
public interface Configurable<CTX> {

	/**
	 * Initializes and configures the component.
	 *
	 * @param aContext The context to be passed to the implementing instance.
	 * 
	 * @throws InitializeException Thrown in case initializing fails.
	 */
	void initialize( CTX aContext ) throws InitializeException;

	/**
	 * Initializes the component by calling {@link #initialize(Object)} without
	 * you to require catching an {@link ConfigureException}.
	 * 
	 * @param aContext The context to be passed to the implementing instance.
	 * 
	 * @throws InitializeRuntimeException encapsulates the aCause and is thrown
	 *         upon encountering a {@link InitializeException} exception
	 */
	default void initializeUnchecked( CTX aContext ) {
		try {
			initialize( aContext );
		}
		catch ( InitializeException e ) {
			throw new InitializeRuntimeException( e );
		}
	}

	/**
	 * The {@link ConfigureAutomaton} interface defines those methods related to
	 * the initialize life-cycle with a provided context.
	 * 
	 * @param <CTX> the context used to initialize the implementing instance.
	 */
	public interface ConfigureAutomaton<CTX> extends Configurable<CTX>, InitializedAccessor {

		/**
		 * Determines whether the component may get initialized.
		 *
		 * @param aContext The context to be passed to the implementing
		 *        instance.
		 * 
		 * @return True if {@link #initialize(Object)} is possible.
		 */
		boolean isInitalizable( CTX aContext );
	}

	/**
	 * To enable the {@link Initializable} functionality to be invoked in a
	 * builder chain.
	 *
	 * @param <CTX> the context used to initialize the implementing instance.
	 * @param <B> The instance to be returned on which to apply succeeding
	 *        builder operations.
	 */
	public interface ConfigureBuilder<CTX, B extends ConfigureBuilder<CTX, B>> {

		/**
		 * Builder method for the {@link Configurable#initialize(Object)}
		 * method.
		 *
		 * @param aContext The context to be passed to the implementing
		 *        instance.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws ConfigureException Thrown in case initializing fails.
		 */
		B withInitialize( CTX aContext ) throws ConfigureException;

		/**
		 * Initializes the component by calling {@link #withInitialize(Object)}
		 * without you to require catching an {@link ConfigureException}.
		 * 
		 * @param aContext The context to be passed to the implementing
		 *        instance.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws ConfigureRuntimeException encapsulates the aCause and is
		 *         thrown upon encountering a {@link ConfigureException}
		 *         exception
		 */
		default B withInitializeUnchecked( CTX aContext ) {
			try {
				return withInitialize( aContext );
			}
			catch ( ConfigureException e ) {
				throw new ConfigureRuntimeException( aContext, e );
			}
		}
	}
}
