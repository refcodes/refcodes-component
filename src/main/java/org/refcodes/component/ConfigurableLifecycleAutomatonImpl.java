// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.component.ConfigurableLifecycleComponent.ConfigurableLifecycleAutomaton;
import org.refcodes.component.LifecycleComponent.LifecycleAutomaton;

/**
 * This class implements a {@link ConfigurableLifecycleAutomaton}.
 *
 * @param <CTX> the context used to initialize the implementing instance.
 */
public class ConfigurableLifecycleAutomatonImpl<CTX> implements ConfigurableLifecycleAutomaton<CTX> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private LifecycleStatus _lifeCycleStatus = LifecycleStatus.NONE;

	private ConfigurableLifecycleComponent<CTX> _configurableLifecycleComponent = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Empty constructor, such {@link LifecycleAutomaton} cannot do much more
	 * than decline the various {@link LifecycleStatus} states for you.
	 */
	public ConfigurableLifecycleAutomatonImpl() {}

	/**
	 * This constructor uses a {@link LifecycleStatus} for wrapping it inside
	 * the {@link ConfigurableLifecycleAutomatonImpl}, making sure of obeying
	 * and guarding the correct {@link LifecycleStatus}'s order of
	 * {@link LifecycleStatus} states for you.
	 * 
	 * @param aLifeConfigurableCycleComponent The component to be guarded
	 *        regarding the correct declination of the {@link LifecycleStatus}
	 *        states.
	 */
	public ConfigurableLifecycleAutomatonImpl( ConfigurableLifecycleComponent<CTX> aLifeConfigurableCycleComponent ) {
		_configurableLifecycleComponent = aLifeConfigurableCycleComponent;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized LifecycleStatus getLifecycleStatus() {
		return _lifeCycleStatus;
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONVENIANCE METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void initialize( CTX aContext ) throws InitializeException {
		if ( !isInitalizable( aContext ) ) {
			throw new ConfigureException( aContext, "Cannot initialize (configure) as the component is in status <" + _lifeCycleStatus + "> which is not the appropriate status for initializing (configuring)." );
		}
		if ( _configurableLifecycleComponent != null ) {
			_configurableLifecycleComponent.initialize( aContext );
		}
		_lifeCycleStatus = LifecycleStatus.INITIALIZED;
	}

	/**
	 * Checks if is initalizable.
	 *
	 * @param aContext the context
	 * 
	 * @return true, if is initalizable
	 */
	@SuppressWarnings("unchecked")
	@Override
	public synchronized boolean isInitalizable( CTX aContext ) {
		final boolean isConfigurable = ( _lifeCycleStatus == LifecycleStatus.NONE ) || ( isDestroyed() );
		if ( isConfigurable && _configurableLifecycleComponent != null && _configurableLifecycleComponent instanceof ConfigureAutomaton<?> ) {
			final ConfigureAutomaton<CTX> theConfigureAutomaton = (ConfigureAutomaton<CTX>) _configurableLifecycleComponent;
			return theConfigureAutomaton.isInitalizable( aContext );
		}
		return isConfigurable;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isInitialized() {
		return ( _lifeCycleStatus == LifecycleStatus.INITIALIZED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isStartable() {
		return ( isInitialized() || isStopped() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void start() throws StartException {
		if ( !isStartable() ) {
			throw new StartException( "Cannot start as the component is in status <" + _lifeCycleStatus + "> which is not the appropriate status for starting." );
		}
		if ( _configurableLifecycleComponent != null ) {
			_configurableLifecycleComponent.start();
		}
		_lifeCycleStatus = LifecycleStatus.STARTED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isRunning() {
		return ( _lifeCycleStatus == LifecycleStatus.STARTED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isPausable() {
		return isRunning();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void pause() throws PauseException {
		if ( !isPausable() ) {
			throw new PauseException( "Cannot pause as the component is in status <" + _lifeCycleStatus + "> which is not the appropriate status for pausing." );
		}
		if ( _configurableLifecycleComponent != null ) {
			_configurableLifecycleComponent.pause();
		}
		_lifeCycleStatus = LifecycleStatus.PAUSED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isPaused() {
		return ( _lifeCycleStatus == LifecycleStatus.PAUSED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isResumable() {
		return isPaused();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void resume() throws ResumeException {
		if ( !isResumable() ) {
			throw new ResumeException( "Cannot resume as the component is in status <" + _lifeCycleStatus + "> which is not the appropriate status for resuming." );
		}
		if ( _configurableLifecycleComponent != null ) {
			_configurableLifecycleComponent.resume();
		}
		_lifeCycleStatus = LifecycleStatus.STARTED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isStoppable() {
		return isRunning() || isPaused();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void stop() throws StopException {
		if ( !isStoppable() ) {
			throw new StopException( "Cannot stop as the component is in status <" + _lifeCycleStatus + "> which is not the appropriate status for stopping." );
		}
		if ( _configurableLifecycleComponent != null ) {
			_configurableLifecycleComponent.stop();
		}
		_lifeCycleStatus = LifecycleStatus.STOPPED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isStopped() {
		return ( _lifeCycleStatus == LifecycleStatus.STOPPED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isDestroyable() {
		return isStopped();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void destroy() {
		if ( !isDestroyable() ) {
			return;
		}
		if ( _configurableLifecycleComponent != null ) {
			_configurableLifecycleComponent.destroy();
		}
		_lifeCycleStatus = LifecycleStatus.DESTROYED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isDestroyed() {
		return ( _lifeCycleStatus == LifecycleStatus.DESTROYED );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Provides access to the {@link LifecycleComponent} instance.
	 * 
	 * @return The {@link LifecycleComponent} instance being set.
	 */
	protected ConfigurableLifecycleComponent<CTX> getLifecycleComponent() {
		return _configurableLifecycleComponent;
	}
}
