package org.refcodes.component;

import java.io.IOException;

import org.refcodes.component.ConnectionOpenable.ConnectionOpenAutomaton;

/**
 * The {@link BidirectionalConnectionOpenableHandle} interface defines those
 * methods related to the handle based open/connect life-cycle.
 * <p>
 * The handle reference requires the {@link BidirectionalConnectionOpenable}
 * interface to be implemented.
 *
 * @param <H> The type of the handle.
 * @param <INPUT> The type of the input connection to be used.
 * @param <OUTPUT> The type of the output connection to be used.
 */
public interface BidirectionalConnectionOpenableHandle<H, INPUT, OUTPUT> {

	/**
	 * Determines whether the handle reference is
	 * {@link BidirectionalConnectionOpenable} by implementing the
	 * {@link BidirectionalConnectionOpenable} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException in case the handle is unknown.
	 */
	boolean hasBidirectionalConnectionOpenable( H aHandle );

	/**
	 * Open/connect the component identified by the given handle.
	 *
	 * @param aHandle The handle identifying the component.
	 * @param aInputConnection The input connection used for opening the
	 *        connection.
	 * @param aOutputConnection The output connection used for opening the
	 *        connection.
	 * 
	 * @throws IOException in case opening/connecting fails.
	 * @throws UnknownHandleRuntimeException in case the given handle is
	 *         unknown.
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 * @throws IllegalHandleStateChangeRuntimeException Thrown in case a state
	 *         change is not possible due to the current state the referenced
	 *         component is in.
	 */
	void open( H aHandle, INPUT aInputConnection, OUTPUT aOutputConnection ) throws IOException;

	/**
	 * The {@link BidirectionalConnectionOpenAutomatonHandle} interface defines
	 * those methods related to the handle based open/connect life-cycle. The
	 * handle reference requires the {@link ConnectionOpenAutomaton} interface
	 * to be implemented.
	 * 
	 * @param <H> The type of the handle.
	 * @param <INPUT> The type of the input connection to be used.
	 * @param <OUTPUT> The type of the output connection to be used.
	 */
	public interface BidirectionalConnectionOpenAutomatonHandle<H, INPUT, OUTPUT> extends BidirectionalConnectionOpenableHandle<H, INPUT, OUTPUT>, OpenedHandle<H> {

		/**
		 * Determines whether the handle reference is configurable by
		 * implementing the {@link ConnectionOpenAutomaton} interface.
		 * 
		 * @param aHandle The handle to test whether the reference provides the
		 *        according functionality.
		 * 
		 * @return True in case the reference provides the according
		 *         functionality.
		 * 
		 * @throws UnknownHandleRuntimeException in case the handle is unknown.
		 */
		boolean hasBidirectionalConnectionOpenAutomaton( H aHandle );

		/**
		 * Determines whether the component identified by the given handle may
		 * get opened/connected.
		 *
		 * @param aHandle The handle identifying the component.
		 * @param aInputConnection The input connection used for opening the
		 *        connection.
		 * @param aOutputConnection The output connection used for opening the
		 *        connection.
		 * 
		 * @return True if {@link #open(Object, Object, Object)} is possible.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isOpenable( H aHandle, INPUT aInputConnection, OUTPUT aOutputConnection );
	}
}
