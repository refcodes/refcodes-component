// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import java.io.IOException;

import org.refcodes.exception.RuntimeIOException;

/**
 * This mixin might be implemented by a component in order to provide flush
 * facilities. A dedicated interface has been created just extending Java's
 * {@link java.io.Flushable} interface to lift it onto the component's layer.
 * <p>
 * ATTENTION: When implementing flush functionality, use this {@link Flushable},
 * though when testing for reset functionality, use {@link java.io.Flushable}
 * (<code>instanceof</code>)!
 */
public interface Flushable extends java.io.Flushable {

	/**
	 * {@inheritDoc}
	 */
	@Override
	void flush() throws IOException;

	/**
	 * Flushes the component by calling {@link #flush()} without you to require
	 * catching a {@link IOException}.
	 * 
	 * @throws RuntimeIOException encapsulates the aCause and is thrown upon
	 *         encountering a {@link IOException} exception
	 */
	default void flushUnchecked() {
		try {
			flush();
		}
		catch ( IOException e ) {
			throw new RuntimeIOException( e );
		}
	}

	/**
	 * Determines whether the implementing instance can safely be flushed. When
	 * false is returned, then calling {@link #flush()} would result in an
	 * {@link IOException} exception to be thrown.
	 * 
	 * @return True in case {@link #flush()} can be called without throwing an
	 *         {@link IOException}.
	 */
	default boolean isFlushable() {
		return true;
	}

	/**
	 * The Interface FlushBuilder.
	 *
	 * @param <B> the generic type
	 */
	public interface FlushBuilder<B extends FlushBuilder<B>> extends Flushable {

		/**
		 * With flush as of {@link #flush()}.
		 *
		 * @return The implementing instance as of the Builder-Pattern.
		 * 
		 * @throws IOException in case flushing failed as of some reason.
		 */
		@SuppressWarnings("unchecked")
		default B withFlush() throws IOException {
			flush();
			return (B) this;
		}

		/**
		 * Flushes the component by calling {@link #withFlush()} without you to
		 * require catching a {@link IOException}.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws RuntimeIOException encapsulates the aCause and is thrown upon
		 *         encountering a {@link IOException} exception
		 */
		default B withFlushUnchecked() {
			try {
				return withFlush();
			}
			catch ( IOException e ) {
				throw new RuntimeIOException( e );
			}
		}
	}
}
