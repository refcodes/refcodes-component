// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.component.DigestException.DigestRuntimeException;

/**
 * A {@link Digester} provides the functionality to consume a job (which is
 * pushed from the outside) for further processing. Job instances may reflect
 * (parts of) a protocol or signals from another system. The {@link Digester} is
 * similar to an observer (listener) with the semantic difference of usually
 * being bound to exactly one job providing system (whereas an observer may be
 * one of many observers observing the same observable).
 *
 * @param <J> the generic type
 */
public interface Digester<J> {

	/**
	 * The {@link #digest(Object)} method is invoked in order to trigger
	 * processing of the provided job, e.g. start execution depending on the job
	 * instance being provided from the outside.
	 * 
	 * @param aJob The job to be digested.
	 * 
	 * @throws DigestException thrown in case digesting (processing) a job by a
	 *         {@link Digester#digest(Object)} caused problems;he aCause is
	 *         usually wrapped by this {@link Exception}.
	 */
	void digest( J aJob ) throws DigestException;

	/**
	 * Digests the job by calling {@link #digest(Object)} without you to require
	 * catching a {@link DigestException}.
	 * 
	 * @param aJob The job to be digested.
	 * 
	 * @throws DigestRuntimeException encapsulates the aCause and is thrown upon
	 *         encountering a {@link DigestException} exception
	 */
	default void digestUnchecked( J aJob ) {
		try {
			digest( aJob );
		}
		catch ( DigestException e ) {
			throw new DigestRuntimeException( e );
		}
	}
}
