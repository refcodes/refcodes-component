// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * Provides an accessor for a handle property for e.g. key / handle pair.
 *
 * @param <H> The type of the handle to be used.
 */
public interface HandleAccessor<H> {

	/**
	 * Retrieves the handle from the handle property.
	 * 
	 * @return The handle stored by the handle property.
	 */
	H getHandle();

	/**
	 * Provides a mutator for a handle property for e.g. key / handle pair.
	 * 
	 * @param <H> The type of the handle property.
	 */
	public interface HandleMutator<H> {

		/**
		 * Sets the handle for the handle property.
		 * 
		 * @param aHandle The handle to be stored by the handle property.
		 */
		void setHandle( H aHandle );
	}

	/**
	 * Provides a handle property for e.g. key / handle pair.
	 * 
	 * @param <H> The type of the handle property.
	 */
	public interface HandleProperty<H> extends HandleAccessor<H>, HandleMutator<H> {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given handle (setter) as of
		 * {@link #setHandle(Object)} and returns the very same value (getter).
		 * 
		 * @param aHandle The handle to set (via {@link #setHandle(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default H letHandle( H aHandle ) {
			setHandle( aHandle );
			return aHandle;
		}
	}
}
