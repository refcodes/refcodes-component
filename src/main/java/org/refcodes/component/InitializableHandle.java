package org.refcodes.component;

import org.refcodes.component.Initializable.InitializeAutomaton;
import org.refcodes.component.InitializeException.InitializeRuntimeException;

/**
 * The {@link InitializableHandle} interface defines those methods related to
 * the handle based initialize life-cycle.
 * <p>
 * The handle reference requires the {@link Initializable} interface to be
 * implemented.
 *
 * @param <H> The type of the handle.
 */
public interface InitializableHandle<H> {

	/**
	 * Determines whether the handle reference is initializeable by implementing
	 * the {@link Initializable} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException in case the handle is unknown.
	 */
	boolean hasInitalizable( H aHandle );

	/**
	 * Initialize the component identified by the given handle.
	 *
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws InitializeException in case initializing fails.
	 * @throws UnknownHandleRuntimeException in case the given handle is
	 *         unknown.
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 * @throws IllegalHandleStateChangeRuntimeException Thrown in case a state
	 *         change is not possible due to the current state the referenced
	 *         component is in.
	 */
	void initialize( H aHandle ) throws InitializeException;

	/**
	 * Initialize the component by calling {@link #initialize(Object)} without
	 * you to require catching an {@link InitializeException}.
	 * 
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws InitializeRuntimeException encapsulates the aCause and is thrown
	 *         upon encountering a {@link InitializeException} exception
	 */
	default void initializeUnchecked( H aHandle ) {
		try {
			initialize( aHandle );
		}
		catch ( InitializeException e ) {
			throw new InitializeRuntimeException( e );
		}
	}

	/**
	 * The {@link InitializeAutomatonHandle} interface defines those methods
	 * related to the handle based initialize life-cycle. The handle reference
	 * requires the {@link InitializeAutomaton} interface to be implemented.
	 * 
	 * @param <H> The type of the handle.
	 */
	public interface InitializeAutomatonHandle<H> extends InitializableHandle<H>, InitializedHandle<H> {

		/**
		 * Determines whether the handle reference is initalizable by
		 * implementing the {@link InitializeAutomaton} interface.
		 * 
		 * @param aHandle The handle to test whether the reference provides the
		 *        according functionality.
		 * 
		 * @return True in case the reference provides the according
		 *         functionality.
		 * 
		 * @throws UnknownHandleRuntimeException in case the handle is unknown.
		 */
		boolean hasInitializeAutomaton( H aHandle );

		/**
		 * Determines whether the component identified by the given handle may
		 * get initialized.
		 *
		 * @param aHandle The handle identifying the component.
		 * 
		 * @return True if {@link #initialize(Object)} is possible.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isInitalizable( H aHandle );
	}
}
