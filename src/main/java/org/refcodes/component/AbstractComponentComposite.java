// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.refcodes.controlflow.ExecutionStrategy;

/**
 * The {@link AbstractComponentComposite} is an implementation of the
 * {@link ComponentComposite}. To make sure that the state change requests you
 * require are supported by the managed {@link Component} instances, specify the
 * according type <code>C</code> as generic type argument.
 *
 * @param <C> The type of the {@link Component} supported by the
 *        {@link AbstractComponentComposite}.
 */
public abstract class AbstractComponentComposite<C extends Component> implements ComponentComposite {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Set<C> _components;
	private ExecutionStrategy _strategy;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link AbstractComponentComposite} containing the provided
	 * components. The {@link ExecutionStrategy#JOIN} {@link ExecutionStrategy}
	 * is used by default.
	 * 
	 * @param aComponents The components to be managed by the
	 *        {@link AbstractComponentComposite}.
	 */
	public AbstractComponentComposite( Collection<C> aComponents ) {
		this( ExecutionStrategy.JOIN, aComponents );
	}

	/**
	 * Creates a {@link AbstractComponentComposite} containing the provided
	 * components.
	 * 
	 * @param aStrategy The {@link ExecutionStrategy} for executing the state
	 *        change requests.
	 * @param aComponents The components to be managed by the
	 *        {@link AbstractComponentComposite}.
	 */
	public AbstractComponentComposite( ExecutionStrategy aStrategy, Collection<C> aComponents ) {
		_components = new HashSet<>( aComponents );
		_strategy = aStrategy;
	}

	/**
	 * Creates a {@link AbstractComponentComposite} containing the provided
	 * components. The {@link ExecutionStrategy#JOIN} {@link ExecutionStrategy}
	 * is used by default.
	 * 
	 * @param aComponents The components to be managed by the
	 *        {@link AbstractComponentComposite}.
	 */
	@SafeVarargs
	public AbstractComponentComposite( C... aComponents ) {
		this( ExecutionStrategy.JOIN, aComponents );
	}

	/**
	 * Creates a {@link AbstractComponentComposite} containing the provided
	 * components.
	 * 
	 * @param aStrategy The {@link ExecutionStrategy} for executing the state
	 *        change requests.
	 * @param aComponents The components to be managed by the
	 *        {@link AbstractComponentComposite}.
	 */
	@SafeVarargs
	public AbstractComponentComposite( ExecutionStrategy aStrategy, C... aComponents ) {
		final List<C> theComponents = Arrays.asList( aComponents );
		_components = new HashSet<>( theComponents );
		_strategy = aStrategy;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		ComponentUtility.flush( _strategy, _components );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void decompose() {
		ComponentUtility.decompose( _strategy, _components );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		ComponentUtility.destroy( _strategy, _components );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() throws StopException {
		ComponentUtility.stop( _strategy, _components );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void resume() throws ResumeException {
		ComponentUtility.resume( _strategy, _components );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pause() throws PauseException {
		ComponentUtility.pause( _strategy, _components );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() throws StartException {
		ComponentUtility.start( _strategy, _components );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize() throws InitializeException {
		ComponentUtility.initialize( _strategy, _components );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		ComponentUtility.reset( _strategy, _components );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		ComponentUtility.open( _strategy, _components );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		ComponentUtility.close( _strategy, _components );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		ComponentUtility.dispose( _strategy, _components );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Provides access to the {@link ExecutionStrategy}.
	 * 
	 * @return The {@link ExecutionStrategy} being set.
	 */
	protected ExecutionStrategy getExecutionStrategy() {
		return _strategy;
	}

	/**
	 * Provides access to the {@link Component} instances.
	 * 
	 * @return The {@link Component} instances being set.
	 */
	protected Set<C> getComponents() {
		return _components;
	}

	/**
	 * The {@link ExtendedCompositeComponentImpl} is an implementation of the
	 * {@link ExtendedComponentComposite}. To make sure that the state change
	 * requests you require are supported by the managed {@link Component}
	 * instances, specify the according type <code>C</code> as generic type
	 * argument.
	 *
	 * @param <C> The type of the {@link Component} supported by the
	 *        {@link ExtendedCompositeComponentImpl}.
	 * @param <CTX> the generic type
	 * @param <CON> the generic type
	 */
	public static class ExtendedCompositeComponentImpl<C extends Component, CTX, CON> extends AbstractComponentComposite<C> implements ExtendedComponentComposite<CTX, CON> {

		// /////////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * Instantiates a new extended composite component impl.
		 *
		 * @param aComponents the components
		 */
		@SafeVarargs
		public ExtendedCompositeComponentImpl( C... aComponents ) {
			super( aComponents );
		}

		/**
		 * Instantiates a new extended composite component impl.
		 *
		 * @param aComponents the components
		 */
		public ExtendedCompositeComponentImpl( Collection<C> aComponents ) {
			super( aComponents );
		}

		/**
		 * Instantiates a new extended composite component impl.
		 *
		 * @param aStrategy the strategy
		 * @param aComponents the components
		 */
		@SafeVarargs
		public ExtendedCompositeComponentImpl( ExecutionStrategy aStrategy, C... aComponents ) {
			super( aStrategy, aComponents );
		}

		/**
		 * Instantiates a new extended composite component impl.
		 *
		 * @param aStrategy the strategy
		 * @param aComponents the components
		 */
		public ExtendedCompositeComponentImpl( ExecutionStrategy aStrategy, Collection<C> aComponents ) {
			super( aStrategy, aComponents );
		}

		// /////////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void initialize( CTX aContext ) throws ConfigureException {
			ComponentUtility.initialize( getExecutionStrategy(), aContext, getComponents() );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void open( CON aConnection ) throws IOException {
			ComponentUtility.open( getExecutionStrategy(), aConnection, getComponents() );
		}
	}
}
