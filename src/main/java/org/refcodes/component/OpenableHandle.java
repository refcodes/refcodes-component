package org.refcodes.component;

import java.io.IOException;

import org.refcodes.component.ConnectionOpenable.ConnectionOpenAutomaton;
import org.refcodes.exception.RuntimeIOException;

/**
 * The {@link OpenableHandle} interface defines those methods related to the
 * handle based open/connect life-cycle.
 * <p>
 * The handle reference requires the {@link Openable} interface to be
 * implemented.
 *
 * @param <H> The type of the handle.
 */
public interface OpenableHandle<H> {

	/**
	 * Determines whether the handle reference is openable by implementing the
	 * {@link Openable} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException in case the handle is unknown.
	 */
	boolean hasOpenable( H aHandle );

	/**
	 * Open/connect the component identified by the given handle.
	 *
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws IOException in case opening/connecting fails.
	 * @throws UnknownHandleRuntimeException in case the given handle is
	 *         unknown.
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 * @throws IllegalHandleStateChangeRuntimeException Thrown in case a state
	 *         change is not possible due to the current state the referenced
	 *         component is in.
	 */
	void open( H aHandle ) throws IOException;

	/**
	 * Opens the component by calling {@link #open(Object)} without you to
	 * require catching an {@link IOException}.
	 * 
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws RuntimeIOException encapsulates the aCause and is thrown upon
	 *         encountering a {@link IOException} exception
	 */
	default void openUnchecked( H aHandle ) {
		try {
			open( aHandle );
		}
		catch ( IOException e ) {
			throw new RuntimeIOException( e );
		}
	}

	/**
	 * The {@link OpenAutomatonHandle} interface defines those methods related
	 * to the handle based open/connect life-cycle. The handle reference
	 * requires the {@link ConnectionOpenAutomaton} interface to be implemented.
	 * 
	 * @param <H> The type of the handle.
	 */
	public interface OpenAutomatonHandle<H> extends OpenableHandle<H>, OpenedHandle<H> {

		/**
		 * Determines whether the handle reference is openable by implementing
		 * the {@link ConnectionOpenAutomaton} interface.
		 * 
		 * @param aHandle The handle to test whether the reference provides the
		 *        according functionality.
		 * 
		 * @return True in case the reference provides the according
		 *         functionality.
		 * 
		 * @throws UnknownHandleRuntimeException in case the handle is unknown.
		 */
		boolean hasOpenAutomaton( H aHandle );

		/**
		 * Determines whether the component identified by the given handle may
		 * get opened/connected.
		 *
		 * @param aHandle The handle identifying the component.
		 * 
		 * @return True if {@link #open(Object)} is possible.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isOpenable( H aHandle );
	}
}
