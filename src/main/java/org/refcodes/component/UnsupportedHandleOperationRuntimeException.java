// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.component.ComponentRuntimeException.ComponentHandleRuntimeException;

/**
 * This exception is thrown in case a handle was provided whose reference does
 * not support a given operation.
 */
public class UnsupportedHandleOperationRuntimeException extends ComponentHandleRuntimeException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public UnsupportedHandleOperationRuntimeException( Object aHandle, String aMessage, String aErrorCode ) {
		super( aMessage, aHandle, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnsupportedHandleOperationRuntimeException( Object aHandle, String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aHandle, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnsupportedHandleOperationRuntimeException( Object aHandle, String aMessage, Throwable aCause ) {
		super( aMessage, aHandle, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnsupportedHandleOperationRuntimeException( Object aHandle, String aMessage ) {
		super( aMessage, aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnsupportedHandleOperationRuntimeException( Object aHandle, Throwable aCause, String aErrorCode ) {
		super( aHandle, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnsupportedHandleOperationRuntimeException( Object aHandle, Throwable aCause ) {
		super( aHandle, aCause );
	}
}