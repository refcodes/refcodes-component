package org.refcodes.component;

import org.refcodes.component.Pausable.PauseAutomaton;
import org.refcodes.component.PauseException.PauseRuntimeException;

/**
 * The {@link PausableHandle} interface defines those methods related to the
 * handle based pause life-cycle.
 * <p>
 * The handle reference requires the {@link Pausable} interface to be
 * implemented.
 *
 * @param <H> The type of the handle.
 */
public interface PausableHandle<H> {

	/**
	 * Determines whether the handle reference is pausable by implementing the
	 * {@link Pausable} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException in case the handle is unknown.
	 */
	boolean hasPausable( H aHandle );

	/**
	 * Pauses the component identified by the given handle.
	 *
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws PauseException in case pausing fails.
	 * @throws UnknownHandleRuntimeException in case the given handle is
	 *         unknown.
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 * @throws IllegalHandleStateChangeRuntimeException Thrown in case a state
	 *         change is not possible due to the current state the referenced
	 *         component is in.
	 */
	void pause( H aHandle ) throws PauseException;

	/**
	 * Pauses the component by calling {@link #pause(Object)} without you to
	 * require catching an {@link PauseException}.
	 * 
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws PauseRuntimeException encapsulates the aCause and is thrown upon
	 *         encountering a {@link PauseException} exception
	 */
	default void pauseUnchecked( H aHandle ) {
		try {
			pause( aHandle );
		}
		catch ( PauseException e ) {
			throw new PauseRuntimeException( e );
		}
	}

	/**
	 * The {@link PauseAutomatonHandle} interface defines those methods related
	 * to the handle based pause life-cycle. The handle reference requires the
	 * {@link PauseAutomaton} interface to be implemented.
	 * 
	 * @param <H> The type of the handle.
	 */
	public interface PauseAutomatonHandle<H> extends PausableHandle<H> {

		/**
		 * Determines whether the handle reference is pausable by implementing
		 * the {@link PauseAutomaton} interface.
		 * 
		 * @param aHandle The handle to test whether the reference provides the
		 *        according functionality.
		 * 
		 * @return True in case the reference provides the according
		 *         functionality.
		 * 
		 * @throws UnknownHandleRuntimeException in case the handle is unknown.
		 */
		boolean hasPauseAutomaton( H aHandle );

		/**
		 * Determines whether the component identified by the given handle may
		 * get paused.
		 *
		 * @param aHandle The handle identifying the component.
		 * 
		 * @return True if {@link #pause(Object)} is possible.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isPausable( H aHandle );

		/**
		 * Determines whether the component identified by the given handle is
		 * paused.
		 *
		 * @param aHandle The handle identifying the component.
		 * 
		 * @return True in case of being paused, else false.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isPaused( H aHandle );
	}
}
