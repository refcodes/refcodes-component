// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * Provides an accessor for a context property for e.g. a service or a
 * component.
 *
 * @param <CTX> The type of the context to be used.
 */
public interface ContextAccessor<CTX> {

	/**
	 * Retrieves the context from the context property.
	 * 
	 * @return The context stored by the context property.
	 */
	CTX getContext();

	/**
	 * Provides a mutator for a context property for e.g. a service or a
	 * component.
	 * 
	 * @param <CTX> The type of the context property.
	 */
	public interface ContextMutator<CTX> {

		/**
		 * Sets the context for the context property.
		 * 
		 * @param aContext The context to be stored by the context property.
		 */
		void setContext( CTX aContext );
	}

	/**
	 * Provides a builder method for a context property returning the builder
	 * for applying multiple build operations.
	 *
	 * @param <CTX> the generic type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ContextBuilder<CTX, B extends ContextBuilder<CTX, B>> {

		/**
		 * Sets the context for the context property.
		 * 
		 * @param aContext The context to be stored by the context property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withContext( CTX aContext );
	}

	/**
	 * Provides a context property for e.g. a service or a component.
	 * 
	 * @param <CTX> The type of the context property.
	 */
	public interface ContextProperty<CTX> extends ContextAccessor<CTX>, ContextMutator<CTX> {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given context (setter) as of
		 * {@link #setContext(Object)} and returns the very same value (getter).
		 * 
		 * @param aContext The context to set (via {@link #setContext(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default CTX letContext( CTX aContext ) {
			setContext( aContext );
			return aContext;
		}
	}
}
