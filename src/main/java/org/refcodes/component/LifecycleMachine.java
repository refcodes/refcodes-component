// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * The {@link LifecycleMachine} implements a {@link LifecycleAutomaton}. The
 * implementation is suffixed with "Machine" instead of "Automaton" for
 * differentiation with any interface of similar (same) name.
 */
public class LifecycleMachine implements org.refcodes.component.LifecycleComponent.LifecycleAutomaton {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private LifecycleStatus _lifeCycleStatus = LifecycleStatus.NONE;
	private LifecycleComponent _lifeCycleComponent = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Empty constructor, such {@link LifecycleAutomaton} cannot do much more
	 * than decline the various {@link LifecycleStatus} states for you.
	 */
	public LifecycleMachine() {}

	/**
	 * This constructor uses a {@link LifecycleStatus} for wrapping it inside
	 * the {@link LifecycleAutomaton}, making sure of obeying and guarding the
	 * correct {@link LifecycleStatus}'s order of {@link LifecycleStatus} states
	 * for you.
	 * 
	 * @param aLifecycleComponent The component to be guarded regarding the
	 *        correct declination of the {@link LifecycleStatus} states.
	 */
	public LifecycleMachine( LifecycleComponent aLifecycleComponent ) {
		_lifeCycleComponent = aLifecycleComponent;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized LifecycleStatus getLifecycleStatus() {
		return _lifeCycleStatus;
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONVENIANCE METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void initialize() throws InitializeException {
		if ( !isInitalizable() ) {
			throw new InitializeException( "Cannot initialize as the component is in status <" + _lifeCycleStatus + "> which is not the appropriate status for initializing." );
		}
		if ( _lifeCycleComponent != null ) {
			_lifeCycleComponent.initialize();
		}
		_lifeCycleStatus = LifecycleStatus.INITIALIZED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isInitalizable() {
		return ( _lifeCycleStatus == LifecycleStatus.NONE || _lifeCycleStatus == LifecycleStatus.ERROR ) || ( isDestroyed() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isInitialized() {
		return ( _lifeCycleStatus == LifecycleStatus.INITIALIZED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isStartable() {
		return ( isInitialized() || isStopped() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void start() throws StartException {
		if ( !isStartable() ) {
			throw new StartException( "Cannot start as the component is in status <" + _lifeCycleStatus + "> which is not the appropriate status for starting." );
		}
		if ( _lifeCycleComponent != null ) {
			_lifeCycleComponent.start();
		}
		_lifeCycleStatus = LifecycleStatus.STARTED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isRunning() {
		return ( _lifeCycleStatus == LifecycleStatus.STARTED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isPausable() {
		return isRunning();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void pause() throws PauseException {
		if ( !isPausable() ) {
			throw new PauseException( "Cannot pause as the component is in status <" + _lifeCycleStatus + "> which is not the appropriate status for pausing." );
		}
		if ( _lifeCycleComponent != null ) {
			_lifeCycleComponent.pause();
		}
		_lifeCycleStatus = LifecycleStatus.PAUSED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isPaused() {
		return ( _lifeCycleStatus == LifecycleStatus.PAUSED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isResumable() {
		return isPaused();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void resume() throws ResumeException {
		if ( !isResumable() ) {
			throw new ResumeException( "Cannot resume as the component is in status <" + _lifeCycleStatus + "> which is not the appropriate status for resuming." );
		}
		if ( _lifeCycleComponent != null ) {
			_lifeCycleComponent.resume();
		}
		_lifeCycleStatus = LifecycleStatus.STARTED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isStoppable() {
		return isRunning() || isPaused();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void stop() throws StopException {
		if ( !isStoppable() ) {
			throw new StopException( "Cannot stop as the component is in status <" + _lifeCycleStatus + "> which is not the appropriate status for stopping." );
		}
		if ( _lifeCycleComponent != null ) {
			_lifeCycleComponent.stop();
		}
		_lifeCycleStatus = LifecycleStatus.STOPPED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isStopped() {
		return ( _lifeCycleStatus == LifecycleStatus.STOPPED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isDestroyable() {
		return isStopped();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void destroy() {
		if ( !isDestroyable() ) {
			return;
		}
		if ( _lifeCycleComponent != null ) {
			_lifeCycleComponent.destroy();
		}
		_lifeCycleStatus = LifecycleStatus.DESTROYED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean isDestroyed() {
		return ( _lifeCycleStatus == LifecycleStatus.DESTROYED );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Provides access to the {@link LifecycleComponent} instance.
	 * 
	 * @return The {@link LifecycleComponent} instance being set.
	 */
	protected LifecycleComponent getLifecycleComponent() {
		return _lifeCycleComponent;
	}

	/**
	 * Provides means to set the {@link LifecycleStatus} manually.
	 * 
	 * @param aStatus The {@link LifecycleStatus} to be set.
	 */
	protected void setLifecycleStatus( LifecycleStatus aStatus ) {
		_lifeCycleStatus = aStatus;
	}

	/**
	 * Opens the otherwise protected
	 * {@link #setLifecycleStatus(LifecycleStatus)} to be public in order to
	 * force any lifecycle status to be set.
	 */
	public static class ManualLifecycleMachine extends LifecycleMachine implements LifecycleStatusMutator {

		/**
		 * Empty constructor, such {@link ManualLifecycleMachine} cannot do much
		 * more than decline the various {@link LifecycleStatus} states for you.
		 */
		public ManualLifecycleMachine() {}

		/**
		 * This constructor uses a {@link ManualLifecycleMachine} for wrapping
		 * it inside the {@link LifecycleAutomaton}, making sure of obeying and
		 * guarding the correct {@link LifecycleStatus}'s order of
		 * {@link LifecycleStatus} states for you.
		 * 
		 * @param aLifecycleComponent The component to be guarded regarding the
		 *        correct declination of the {@link LifecycleStatus} states.
		 */

		public ManualLifecycleMachine( LifecycleComponent aLifecycleComponent ) {
			super( aLifecycleComponent );
		}

		/**
		 * Method to force any {@link LifecycleStatus} to be set with no rules
		 * being applied. {@inheritDoc}
		 * 
		 * @param aStatus The status to be "forced".
		 */
		@Override
		public void setLifecycleStatus( LifecycleStatus aStatus ) {
			super.setLifecycleStatus( aStatus );
		}
	}
}
