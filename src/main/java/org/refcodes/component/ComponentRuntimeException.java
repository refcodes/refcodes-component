// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.exception.AbstractRuntimeException;

/**
 * This exception is the base runtime exception for the component package.
 */
public abstract class ComponentRuntimeException extends AbstractRuntimeException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public ComponentRuntimeException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ComponentRuntimeException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ComponentRuntimeException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public ComponentRuntimeException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public ComponentRuntimeException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ComponentRuntimeException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * This exception is the base exception for handle related runtime
	 * exceptions.
	 */
	@SuppressWarnings("rawtypes")
	protected abstract static class ComponentHandleRuntimeException extends ComponentRuntimeException implements HandleAccessor {

		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////////

		protected Object _handle;

		// /////////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 * 
		 * @param aHandle The handle involved in this exception.
		 */
		public ComponentHandleRuntimeException( String aMessage, Object aHandle, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_handle = aHandle;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aHandle The handle involved in this exception.
		 */
		public ComponentHandleRuntimeException( String aMessage, Object aHandle, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_handle = aHandle;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aHandle The handle involved in this exception.
		 */
		public ComponentHandleRuntimeException( String aMessage, Object aHandle, Throwable aCause ) {
			super( aMessage, aCause );
			_handle = aHandle;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aHandle The handle involved in this exception.
		 */
		public ComponentHandleRuntimeException( String aMessage, Object aHandle ) {
			super( aMessage );
			_handle = aHandle;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aHandle The handle involved in this exception.
		 */
		public ComponentHandleRuntimeException( Object aHandle, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_handle = aHandle;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aHandle The handle involved in this exception.
		 */
		public ComponentHandleRuntimeException( Object aHandle, Throwable aCause ) {
			super( aCause );
			_handle = aHandle;
		}

		// /////////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object getHandle() {
			return _handle;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getPatternArguments() {
			return new Object[] { _handle };
		}
	}
}
