// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import java.io.IOException;

import org.refcodes.exception.RuntimeIOException;

/**
 * This mixin might be implemented by a component in order to provide reload
 * facilities.
 */
public interface Reloadable {

	/**
	 * Reloads the (state of the) component.
	 * 
	 * @throws IOException in case reloading failed as of some reason.
	 */
	void reload() throws IOException;

	/**
	 * Reloads the component by calling {@link #reload()} without you to require
	 * catching a {@link IOException}.
	 * 
	 * @throws RuntimeIOException encapsulates the aCause and is thrown upon
	 *         encountering a {@link IOException} exception
	 */
	default void reloadUnchecked() {
		try {
			reload();
		}
		catch ( IOException e ) {
			throw new RuntimeIOException( e );
		}
	}

	/**
	 * The Interface ReloadBuilder.
	 *
	 * @param <B> the generic type
	 */
	public interface ReloadBuilder<B extends ReloadBuilder<B>> extends Reloadable {

		/**
		 * With reload as of {@link #reload()}.
		 *
		 * @return The implementing instance as of the Builder-Pattern.
		 * 
		 * @throws IOException in case reloading failed as of some reason.
		 */
		@SuppressWarnings("unchecked")
		default B withReload() throws IOException {
			reload();
			return (B) this;
		}

		/**
		 * Reloads the component by calling {@link #withReload()} without you to
		 * require catching a {@link IOException}.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws RuntimeIOException encapsulates the aCause and is thrown upon
		 *         encountering a {@link IOException} exception
		 */
		default B withReloadUnchecked() {
			try {
				return withReload();
			}
			catch ( IOException e ) {
				throw new RuntimeIOException( e );
			}
		}
	}
}
