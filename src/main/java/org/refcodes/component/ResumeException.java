// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * Thrown in case resuming a component caused problems. Usually a method similar
 * to "resume()" throws such an exception.
 */
public class ResumeException extends LifecycleException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public ResumeException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ResumeException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ResumeException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public ResumeException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public ResumeException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ResumeException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * Unchecked exception with the same semantics as the
	 * {@link ResumeException}.
	 */
	public static class ResumeRuntimeException extends LifecycleRuntimeException {

		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		public ResumeRuntimeException( String aMessage, String aErrorCode ) {
			super( aMessage, aErrorCode );
		}

		/**
		 * {@inheritDoc}
		 */
		public ResumeRuntimeException( String aMessage, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
		}

		/**
		 * {@inheritDoc}
		 */
		public ResumeRuntimeException( String aMessage, Throwable aCause ) {
			super( aMessage, aCause );
		}

		/**
		 * {@inheritDoc}
		 */
		public ResumeRuntimeException( String aMessage ) {
			super( aMessage );
		}

		/**
		 * {@inheritDoc}
		 */
		public ResumeRuntimeException( Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
		}

		/**
		 * {@inheritDoc}
		 */
		public ResumeRuntimeException( Throwable aCause ) {
			super( aCause );
		}
	}
}
