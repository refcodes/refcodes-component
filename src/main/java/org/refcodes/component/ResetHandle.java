package org.refcodes.component;

import org.refcodes.mixin.Resetable;

/**
 * The {@link ResetHandle} interface defines those methods related to the handle
 * based reset operation.
 * <p>
 * The handle reference requires the {@link Resetable} interface to be
 * implemented.
 *
 * @param <H> The type of the handle.
 */
public interface ResetHandle<H> {

	/**
	 * Determines whether the handle reference provides resetting by
	 * implementing the {@link Resetable} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException Thrown in case the handle is
	 *         unknown (there is none reference for this handle).
	 */
	boolean hasReset( H aHandle );

	/**
	 * Resets the component identified by the given handle.
	 *
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws UnknownHandleRuntimeException Thrown in case the handle is
	 *         unknown (there is none reference for this handle).
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 */
	void reset( H aHandle );
}
