// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Provides an accessor for a connection property for e.g. an
 * {@link InputStream} or an {@link OutputStream}.
 *
 * @param <INPUT> The type of the input connection property.
 * @param <OUTPUT> The type of the output connection property.
 */
public interface BidirectionalConnectionAccessor<INPUT, OUTPUT> {

	/**
	 * Retrieves the input connection from the connection property.
	 * 
	 * @return The input connection stored by the connection property.
	 */
	INPUT getInputConnection();

	/**
	 * Retrieves the output connection from the connection property.
	 * 
	 * @return The output connection stored by the connection property.
	 */
	OUTPUT getOutputConnection();

	/**
	 * Provides a mutator for a connection property for e.g. an
	 * {@link InputStream} or an {@link OutputStream}.
	 * 
	 * @param <INPUT> The type of the input connection property.
	 * @param <OUTPUT> The type of the output connection property.
	 */
	public interface BidirectionalConnectionMutator<INPUT, OUTPUT> {

		/**
		 * Sets the input connection for the connection property.
		 * 
		 * @param aInputConnection The input connection to be stored by the
		 *        connection property.
		 */
		void setInputConnection( INPUT aInputConnection );

		/**
		 * Sets the output connection for the connection property.
		 * 
		 * @param aOutputConnection The output connection to be stored by the
		 *        connection property.
		 */
		void setOutputConnection( OUTPUT aOutputConnection );
	}

	/**
	 * Provides a connection property for e.g. an {@link InputStream} or an
	 * {@link OutputStream}.
	 * 
	 * @param <INPUT> The type of the input connection property.
	 * @param <OUTPUT> The type of the output connection property.
	 */
	public interface BidirectionalConnectionProperty<INPUT, OUTPUT> extends BidirectionalConnectionAccessor<INPUT, OUTPUT>, BidirectionalConnectionMutator<INPUT, OUTPUT> {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given input connection
		 * (setter) as of {@link #setInputConnection(Object)} and returns the
		 * very same value (getter).
		 * 
		 * @param aInputConnection The input connection to set (via
		 *        {@link #setInputConnection(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default INPUT letInputConnection( INPUT aInputConnection ) {
			setInputConnection( aInputConnection );
			return aInputConnection;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given output connection
		 * (setter) as of {@link #setOutputConnection(Object)} and returns the
		 * very same value (getter).
		 * 
		 * @param aOutputConnection The output connection to set (via
		 *        {@link #setOutputConnection(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default OUTPUT letOutputConnection( OUTPUT aOutputConnection ) {
			setOutputConnection( aOutputConnection );
			return aOutputConnection;
		}
	}
}
