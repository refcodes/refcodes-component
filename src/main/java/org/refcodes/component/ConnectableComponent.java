// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * Any {@link Component} which operates established connections may implement
 * the {@link ConnectableComponent} interface providing a minimal set of
 * functionality to work with connections; to test whether a connection is open
 * and to close an open connection.
 */
public interface ConnectableComponent extends Closable {

	/**
	 * The {@link ConnectableAutomaton} extends the {@link ConnectableComponent}
	 * with automaton functionality to get detailed status information regarding
	 * connections.
	 */
	public interface ConnectableAutomaton extends ConnectableComponent, OpenedAccessor, CloseAutomaton, ConnectionStatusAccessor {}
}