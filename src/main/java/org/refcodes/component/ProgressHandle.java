package org.refcodes.component;

/**
 * Whenever a handle is associated with a progress, then a component providing
 * such handles should implement this interface.
 * <p>
 * The handle reference requires the {@link ProgressAccessor} interface to be
 * implemented.
 *
 * @param <H> the generic type
 */
public interface ProgressHandle<H> {

	/**
	 * Determines whether the handle reference provides a progress by
	 * implementing the {@link ProgressAccessor} interface.
	 *
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException Thrown in case the handle is
	 *         unknown (there is none reference for this handle).
	 */
	boolean hasProgress( H aHandle );

	/**
	 * The progress of a handle can be queried by this method. A value of zero
	 * ("0") indicates that there was no progress so far, a value of one ("1")
	 * indicates that the progress is 100%. The handle association requires the
	 * {@link ProgressAccessor} interface to be implemented.
	 * 
	 * @param aHandle The handle for which to determine the current progress.
	 * 
	 * @return A value between zero ("0") and one ("1") determining the progress
	 *         related to the given handle.
	 * 
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 * @throws UnknownHandleRuntimeException Thrown in case the handle is
	 *         unknown (there is none reference for this handle).
	 */
	float getProgress( H aHandle );
}
