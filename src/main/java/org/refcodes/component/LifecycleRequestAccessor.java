// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * Provides an accessor for a {@link LifecycleRequest} property.
 */
public interface LifecycleRequestAccessor {

	/**
	 * Retrieves the {@link LifecycleRequest} property from the property.
	 * Determines in which {@link LifecycleRequest} is being requested for a
	 * component.
	 * 
	 * @return Returns the {@link LifecycleRequest} property stored by the
	 *         property.
	 */
	LifecycleRequest getLifecycleRequest();

	/**
	 * Provides a mutator for a {@link LifecycleRequest} property.
	 */
	public interface LifecycleRequestMutator {

		/**
		 * Sets the {@link LifecycleRequest} property for the property.
		 * 
		 * @param aLifecycleRequest The {@link LifecycleRequest} property to be
		 *        stored by the property.
		 */
		void setLifecycleRequest( LifecycleRequest aLifecycleRequest );
	}

	/**
	 * Provides a {@link LifecycleRequest} property.
	 */
	public interface LifecycleRequestProperty extends LifecycleRequestAccessor, LifecycleRequestMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link LifecycleRequest}
		 * (setter) as of {@link #setLifecycleRequest(LifecycleRequest)} and
		 * returns the very same value (getter).
		 * 
		 * @param aLifecycleRequest The {@link LifecycleRequest} to set (via
		 *        {@link #setLifecycleRequest(LifecycleRequest)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default LifecycleRequest letLifecycleRequest( LifecycleRequest aLifecycleRequest ) {
			setLifecycleRequest( aLifecycleRequest );
			return aLifecycleRequest;
		}
	}
}
