// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * Provides an accessor for a {@link ConnectionStatus} property.
 */
public interface ConnectionStatusAccessor extends OpenedAccessor {

	/**
	 * Retrieves the {@link ConnectionStatus} property from the property.
	 * Determines in which {@link ConnectionStatus} status a component is in.
	 * 
	 * @return Returns the {@link ConnectionStatus} property stored by the
	 *         property.
	 */
	ConnectionStatus getConnectionStatus();

	/**
	 * Determines whether the connection is ought to be open or not.
	 * 
	 * @return True in case the connection is open, else the connection is
	 *         either closed or of a status {@link ConnectionStatus#NONE}.
	 */
	@Override
	default boolean isOpened() {
		return getConnectionStatus() == ConnectionStatus.OPENED;
	}

	/**
	 * Provides a mutator for a {@link ConnectionStatus} property.
	 */
	public interface ConnectionStatusMutator {

		/**
		 * Sets the {@link ConnectionStatus} property for the property.
		 * 
		 * @param aConnectionStatus The {@link ConnectionStatus} property to be
		 *        stored by the property.
		 */
		void setConnectionStatus( ConnectionStatus aConnectionStatus );
	}

	/**
	 * Provides a {@link ConnectionStatus} property.
	 */
	public interface ConnectionStatusProperty extends ConnectionStatusAccessor, ConnectionStatusMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link ConnectionStatus}
		 * (setter) as of {@link #setConnectionStatus(ConnectionStatus)} and
		 * returns the very same value (getter).
		 * 
		 * @param aConnectionStatus The {@link ConnectionStatus} to set (via
		 *        {@link #setConnectionStatus(ConnectionStatus)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ConnectionStatus letConnectionStatus( ConnectionStatus aConnectionStatus ) {
			setConnectionStatus( aConnectionStatus );
			return aConnectionStatus;
		}
	}
}
