// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import java.io.IOException;

import org.refcodes.component.ConnectableComponent.ConnectableAutomaton;
import org.refcodes.component.Openable.OpenAutomaton;
import org.refcodes.controlflow.ControlFlowUtility;

/**
 * The {@link AbstractConnectableAutomaton} implements the very basic
 * {@link AbstractConnectableAutomaton} functionality.
 */
public abstract class AbstractConnectableAutomaton extends AbstractConnectable implements ConnectableAutomaton {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ConnectionStatus _connectionStatus = ConnectionStatus.NONE;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpened() {
		return _connectionStatus == ConnectionStatus.OPENED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosable() {
		return _connectionStatus == ConnectionStatus.OPENED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		ControlFlowUtility.throwIllegalStateException( !isOpened() && !isClosed() );
		setConnectionStatus( ConnectionStatus.CLOSED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosed() {
		return _connectionStatus == ConnectionStatus.CLOSED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _connectionStatus;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Checks if is openable.
	 *
	 * @return true, if is openable
	 * 
	 * @see OpenAutomaton#isOpenable()
	 */
	protected boolean isOpenable() {
		return !isOpened();
	}

	/**
	 * Open.
	 *
	 * @throws IOException the open exception
	 * 
	 * @see Openable#open()
	 */
	protected void open() throws IOException {
		if ( isOpened() ) {
			throw new IOException( "Unable to open the connection is is is ALREADY OPEN; connection status is " + getConnectionStatus() + "." );
		}
		setConnectionStatus( ConnectionStatus.OPENED );
	}

	/**
	 * Sets the {@link ConnectionStatus} property for the property.
	 * 
	 * @param aConnectionStatus The {@link ConnectionStatus} property to be
	 *        stored by the property.
	 */
	protected void setConnectionStatus( ConnectionStatus aConnectionStatus ) {
		_connectionStatus = aConnectionStatus;
	}
}
