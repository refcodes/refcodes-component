// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * Thrown in case initializing a component caused problems. Usually a method
 * similar to ""initialize(...)" throws such an exception.
 */
@SuppressWarnings("rawtypes")
public class ConfigureException extends InitializeException implements ContextAccessor {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Object _context;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aContext the context
	 * @param aMessage The aMessage describing this exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public ConfigureException( Object aContext, String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_context = aContext;
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aContext the context
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public ConfigureException( Object aContext, String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_context = aContext;
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aContext the context
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public ConfigureException( Object aContext, String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
		_context = aContext;
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aContext the context
	 * @param aMessage The aMessage describing this exception.
	 */
	public ConfigureException( Object aContext, String aMessage ) {
		super( aMessage );
		_context = aContext;
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aContext the context
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public ConfigureException( Object aContext, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_context = aContext;
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aContext the context
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public ConfigureException( Object aContext, Throwable aCause ) {
		super( aCause );
		_context = aContext;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getContext() {
		return _context;
	}

	/**
	 * Thrown in case initializing a component caused problems. Usually a method
	 * similar to ""initialize(...)" throws such an exception.
	 */
	public static class ConfigureRuntimeException extends InitializeRuntimeException implements ContextAccessor {

		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////////

		private final Object _context;

		// /////////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * Instantiates a new according exception.
		 *
		 * @param aContext the context
		 * @param aMessage The aMessage describing this exception.
		 * @param aErrorCode The error code identifying this exception.
		 */
		public ConfigureRuntimeException( Object aContext, String aMessage, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_context = aContext;
		}

		/**
		 * Instantiates a new according exception.
		 *
		 * @param aContext the context
		 * @param aMessage The aMessage describing this exception.
		 * @param aCause The {@link Throwable} ({@link Exception}) causing this
		 *        exception.
		 * @param aErrorCode The error code identifying this exception.
		 */
		public ConfigureRuntimeException( Object aContext, String aMessage, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_context = aContext;
		}

		/**
		 * Instantiates a new according exception.
		 *
		 * @param aContext the context
		 * @param aMessage The aMessage describing this exception.
		 * @param aCause The {@link Throwable} ({@link Exception}) causing this
		 *        exception.
		 */
		public ConfigureRuntimeException( Object aContext, String aMessage, Throwable aCause ) {
			super( aMessage, aCause );
			_context = aContext;
		}

		/**
		 * Instantiates a new according exception.
		 *
		 * @param aContext the context
		 * @param aMessage The aMessage describing this exception.
		 */
		public ConfigureRuntimeException( Object aContext, String aMessage ) {
			super( aMessage );
			_context = aContext;
		}

		/**
		 * Instantiates a new according exception.
		 *
		 * @param aContext the context
		 * @param aCause The {@link Throwable} ({@link Exception}) causing this
		 *        exception.
		 * @param aErrorCode The error code identifying this exception.
		 */
		public ConfigureRuntimeException( Object aContext, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_context = aContext;
		}

		/**
		 * Instantiates a new according exception.
		 *
		 * @param aContext the context
		 * @param aCause The {@link Throwable} ({@link Exception}) causing this
		 *        exception.
		 */
		public ConfigureRuntimeException( Object aContext, Throwable aCause ) {
			super( aCause );
			_context = aContext;
		}

		// /////////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object getContext() {
			return _context;
		}
	}
}
