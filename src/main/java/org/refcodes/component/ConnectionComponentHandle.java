package org.refcodes.component;

/**
 * The {@link ConnectionComponentHandle} manages various
 * {@link ConnectionStatus} states for {@link ConnectionComponent} instances
 * each related to a handle. Operations manipulating on the
 * {@link ConnectionStatus} are invoked by this
 * {@link ConnectionComponentHandle} with a handle identifying the according
 * referenced {@link Component}.
 * <p>
 * The {@link ConnectionComponent} contains the business-logic where as the
 * {@link ConnectionComponentHandle} provides the frame for managing this
 * business-logic. The {@link ConnectionAutomatonHandle} takes care of the
 * correct open/close-cycle applied on a {@link ConnectionComponent}.
 *
 * @param <H> The type of the handles.
 * @param <CON> The connection used to open the referenced instance.
 */
public interface ConnectionComponentHandle<H, CON> extends ConnectionOpenableHandle<H, CON>, ClosableHandle<H> {

	/**
	 * The {@link ConnectionAutomatonHandle} is an automaton managing various
	 * {@link ConnectionStatus} states for {@link ConnectionComponent} instances
	 * each related to a handle. Operations manipulating on the
	 * {@link ConnectionStatus} are invoked by this
	 * {@link ConnectionAutomatonHandle} with a handle identifying the according
	 * referenced {@link ConnectionComponent}. The {@link ConnectionComponent}
	 * contains the business-logic where as the
	 * {@link ConnectionAutomatonHandle} provides the frame for managing this
	 * business-logic. The {@link ConnectionAutomatonHandle} takes care of the
	 * correct life-cycle applied on a {@link ConnectionComponent}.
	 * 
	 * @param <H> The type of the handle.
	 * @param <CON> The connection used to open the referenced instance.
	 */
	public interface ConnectionAutomatonHandle<H, CON> extends ConnectionComponentHandle<H, CON>, ConnectionOpenableHandle<H, CON>, ClosableHandle<H>, ConnectionStatusHandle<H> {}
}
