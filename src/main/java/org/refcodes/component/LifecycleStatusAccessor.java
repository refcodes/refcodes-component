// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * Provides an accessor for a {@link LifecycleStatus} property.
 */
public interface LifecycleStatusAccessor {

	/**
	 * Retrieves the {@link LifecycleStatus} property from the property.
	 * Determines in which {@link LifecycleStatus} status a component is in.
	 * 
	 * @return Returns the {@link LifecycleStatus} property stored by the
	 *         property.
	 */
	LifecycleStatus getLifecycleStatus();

	/**
	 * Provides a mutator for a {@link LifecycleStatus} property.
	 */
	public interface LifecycleStatusMutator {

		/**
		 * Sets the {@link LifecycleStatus} property for the property.
		 * 
		 * @param aLifecycle The {@link LifecycleStatus} property to be stored
		 *        by the property.
		 */
		void setLifecycleStatus( LifecycleStatus aLifecycle );
	}

	/**
	 * Provides a builder method for a {@link LifecycleStatus} property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface LifecycleStatusBuilder<B extends LifecycleStatusBuilder<B>> {

		/**
		 * Sets the {@link LifecycleStatus} for the property.
		 * 
		 * @param aLifecycleStatus The {@link LifecycleStatus} to be stored by
		 *        the property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withLifecycleStatus( LifecycleStatus aLifecycleStatus );
	}

	/**
	 * Provides a {@link LifecycleStatus} property.
	 */
	public interface LifecycleStatusProperty extends LifecycleStatusAccessor, LifecycleStatusMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link LifecycleStatus}
		 * (setter) as of {@link #setLifecycleStatus(LifecycleStatus)} and
		 * returns the very same value (getter).
		 * 
		 * @param aLifecycleStatus The {@link LifecycleStatus} to set (via
		 *        {@link #setLifecycleStatus(LifecycleStatus)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default LifecycleStatus letLifecycleStatus( LifecycleStatus aLifecycleStatus ) {
			setLifecycleStatus( aLifecycleStatus );
			return aLifecycleStatus;
		}
	}
}
