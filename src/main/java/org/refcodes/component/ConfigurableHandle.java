package org.refcodes.component;

import org.refcodes.component.Configurable.ConfigureAutomaton;
import org.refcodes.component.ConfigureException.ConfigureRuntimeException;

/**
 * The {@link ConfigurableHandle} interface defines those methods related to the
 * handle based initialize/configure life-cycle.
 * <p>
 * The handle reference requires the {@link Configurable} interface to be
 * implemented.
 *
 * @param <H> The type of the handle.
 * @param <CTX> The context used to initialize the referenced instance.
 */
public interface ConfigurableHandle<H, CTX> {

	/**
	 * Determines whether the handle reference is configurable by implementing
	 * the {@link Configurable} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException in case the handle is unknown.
	 */
	boolean hasConfigurable( H aHandle );

	/**
	 * Initialize/configure the component identified by the given handle.
	 *
	 * @param aHandle The handle identifying the component.
	 * @param aContext The context to be passed to the implementing instance.
	 * 
	 * @throws ConfigureException in case initializing fails.
	 * @throws UnknownHandleRuntimeException in case the given handle is
	 *         unknown.
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 * @throws IllegalHandleStateChangeRuntimeException Thrown in case a state
	 *         change is not possible due to the current state the referenced
	 *         component is in.
	 */
	void initialize( H aHandle, CTX aContext ) throws ConfigureException;

	/**
	 * Initializes the component by calling {@link #initialize(Object, Object)}
	 * without you to require catching an {@link ConfigureException}.
	 * 
	 * @param aHandle The handle identifying the component.
	 * @param aContext The context to be passed to the implementing instance.
	 * 
	 * @throws ConfigureRuntimeException encapsulates the aCause and is thrown
	 *         upon encountering a {@link ConfigureException} exception
	 */
	default void initializeUnchecked( H aHandle, CTX aContext ) {
		try {
			initialize( aHandle, aContext );
		}
		catch ( ConfigureException e ) {
			throw new ConfigureRuntimeException( aContext, e );
		}
	}

	/**
	 * The {@link ConfigureAutomatonHandle} interface defines those methods
	 * related to the handle based initialize/configure life-cycle. The handle
	 * reference requires the {@link ConfigureAutomaton} interface to be
	 * implemented.
	 * 
	 * @param <H> The type of the handle.
	 * @param <CTX> the context used to initialize the referenced instance.
	 */
	public interface ConfigureAutomatonHandle<H, CTX> extends ConfigurableHandle<H, CTX>, InitializedHandle<H> {

		/**
		 * Determines whether the handle reference is configurable by
		 * implementing the {@link ConfigureAutomaton} interface.
		 * 
		 * @param aHandle The handle to test whether the reference provides the
		 *        according functionality.
		 * 
		 * @return True in case the reference provides the according
		 *         functionality.
		 * 
		 * @throws UnknownHandleRuntimeException in case the handle is unknown.
		 */
		boolean hasConfigureAutomaton( H aHandle );

		/**
		 * Determines whether the component identified by the given handle may
		 * get initialized/configured.
		 *
		 * @param aHandle The handle identifying the component.
		 * @param aContext The context to be passed to the implementing
		 *        instance.
		 * 
		 * @return True if {@link #initialize(Object, Object)} is possible.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isInitalizable( H aHandle, CTX aContext );
	}
}
