// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import java.io.IOException;

import org.refcodes.component.ConnectionComponent.ConnectionAutomaton;
import org.refcodes.component.LifecycleComponent.LifecycleAutomaton;

/**
 * This class implements a {@link ConnectionAutomaton}.
 *
 * @param <CON> The type of the connection to be used.
 */
public class ConnectionAutomatonImpl<CON> implements ConnectionAutomaton<CON> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ConnectionStatus _connectionStatus = ConnectionStatus.NONE;

	protected ConnectionComponent<CON> _connectionComponent = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Empty constructor, such {@link LifecycleAutomaton} cannot do much more
	 * than decline the various {@link LifecycleStatus} states for you.
	 */
	public ConnectionAutomatonImpl() {}

	/**
	 * This constructor uses a {@link LifecycleStatus} for wrapping it inside
	 * the {@link ConnectionAutomatonImpl}, making sure of obeying and guarding
	 * the correct {@link LifecycleStatus}'s order of {@link LifecycleStatus}
	 * states for you.
	 * 
	 * @param aConnectionComponent The component to be guarded regarding the
	 *        correct declination of the {@link LifecycleStatus} states.
	 */
	public ConnectionAutomatonImpl( ConnectionComponent<CON> aConnectionComponent ) {
		_connectionComponent = aConnectionComponent;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized ConnectionStatus getConnectionStatus() {
		return _connectionStatus;
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONVENIANCE METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpenable( CON aConnection ) {
		final boolean isConnectable = ( _connectionStatus == ConnectionStatus.NONE ) || ( isClosed() );
		if ( isConnectable && _connectionComponent != null && _connectionComponent instanceof ConnectionAutomaton<?> ) {
			final ConnectionAutomaton<CON> theContactAutomaton = (ConnectionAutomaton<CON>) _connectionComponent;
			return theContactAutomaton.isOpenable( aConnection );
		}
		return isConnectable;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open( CON aConnection ) throws IOException {
		if ( !isOpenable( aConnection ) ) {
			throw new IOException( "Cannot open as the component is in status <" + _connectionStatus + "> which is not the appropriate status for opening." );
		}
		if ( _connectionComponent != null ) {
			_connectionComponent.open( aConnection );
		}
		_connectionStatus = ConnectionStatus.OPENED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpened() {
		return ( _connectionStatus == ConnectionStatus.OPENED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosable() {
		return ( _connectionStatus == ConnectionStatus.OPENED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosed() {
		return ( _connectionStatus == ConnectionStatus.CLOSED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		if ( _connectionStatus == ConnectionStatus.OPENED ) { // Prevent stack overflow in case sub-type overrides #isOpened()
			try {
				if ( _connectionComponent != null ) {
					_connectionComponent.close();
				}
			}
			catch ( Exception e ) {
				/* ignore */
			}
		}
		_connectionStatus = ConnectionStatus.CLOSED;
	}
}
