// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import java.io.IOException;

import org.refcodes.component.LifecycleComponent.LifecycleAutomaton;
import org.refcodes.component.LinkComponent.LinkAutomaton;

/**
 * This class implements a {@link LifecycleAutomaton}.
 */
public class LinkAutomatonImpl implements LinkAutomaton {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ConnectionStatus _connectionStatus = ConnectionStatus.NONE;

	protected LinkComponent _connectionComponent = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Empty constructor, such {@link LifecycleAutomaton} cannot do much more
	 * than decline the various {@link LifecycleStatus} states for you.
	 */
	public LinkAutomatonImpl() {}

	/**
	 * This constructor uses a {@link LifecycleStatus} for wrapping it inside
	 * the {@link LinkAutomatonImpl}, making sure of obeying and guarding the
	 * correct {@link LifecycleStatus}'s order of {@link LifecycleStatus} states
	 * for you.
	 * 
	 * @param aConnectionComponent The component to be guarded regarding the
	 *        correct declination of the {@link LifecycleStatus} states.
	 */
	public LinkAutomatonImpl( LinkComponent aConnectionComponent ) {
		_connectionComponent = aConnectionComponent;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized ConnectionStatus getConnectionStatus() {
		return _connectionStatus;
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONVENIANCE METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpenable() {
		return ( _connectionStatus == ConnectionStatus.NONE ) || ( isClosed() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		if ( !isOpenable() ) {
			throw new IOException( "Cannot open as the component is in status <" + _connectionStatus + "> which is not the appropriate status for opening." );
		}
		if ( _connectionComponent != null ) {
			_connectionComponent.open();
		}
		_connectionStatus = ConnectionStatus.OPENED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpened() {
		return ( _connectionStatus == ConnectionStatus.OPENED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosable() {
		return ( _connectionStatus == ConnectionStatus.OPENED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosed() {
		return ( _connectionStatus == ConnectionStatus.CLOSED );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		if ( isOpened() ) {
			try {
				if ( _connectionComponent != null ) {
					_connectionComponent.close();
				}
			}
			catch ( Exception e ) {
				/* ignore */
			}
		}
		_connectionStatus = ConnectionStatus.CLOSED;
	}
}
