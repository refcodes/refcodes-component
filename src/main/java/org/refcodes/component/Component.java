// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.mixin.Disposable;

/**
 * Per definition any {@link Component} at least must provide a
 * {@link #destroy()} method. In case a {@link Component} has been destroyed,
 * then invoking any of that {@link Component} instance's methods (except the
 * {@link #destroy()} method) must throw an {@link IllegalStateException} as by
 * definition a once destroyed {@link Component} is in the state of being
 * destroyed which is irreversible.
 * <p>
 * ATTENTION: In case you intend to provide {@link #destroy()} functionality to
 * a plain java class which is not intended to be a mature {@link Component},
 * then please just implement the {@link Disposable} interface from the
 * refcodes-mixin artifact. This semantically underlines your intentions more
 * clearly (not being a {@link Component} and reduces dependencies to the
 * refcodes-component artifact.
 */
public interface Component extends Destroyable {}
