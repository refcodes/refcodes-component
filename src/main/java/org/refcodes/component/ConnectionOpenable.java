// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import java.io.IOException;

import org.refcodes.component.Configurable.ConfigureAutomaton;
import org.refcodes.exception.RuntimeIOException;

/**
 * This mixin might be implemented by a component in order to provide opening
 * connection(s) facilities. The semantics of this interface is very similar to
 * that of the {@link Configurable} interface. To clarify the context regarding
 * connections, the {@link ConnectionOpenable} interface has been introduced.
 * <p>
 * In case a no connection is to be provided to the {@link #open(Object)} method
 * (as it may have been passed via the constructor), you may use the
 * {@link Openable} interface with its {@link Openable#open()} method, which
 * does not require any arguments specifying a connection.
 *
 * @param <CON> The type of the connection to be used.
 */
public interface ConnectionOpenable<CON> {

	/**
	 * Opens the component with the given connection.
	 * 
	 * @param aConnection The connection used for opening the connection.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 */
	void open( CON aConnection ) throws IOException;

	/**
	 * Opens the component by calling {@link #open(Object)} without you to
	 * require catching an {@link IOException}.
	 * 
	 * @param aConnection The connection used for opening the connection.
	 * 
	 * @throws RuntimeIOException encapsulates the aCause and is thrown upon
	 *         encountering a {@link IOException} exception
	 */
	default void openUnchecked( CON aConnection ) {
		try {
			open( aConnection );
		}
		catch ( IOException e ) {
			throw new RuntimeIOException( e );
		}
	}

	/**
	 * To enable the {@link Startable} functionality to be invoked in a builder
	 * chain.
	 *
	 * @param <CON> the generic type
	 * @param <B> The instance to be returned on which to apply succeeding
	 *        builder operations.
	 */
	public interface ConnectionOpenBuilder<CON, B extends ConnectionOpenBuilder<CON, B>> {

		/**
		 * Builder method for the {@link ConnectionOpenable#open(Object)}
		 * method.
		 * 
		 * @param aConnection The connection used for opening the connection.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws IOException Thrown in case opening or accessing an open line
		 *         (connection, junction, link) caused problems.
		 */
		B withOpen( CON aConnection ) throws IOException;

		/**
		 * Opens the component by calling {@link #withOpen(Object)} without you
		 * to require catching an {@link IOException}.
		 * 
		 * @param aConnection The connection used for opening the connection.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws RuntimeIOException encapsulates the aCause and is thrown upon
		 *         encountering a {@link IOException} exception
		 */
		default B withOpenUnchecked( CON aConnection ) {
			try {
				return withOpen( aConnection );
			}
			catch ( IOException e ) {
				throw new RuntimeIOException( e );
			}
		}
	}

	/**
	 * The {@link ConnectionOpenAutomaton} interface defines those methods
	 * related to the opening of connection(s) life-cycle. The semantics of this
	 * interface is very similar to that of the {@link ConfigureAutomaton}
	 * interface. To clarify the context regarding connections, the
	 * {@link ConnectionOpenAutomaton} interface has been introduced.
	 * 
	 * @param <CON> The type of the connection to be used.
	 */
	public interface ConnectionOpenAutomaton<CON> extends ConnectionOpenable<CON>, OpenedAccessor {

		/**
		 * Determines whether the given connection may get opened, if true then
		 * component may open a connection with the given connection via the
		 * {@link #open(Object)} method. Usually no physical connection is
		 * established; usually criteria describing the provided connection are
		 * evaluated; for example the connection is tested against a black list,
		 * a white list or against well-formedness or whether the specified
		 * protocols are supported (in case of a connection being a String URL
		 * beginning with "http://", "ftp://" or similar).
		 * ---------------------------------------------------------------------
		 * CAUTION: Even in case true is returned, the actual opening of a
		 * connection may fail (e.g. due to network failure or authentication
		 * issues).
		 * ---------------------------------------------------------------------
		 * 
		 * @param aConnection The connection for which to determine whether it
		 *        can be used to open a connection.
		 * 
		 * @return True if {@link #open(Object)} is theoretically possible.
		 */
		boolean isOpenable( CON aConnection );
	}
}
