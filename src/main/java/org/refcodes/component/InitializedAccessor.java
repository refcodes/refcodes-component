// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * Provides an accessor for a initialized property.
 */
public interface InitializedAccessor {

	/**
	 * Retrieves the is-initialized property from the initialized property.
	 * Determines whether the component is initialized. A component is
	 * initialized after being initialized as of
	 * {@link Initializable#initialize()} or
	 * {@link Configurable#initialize(Object)}.
	 * 
	 * @return True in case of being initialized (returns the is-initialized
	 *         property stored by the initialized property).
	 */
	boolean isInitialized();

	/**
	 * Provides a mutator for a initialized property.
	 */
	public interface InitializedMutator {

		/**
		 * Sets the is-initialized property for the initialized property.
		 * 
		 * @param isInitialized The initialized property to be stored by the
		 *        initialized property.
		 */
		void setInitialized( boolean isInitialized );
	}

	/**
	 * Provides a initialized property.
	 */
	public interface InitializedProperty extends InitializedAccessor, InitializedMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setInitialized(boolean)} and returns the very same value
		 * (getter).
		 * 
		 * @param isInitialized The boolean to set (via
		 *        {@link #setInitialized(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letInitialized( boolean isInitialized ) {
			setInitialized( isInitialized );
			return isInitialized;
		}
	}
}
