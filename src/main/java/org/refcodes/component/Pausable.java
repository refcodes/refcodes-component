// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.component.PauseException.PauseRuntimeException;

/**
 * This mixin might be implemented by a component in order to provide pause
 * facilities.
 */
public interface Pausable {

	/**
	 * Pauses the component.
	 * 
	 * @throws PauseException in case pausing fails.
	 */
	void pause() throws PauseException;

	/**
	 * Pauses the component by calling {@link #pause()} without you to require
	 * catching an {@link PauseException}.
	 * 
	 * @throws PauseRuntimeException encapsulates the aCause and is thrown upon
	 *         encountering a {@link PauseException} exception
	 */
	default void pauseUnchecked() {
		try {
			pause();
		}
		catch ( PauseException e ) {
			throw new PauseRuntimeException( e );
		}
	}

	/**
	 * The {@link PauseAutomaton} interface defines those methods related to the
	 * pause life-cycle.
	 */
	public interface PauseAutomaton extends Pausable {

		/**
		 * Determines whether the component may get paused.
		 * 
		 * @return True if {@link #pause()} is possible.
		 */
		boolean isPausable();

		/**
		 * Determines whether the component is paused.
		 * 
		 * @return True in case of being paused, else false.
		 */
		boolean isPaused();
	}

	/**
	 * To enable the {@link Pausable} functionality to be invoked in a builder
	 * chain.
	 *
	 * @param <B> The instance to be returned on which to apply succeeding
	 *        builder operations.
	 */
	public interface PauseBuilder<B extends PauseBuilder<B>> {

		/**
		 * Builder method for the {@link Pausable#pause()} method.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws PauseException Thrown in case pausing fails.
		 */
		B withPause() throws PauseException;

		/**
		 * Pauses the component by calling {@link #withPause()} without you to
		 * require catching an {@link PauseException}.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws PauseRuntimeException encapsulates the aCause and is thrown
		 *         upon encountering a {@link PauseException} exception
		 */
		default B pauseUnchecked() {
			try {
				return withPause();
			}
			catch ( PauseException e ) {
				throw new PauseRuntimeException( e );
			}
		}
	}

	/**
	 * See {@link Pausable} without any checked exception being declared.
	 */
	public interface UncheckedPausable extends Pausable {

		/**
		 * {@inheritDoc} Same as {@link Pausable#pause()} without any checked
		 * exception being declared.
		 */
		@Override
		void pause();
	}
}
