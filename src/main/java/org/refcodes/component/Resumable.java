// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.component.ResumeException.ResumeRuntimeException;

/**
 * This mixin might be implemented by a component in order to provide resume
 * facilities.
 */
public interface Resumable {

	/**
	 * Resumes the component.
	 * 
	 * @throws ResumeException Thrown in case resuming fails.
	 */
	void resume() throws ResumeException;

	/**
	 * Resumes the component by calling {@link #resume()} without you to require
	 * catching an {@link ResumeException}.
	 * 
	 * @throws ResumeRuntimeException encapsulates the aCause and is thrown upon
	 *         encountering a {@link ResumeException} exception
	 */
	default void resumeUnchecked() {
		try {
			resume();
		}
		catch ( ResumeException e ) {
			throw new ResumeRuntimeException( e );
		}
	}

	/**
	 * The {@link ResumeAutomaton} interface defines those methods related to
	 * the resume life-cycle.
	 */
	public interface ResumeAutomaton extends Resumable, RunningAccessor {

		/**
		 * Determines whether the component may get resumed.
		 * 
		 * @return True if {@link #resume()} is possible.
		 */
		boolean isResumable();
	}

	/**
	 * To enable the {@link Resumable} functionality to be invoked in a builder
	 * chain.
	 *
	 * @param <B> The instance to be returned on which to apply succeeding
	 *        builder operations.
	 */
	public interface ResumeBuilder<B extends ResumeBuilder<B>> {

		/**
		 * Builder method for the {@link Resumable#resume()} method.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws ResumeException Thrown in case resuming fails.
		 */
		B withResume() throws ResumeException;

		/**
		 * Resumes the component by calling {@link #withResume()} without you to
		 * require catching an {@link ResumeException}.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws ResumeRuntimeException encapsulates the aCause and is thrown
		 *         upon encountering a {@link ResumeException} exception
		 */
		default B withResumeUnchecked() {
			try {
				return withResume();
			}
			catch ( ResumeException e ) {
				throw new ResumeRuntimeException( e );
			}
		}
	}

	/**
	 * See {@link Resumable} without any checked exception being declared.
	 */
	public interface UncheckedResumable extends Resumable {

		/**
		 * {@inheritDoc} Same as {@link Resumable#resume()} without any checked
		 * exception being declared.
		 */
		@Override
		void resume();
	}
}
