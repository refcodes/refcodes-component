// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other close source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import org.refcodes.component.Initializable.InitializeAutomaton;
import org.refcodes.controlflow.RetryTimeout;
import org.refcodes.data.IoRetryCount;
import org.refcodes.data.IoTimeout;
import org.refcodes.exception.RuntimeIOException;

/**
 * This mixin might be implemented by a component in order to provide closing
 * connection(s) facilities.
 */
public interface Closable {

	/**
	 * Closes the component's connection(s). Throws a {@link IOException} as
	 * upon close we may have to do things like flushing buffers which can fail
	 * (and would otherwise fail unhandled or even worse unnoticed).
	 * 
	 * @throws IOException thrown in case closing failed.
	 */
	void close() throws IOException;

	/**
	 * Closes the component by calling {@link #close()} without you to require
	 * catching an {@link IOException}.
	 * 
	 * @throws RuntimeIOException encapsulates the aCause and is thrown upon
	 *         encountering a {@link IOException} exception
	 */
	default void closeUnchecked() {
		try {
			close();
		}
		catch ( IOException e ) {
			throw new RuntimeIOException( e );
		}
	}

	/**
	 * Tries to close the component's connection(s). Ignores any problems which
	 * normally would be reported by the {@link #close()} method with an
	 * according exception. The default implementation tries to invoke a
	 * "flush()" in case the implementing instance implements the
	 * {@link java.io.Flushable} interface before invoking {@link #close()}.
	 * Additionally the default implementation tries {@link IoRetryCount#NORM}
	 * number of times to invoke {@link #close()} till a timeout of
	 * {@link IoTimeout#NORM} is reached. In any case this method will return
	 * quietly without throwing any exception.
	 */
	default void closeQuietly() {
		if ( this instanceof java.io.Flushable ) {
			try {
				( (java.io.Flushable) this ).flush();
			}
			catch ( Exception ignore ) {}
		}
		final RetryTimeout theRetryTimeout = new RetryTimeout( IoTimeout.NORM.getTimeMillis(), IoRetryCount.NORM.getValue() );
		while ( theRetryTimeout.hasNextRetry() ) {
			try {
				close();
				return;
			}
			catch ( IOException ignore ) {}
		}
	}

	/**
	 * Tries to close the component's connection(s) after the given time in
	 * milliseconds. This method doesn't throw an exception as it immediately
	 * returns and closes after the given time expired. In case of an
	 * {@link IOException} exception it will be wrapped in a
	 * {@link RuntimeIOException}. Use (if implemented)
	 * {@link ClosedAccessor#isClosed()} to finally determine whether the close
	 * operation succeeded.
	 * 
	 * @param aCloseMillis The time in milliseconds to pass till
	 *        {@link #close()} is called.
	 */
	default void closeIn( int aCloseMillis ) {
		final Timer theTimer = new Timer();
		theTimer.schedule( new TimerTask() {
			@Override
			public void run() {
				try {
					close();
				}
				catch ( IOException e ) {
					throw new RuntimeIOException( e );
				}
				finally {
					theTimer.cancel();
				}
			}
		}, aCloseMillis );
	}

	/**
	 * To enable the {@link Closable} functionality to be invoked in a builder
	 * chain.
	 *
	 * @param <B> The instance to be returned on which to apply succeeding
	 *        builder operations.
	 */
	public interface CloseBuilder<B extends CloseBuilder<B>> {

		/**
		 * Builder method for the {@link Closable#close()} method.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws IOException in case closing or pre-closing (flushing) fails.
		 */
		B withClose() throws IOException;

		/**
		 * Builder method for the {@link Closable#closeQuietly()} method.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 */
		B withCloseQuietly();

		/**
		 * Builder method for the {@link Closable#closeIn( int )} method.
		 *
		 * @param aCloseMillis The time in milliseconds to pass till
		 *        {@link #close()} is called.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 */
		B withCloseIn( int aCloseMillis );

		/**
		 * Closes the component by calling {@link #withClose()} without you to
		 * require catching an {@link IOException}.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws RuntimeIOException encapsulates the aCause and is thrown upon
		 *         encountering a {@link IOException} exception
		 */
		default B withCloseUnchecked() {
			try {
				return withClose();
			}
			catch ( IOException e ) {
				throw new RuntimeIOException( e );
			}
		}
	}

	/**
	 * The {@link CloseAutomaton} interface defines those methods related to the
	 * closing of connection(s) life-cycle. The semantics of this interface is
	 * very similar to that of the {@link InitializeAutomaton} interface. To
	 * clarify the context regarding connections, the {@link CloseAutomaton}
	 * interface has been introduced.
	 */
	public interface CloseAutomaton extends Closable, ClosedAccessor {

		/**
		 * Determines whether the component's connection(s) may get closed.
		 * 
		 * @return True if {@link #close()} is possible.
		 */
		boolean isClosable();
	}
}
