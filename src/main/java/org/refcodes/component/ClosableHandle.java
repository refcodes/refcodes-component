package org.refcodes.component;

import java.io.IOException;

import org.refcodes.component.ConnectionOpenable.ConnectionOpenAutomaton;
import org.refcodes.exception.RuntimeIOException;

/**
 * The {@link ClosableHandle} interface defines those methods related to the
 * handle based close life-cycle.
 * <p>
 * The handle reference requires the {@link Closable} interface to be
 * implemented.
 *
 * @param <H> The type of the handle.
 */
public interface ClosableHandle<H> {

	/**
	 * Determines whether the handle reference is closable by implementing the
	 * {@link Closable} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException in case the handle is unknown.
	 */
	boolean hasClosable( H aHandle );

	/**
	 * Closes or pre-closes (flush) the component identified by the given
	 * handle. Throws a {@link IOException} as upon close we may have to do
	 * things like flushing buffers which can fail (and would otherwise fail
	 * unhandled or even worse unnoticed).
	 *
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws IOException Thrown in case closing or pre-closing (flushing)
	 *         fails.
	 * @throws UnknownHandleRuntimeException in case the given handle is
	 *         unknown.
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 * @throws IllegalHandleStateChangeRuntimeException Thrown in case a state
	 *         change is not possible due to the current state the referenced
	 *         component is in.
	 */
	void close( H aHandle ) throws IOException;

	/**
	 * Closes the component by calling {@link #close(Object)} without you to
	 * require catching an {@link IOException}.
	 * 
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws RuntimeIOException encapsulates the aCause and is thrown upon
	 *         encountering a {@link IOException} exception
	 */
	default void closeUnchecked( H aHandle ) {
		try {
			close( aHandle );
		}
		catch ( IOException e ) {
			throw new RuntimeIOException( e );
		}
	}

	/**
	 * The {@link CloseAutomatonHandle} interface defines those methods related
	 * to the handle based close life-cycle. The handle reference requires the
	 * {@link ConnectionOpenAutomaton} interface to be implemented.
	 * 
	 * @param <H> The type of the handle.
	 */
	public interface CloseAutomatonHandle<H> extends ClosableHandle<H> {

		/**
		 * Determines whether the handle reference is closable by implementing
		 * the {@link ConnectionOpenAutomaton} interface.
		 * 
		 * @param aHandle The handle to test whether the reference provides the
		 *        according functionality.
		 * 
		 * @return True in case the reference provides the according
		 *         functionality.
		 * 
		 * @throws UnknownHandleRuntimeException in case the handle is unknown.
		 */
		boolean hasCloseAutomaton( H aHandle );

		/**
		 * Determines whether the component identified by the given handle may
		 * get closed/disconnected.
		 *
		 * @param aHandle The handle identifying the component.
		 * 
		 * @return True if {@link #close(Object)} is possible.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isClosable( H aHandle );

		/**
		 * Determines whether the component (its connection) identified by the
		 * given handle is closed (disconnected).
		 *
		 * @param aHandle The handle identifying the component.
		 * 
		 * @return True in case of being closed, else false.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isClosed( H aHandle );
	}
}
