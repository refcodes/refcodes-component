// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.mixin.Disposable;

/**
 * This mixin might be implemented by a {@link Component} in order to provide
 * destroy facilities. No exception is thrown as destroy must work always!
 * <p>
 * The "decompose()" method {@link Decomposable#decompose()} differs from the
 * "destroy()" method {@link Destroyable#destroy()} in that "destroy()" shuts
 * down the component in memory, whereas "decompose()" also tears down external
 * resources such as files or DB schemas. This means that with "decompose()" all
 * external data will be lost, as with "destroy()" external data will be kept
 * (in terms that it makes sense for the actual implementation).
 * <p>
 * In case a {@link Component} has been destroyed, then invoking any of that
 * {@link Component} instance's methods (except the {@link #destroy()} method)
 * must throw an {@link IllegalStateException} as by definition a once destroyed
 * {@link Component} is in the state of being destroyed which is irreversible.
 * <p>
 * ATTENTION: In case you intend to provide {@link #destroy()} functionality to
 * a plain java class which is not intended to be a mature {@link Component},
 * then please just implement the {@link Disposable} interface from the
 * refcodes-mixin artifact. This semantically underlines your intentions more
 * clearly (not being a {@link Component} and reduces dependencies to the
 * refcodes-component artifact.
 */
public interface Destroyable {

	/**
	 * Destroys the component. External resources might stay untouched! This
	 * should always be possible and must not throw any exception. In case a
	 * {@link Component} has been destroyed, then invoking any of that
	 * {@link Component} instance's methods (except the {@link #destroy()}
	 * method) must throw an {@link IllegalStateException} as by definition a
	 * once destroyed {@link Component} is in the state of being destroyed which
	 * is irreversible.
	 * 
	 * @throws DestroyException in case destroying a component caused problems.
	 *         This is a {@link RuntimeException} as destroying a component
	 *         should always succeed and a {@link DestroyException} should only
	 *         be thrown in case of obvious malfunctioning and the philosophy of
	 *         "fail early!" without forcing a method to declare an according
	 *         checked exception! You may omit the {@link DestroyException} in
	 *         the method signature when implementing this method!
	 */
	void destroy();

	/**
	 * The {@link DestroyAutomaton} interface defines those methods related to
	 * the destroy life-cycle.
	 */
	public interface DestroyAutomaton extends Destroyable {

		/**
		 * Determines whether the component may get destroyed.
		 * 
		 * @return True if {@link #destroy()} is possible.
		 */
		boolean isDestroyable();

		/**
		 * Determines whether the component is destroyed. In case of being true,
		 * then invoking any of that {@link Component} instance's methods
		 * (except the {@link #destroy()} method) must throw an
		 * {@link IllegalStateException} as by definition a once destroyed
		 * {@link Component} is in the state of being destroyed which is
		 * irreversible.
		 * 
		 * @return True in case of being destroyed, else false.
		 */
		boolean isDestroyed();
	}
}
