package org.refcodes.component;

/**
 * Provides an accessor for a progress property. Any component which wants to
 * provide a progress information implements this interface. A value of zero
 * ("0") indicates that there was no progress so far, a value of one ("1")
 * indicates that the progress is 100%.
 */
public interface ProgressAccessor {

	/**
	 * The progress can be queried by this method. A value of zero ("0")
	 * indicates that there was no progress so far, a value of one ("1")
	 * indicates that the progress is 100%.
	 * 
	 * @return A value between zero ("0") and one ("1") determining the
	 *         progress.
	 */
	float getProgress();

	/**
	 * Provides a mutator for a progress property. Any component which wants to
	 * provide a progress information implements this interface. A value of zero
	 * ("0") indicates that there was no progress so far, a value of one ("1")
	 * indicates that the progress is 100%.
	 */
	public interface ProgressMutator {

		/**
		 * Sets the progress property. A value of zero ("0") indicates that
		 * there was no progress so far, a value of one ("1") indicates that the
		 * progress is 100%.
		 * 
		 * @param aProgress The progress to be stored by the property.
		 */
		void setProgress( float aProgress );
	}

	/**
	 * Provides a progress property.
	 */
	public interface ProgressProperty extends ProgressAccessor, ProgressMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given float (setter) as of
		 * {@link #setProgress(float)} and returns the very same value (getter).
		 * 
		 * @param aProgress The float to set (via {@link #setProgress(float)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default float letProgress( float aProgress ) {
			setProgress( aProgress );
			return aProgress;
		}
	}
}