// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.component.StartException.StartRuntimeException;

/**
 * This mixin might be implemented by a component in order to provide start
 * facilities.
 */
public interface Startable {

	/**
	 * Starts the component.
	 * 
	 * @throws StartException Thrown in case starting fails.
	 */
	void start() throws StartException;

	/**
	 * Starts the component by calling {@link #start()} without you to require
	 * catching an {@link StartException}.
	 * 
	 * @throws StartRuntimeException encapsulates the aCause and is thrown upon
	 *         encountering a {@link StartException} exception
	 */
	default void startUnchecked() {
		try {
			start();
		}
		catch ( StartException e ) {
			throw new StartRuntimeException( e );
		}
	}

	/**
	 * The {@link StartAutomaton} interface defines those methods related to the
	 * start life-cycle.
	 */
	public interface StartAutomaton extends Startable, RunningAccessor {

		/**
		 * Determines whether the component may get started.
		 * 
		 * @return True if {@link #start()} is possible.
		 */
		boolean isStartable();
	}

	/**
	 * To enable the {@link Startable} functionality to be invoked in a builder
	 * chain.
	 *
	 * @param <B> The instance to be returned on which to apply succeeding
	 *        builder operations.
	 */
	public interface StartBuilder<B extends StartBuilder<B>> {

		/**
		 * Builder method for the {@link Startable#start()} method.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws StartException Thrown in case starting fails.
		 */
		B withStart() throws StartException;

		/**
		 * Starts the component by calling {@link #withStart()} without you to
		 * require catching an {@link StartException}.
		 * 
		 * @return The instance to be returned on which to apply succeeding
		 *         builder operations.
		 * 
		 * @throws StartRuntimeException encapsulates the aCause and is thrown
		 *         upon encountering a {@link StartException} exception
		 */
		default B withStartUnchecked() {
			try {
				return withStart();
			}
			catch ( StartException e ) {
				throw new StartRuntimeException( e );
			}
		}
	}

	/**
	 * See {@link Startable} without any checked exception being declared.
	 */
	public interface UncheckedStartable extends Startable {

		/**
		 * {@inheritDoc} Same as {@link Startable#start()} without any checked
		 * exception being declared.
		 */
		@Override
		void start();
	}
}
