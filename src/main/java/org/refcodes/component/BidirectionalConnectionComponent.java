// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.component.ConnectionComponentHandle.ConnectionAutomatonHandle;

/**
 * A component implementing the {@link BidirectionalConnectionComponent}
 * interface supports a connection. I.e. such a component may be instructed open
 * or close a connection: "open" - "close" In case no connection is to be
 * provided to the {@link #open(Object, Object)} method (as it may have been
 * passed via the constructor), you may use the {@link LinkComponent} interface
 * with its {@link Openable#open()} method, which does not require any arguments
 * specifying a connection.
 *
 * @param <INPUT> The type of the input connection to be used.
 * @param <OUTPUT> The type of the output connection to be used.
 */
public interface BidirectionalConnectionComponent<INPUT, OUTPUT> extends BidirectionalConnectionOpenable<INPUT, OUTPUT>, Closable {

	/**
	 * A system implementing the {@link BidirectionalConnectionAutomaton}
	 * interface supports managing {@link BidirectionalConnectionComponent}
	 * instances and takes care that the open/close statuses are invoked in the
	 * correct order by throwing according exceptions in case the
	 * open/close-cycle is invoked in the wrong order. A
	 * {@link BidirectionalConnectionAutomaton} may be used to wrap a
	 * {@link BidirectionalConnectionComponent} by a
	 * {@link ConnectionAutomatonHandle} for managing
	 * {@link BidirectionalConnectionAutomaton} instances. The
	 * {@link BidirectionalConnectionComponent} contains the business-logic
	 * where as the {@link ConnectionAutomatonHandle} provides the frame for
	 * managing this business-logic. The
	 * {@link BidirectionalConnectionAutomaton} takes care of the correct
	 * open/close-cycle applied on a {@link BidirectionalConnectionComponent}.
	 * 
	 * @param <INPUT> The type of the input connection to be used.
	 * @param <OUTPUT> The type of the output connection to be used.
	 */
	public interface BidirectionalConnectionAutomaton<INPUT, OUTPUT> extends BidirectionalConnectionComponent<INPUT, OUTPUT>, BidirectionalConnectionOpenAutomaton<INPUT, OUTPUT>, CloseAutomaton, ConnectionStatusAccessor {}

}
