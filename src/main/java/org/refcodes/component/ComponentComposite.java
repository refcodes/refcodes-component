// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.mixin.Disposable;
import org.refcodes.mixin.Resetable;

/**
 * Any composite implementation of the refcodes frameworks should implement this
 * {@link ComponentComposite} interface so that any therein contained
 * {@link Component} instances' state change methods are supported by the
 * {@link ComponentComposite} as well and forwarded to the {@link Component}
 * contained in the {@link ComponentComposite} instance.
 * <p>
 * For implementing sequential, threaded or threaded joined invocation of the
 * therein contained {@link Component} instances' state change methods (as of
 * the {@link ExecutionStrategy}), you may use the {@link ComponentUtility},
 * e.g. {@link ComponentUtility#start(ExecutionStrategy, java.util.Collection)})
 * or in more seldom cases the {@link ComponentComposite}.
 * <p>
 * A {@link ComponentComposite} manages a set of {@link Component} instances by
 * forwarding the state change requests to the contained elements in case them
 * elements support the according state change method. In case such a state
 * change is not supported by an therein contained element, then the element is
 * just ignored for that state change request.
 * <p>
 * In case of exceptional state, depending on the used {@link ExecutionStrategy}
 * , either no exception is thrown or the one thrown by the first erroneous
 * {@link Component}.
 * <p>
 * The {@link ComponentComposite} supports these interfaces for the according
 * state change requests, depending on the therein contained {@link Component}'s
 * and them implemented interfaces, the according methods are delegated to all
 * implementing {@link Component}s:
 * <p>
 * {@link Initializable} {@link Startable} {@link Pausable} {@link Resumable}
 * {@link Stoppable} {@link Destroyable} {@link Decomposable} {@link Flushable}
 */
public interface ComponentComposite extends LifecycleComponent, LinkComponent, Flushable, Decomposable, Resetable, Disposable {

	/**
	 * The {@link ExtendedComponentComposite} provides extended functionality to
	 * the {@link ComponentComposite}'s functionality by including extended
	 * {@link Openable} functionality ({@link ConnectionOpenable}) and extends
	 * {@link Initializable} functionality ({@link Configurable}).
	 * 
	 * @param <CTX> the context used to initialize the implementing instance.
	 * @param <CON> The type of the connection to be used.
	 */
	public interface ExtendedComponentComposite<CTX, CON> extends ComponentComposite, ConfigurableLifecycleComponent<CTX>, ConnectionComponent<CON> {}

}
