package org.refcodes.component;

import org.refcodes.component.StopException.StopRuntimeException;
import org.refcodes.component.Stoppable.StopAutomaton;

/**
 * The {@link StoppableHandle} interface defines those methods related to the
 * handle based stop life-cycle.
 * <p>
 * The handle reference requires the {@link Stoppable} interface to be
 * implemented.
 *
 * @param <H> The type of the handle.
 */
public interface StoppableHandle<H> {

	/**
	 * Determines whether the handle reference is stoppable by implementing the
	 * {@link Stoppable} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException in case the handle is unknown.
	 */
	boolean hasStoppable( H aHandle );

	/**
	 * Stops the component identified by the given handle.
	 *
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws StopException in case stopping fails.
	 * @throws UnknownHandleRuntimeException in case the given handle is
	 *         unknown.
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 * @throws IllegalHandleStateChangeRuntimeException Thrown in case a state
	 *         change is not possible due to the current state the referenced
	 *         component is in.
	 */
	void stop( H aHandle ) throws StopException;

	/**
	 * Stops the component by calling {@link #stop(Object)} without you to
	 * require catching an {@link StopException}.
	 * 
	 * @param aHandle The handle identifying the component.
	 * 
	 * @throws StopRuntimeException encapsulates the aCause and is thrown upon
	 *         encountering a {@link StopException} exception
	 */
	default void stopUnchecked( H aHandle ) {
		try {
			stop( aHandle );
		}
		catch ( StopException e ) {
			throw new StopRuntimeException( e );
		}
	}

	/**
	 * The {@link StopAutomatonHandle} interface defines those methods related
	 * to the handle based stop life-cycle. The handle reference requires the
	 * {@link StopAutomaton} interface to be implemented.
	 * 
	 * @param <H> The type of the handle.
	 */
	public interface StopAutomatonHandle<H> extends StoppableHandle<H> {

		/**
		 * Determines whether the handle reference is stoppable by implementing
		 * the {@link StopAutomaton} interface.
		 * 
		 * @param aHandle The handle to test whether the reference provides the
		 *        according functionality.
		 * 
		 * @return True in case the reference provides the according
		 *         functionality.
		 * 
		 * @throws UnknownHandleRuntimeException in case the handle is unknown.
		 */
		boolean hasStopAutomaton( H aHandle );

		/**
		 * Determines whether the component identified by the given handle may
		 * get stopped.
		 *
		 * @param aHandle The handle identifying the component.
		 * 
		 * @return True if {@link #stop(Object)} is possible.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isStoppable( H aHandle );

		/**
		 * Determines whether the component identified by the given handle is
		 * stopped.
		 *
		 * @param aHandle The handle identifying the component.
		 * 
		 * @return True in case of being stopped, else false.
		 * 
		 * @throws UnknownHandleRuntimeException in case the given handle is
		 *         unknown.
		 * @throws UnsupportedHandleOperationRuntimeException in case the
		 *         reference of the handle does not support the requested
		 *         operation.
		 */
		boolean isStopped( H aHandle );
	}
}
