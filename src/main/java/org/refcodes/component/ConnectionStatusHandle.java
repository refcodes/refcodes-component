// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * The Interface ConnectionStatusHandle.
 *
 * @param <H> the generic type
 */
public interface ConnectionStatusHandle<H> {

	/**
	 * Determines whether the handle reference provides a
	 * {@link ConnectionStatus} by implementing the
	 * {@link ConnectionStatusAccessor} interface.
	 * 
	 * @param aHandle The handle to test whether the reference provides the
	 *        according functionality.
	 * 
	 * @return True in case the reference provides the according functionality.
	 * 
	 * @throws UnknownHandleRuntimeException in case the handle is unknown.
	 */
	boolean hasConnectionStatus( H aHandle );

	/**
	 * Retrieves the {@link ConnectionStatus} related to the given handle.
	 * Determines in which {@link ConnectionStatus} status a component related
	 * to the given handle is in.
	 *
	 * @param aHandle The handle for which to retrieve the
	 *        {@link ConnectionStatus}.
	 * 
	 * @return Returns the {@link ConnectionStatus} related to the given handle.
	 * 
	 * @throws UnknownHandleRuntimeException in case the given handle is
	 *         unknown.
	 * @throws UnsupportedHandleOperationRuntimeException in case the reference
	 *         of the handle does not support the requested operation.
	 */
	ConnectionStatus getConnectionStatus( H aHandle );
}
