// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

/**
 * Provides an accessor for a opened property.
 */
public interface OpenedAccessor {

	/**
	 * Retrieves the is-opened property from the opened property. Determines
	 * whether the component's connection is opened. A component's connection is
	 * opened after being opened as of {@link Openable#open()} or
	 * {@link ConnectionOpenable#open(Object)}.
	 * 
	 * @return True in case of being opened (returns the is-opened property
	 *         stored by the opened property).
	 */
	boolean isOpened();

	/**
	 * Provides a mutator for a opened property. Determines whether the
	 * component's connection is opened. A component's connection is opened
	 * after being opened as of {@link Openable#open()} or
	 * {@link ConnectionOpenable#open(Object)}.
	 */
	public interface OpenedMutator {

		/**
		 * Sets the is-opened property for the opened property.
		 * 
		 * @param isOpened The opened property to be stored by the opened
		 *        property.
		 */
		void setOpened( boolean isOpened );
	}

	/**
	 * Provides a opened property.
	 */
	public interface OpenedProperty extends OpenedAccessor, OpenedMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setOpened(boolean)} and returns the very same value (getter).
		 * 
		 * @param aOpened The boolean to set (via {@link #setOpened(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letOpened( boolean aOpened ) {
			setOpened( aOpened );
			return aOpened;
		}
	}
}
