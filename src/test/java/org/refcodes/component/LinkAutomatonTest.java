// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

/**
 * The Class DeviceAutomatonTest.
 */
public class LinkAutomatonTest implements LinkComponent {
	/**
	 * Test sunny day connection.
	 *
	 * @throws IOException the open exception
	 */
	@Test
	public void testSunnyDayConnection() throws IOException {

		// ---------------------------------------------------------------------
		// ZOMBIE:
		// ---------------------------------------------------------------------

		final LinkAutomatonImpl theDeviceAutomaton = new LinkAutomatonImpl( this );
		assertTrue( theDeviceAutomaton.isOpenable() );
		assertFalse( theDeviceAutomaton.isOpened() );
		assertFalse( theDeviceAutomaton.isClosable() );
		assertFalse( theDeviceAutomaton.isClosed() );
		// ---------------------------------------------------------------------
		// OPEN:
		// ---------------------------------------------------------------------
		theDeviceAutomaton.open();
		assertFalse( theDeviceAutomaton.isOpenable() );
		assertTrue( theDeviceAutomaton.isOpened() );
		assertTrue( theDeviceAutomaton.isClosable() );
		assertFalse( theDeviceAutomaton.isClosed() );
		// ---------------------------------------------------------------------
		// CLOSE:
		// ---------------------------------------------------------------------
		theDeviceAutomaton.close();
		assertTrue( theDeviceAutomaton.isOpenable() );
		assertFalse( theDeviceAutomaton.isOpened() );
		assertFalse( theDeviceAutomaton.isClosable() );
		assertTrue( theDeviceAutomaton.isClosed() );
		// ---------------------------------------------------------------------
		// OPEN:
		// ---------------------------------------------------------------------
		theDeviceAutomaton.open();
		assertFalse( theDeviceAutomaton.isOpenable() );
		assertTrue( theDeviceAutomaton.isOpened() );
		assertTrue( theDeviceAutomaton.isClosable() );
		assertFalse( theDeviceAutomaton.isClosed() );
	}

	@Test
	public void testRainyDayConnection() throws IOException {

		// ---------------------------------------------------------------------
		// ZOMBIE:
		// ---------------------------------------------------------------------

		final LinkAutomatonImpl theDeviceAutomaton = new LinkAutomatonImpl( this );
		assertTrue( theDeviceAutomaton.isOpenable() );
		assertFalse( theDeviceAutomaton.isOpened() );
		assertFalse( theDeviceAutomaton.isClosable() );
		assertFalse( theDeviceAutomaton.isClosed() );
		// Annoy the connection status:
		theDeviceAutomaton.open();
		try {
			theDeviceAutomaton.open();
			fail( "Component must not be openable." );
		}
		catch ( IOException e ) {}
		// ---------------------------------------------------------------------
		// CLOSE:
		// ---------------------------------------------------------------------
		theDeviceAutomaton.close();
		theDeviceAutomaton.close();
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// CONNECTION-STATUS:

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Override
	public void open() throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "open" );
		}
	}

	@Override
	public void close() throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "close" );
		}
	}
}