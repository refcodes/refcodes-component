// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.component.ConnectionComponent.ConnectionAutomaton;
import org.refcodes.data.Text;
import org.refcodes.runtime.SystemProperty;

/**
 * The Class ConnectionAutomatonTest.
 */
public class ConnectionAutomatonTest implements ConnectionComponent<String>, ConnectionAutomaton<String> {

	private static final String ACCEPTED = "ACCEPTED";
	private static final String REJECTED = "REJECTED";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testSunnyDayConnection() throws IOException {

		// ---------------------------------------------------------------------
		// ZOMBIE:
		// ---------------------------------------------------------------------

		final ConnectionAutomatonImpl<String> theConnectionAutomaton = new ConnectionAutomatonImpl<>( this );
		assertTrue( theConnectionAutomaton.isOpenable( ACCEPTED ) );
		assertFalse( theConnectionAutomaton.isOpened() );
		assertFalse( theConnectionAutomaton.isClosable() );
		assertFalse( theConnectionAutomaton.isClosed() );
		// ---------------------------------------------------------------------
		// OPEN:
		// ---------------------------------------------------------------------
		theConnectionAutomaton.open( ACCEPTED );
		assertFalse( theConnectionAutomaton.isOpenable( ACCEPTED ) );
		assertTrue( theConnectionAutomaton.isOpened() );
		assertTrue( theConnectionAutomaton.isClosable() );
		assertFalse( theConnectionAutomaton.isClosed() );
		// ---------------------------------------------------------------------
		// CLOSE:
		// ---------------------------------------------------------------------
		theConnectionAutomaton.close();
		assertTrue( theConnectionAutomaton.isOpenable( ACCEPTED ) );
		assertFalse( theConnectionAutomaton.isOpened() );
		assertFalse( theConnectionAutomaton.isClosable() );
		assertTrue( theConnectionAutomaton.isClosed() );
		// ---------------------------------------------------------------------
		// OPEN:
		// ---------------------------------------------------------------------
		theConnectionAutomaton.open( ACCEPTED );
		assertFalse( theConnectionAutomaton.isOpenable( ACCEPTED ) );
		assertTrue( theConnectionAutomaton.isOpened() );
		assertTrue( theConnectionAutomaton.isClosable() );
		assertFalse( theConnectionAutomaton.isClosed() );
	}

	@Test
	public void testRainyDayConnection() throws IOException {

		// ---------------------------------------------------------------------
		// ZOMBIE:
		// ---------------------------------------------------------------------

		final ConnectionAutomatonImpl<String> theConnectionAutomaton = new ConnectionAutomatonImpl<>( this );
		assertTrue( theConnectionAutomaton.isOpenable( ACCEPTED ) );
		assertFalse( theConnectionAutomaton.isOpened() );
		assertFalse( theConnectionAutomaton.isClosable() );
		assertFalse( theConnectionAutomaton.isClosed() );
		// Annoy the connection status:
		theConnectionAutomaton.open( ACCEPTED );
		try {
			theConnectionAutomaton.open( ACCEPTED );
			fail( "Component must not be openable." );
		}
		catch ( IOException e ) {}
		// ---------------------------------------------------------------------
		// CLOSE:
		// ---------------------------------------------------------------------
		theConnectionAutomaton.close();
		theConnectionAutomaton.close();
	}

	@Test
	public void tesBadConnection() throws IOException {

		// ---------------------------------------------------------------------
		// ZOMBIE:
		// ---------------------------------------------------------------------

		final ConnectionAutomatonImpl<String> theConnectionAutomaton = new ConnectionAutomatonImpl<>( this );
		assertFalse( theConnectionAutomaton.isOpenable( REJECTED ) );
		assertFalse( theConnectionAutomaton.isOpened() );
		assertFalse( theConnectionAutomaton.isClosable() );
		assertFalse( theConnectionAutomaton.isClosed() );
		try {
			theConnectionAutomaton.open( REJECTED );
			fail( "Component must not be openable." );
		}
		catch ( IOException e ) {}
		// ---------------------------------------------------------------------
		// CLOSE:
		// ---------------------------------------------------------------------
		theConnectionAutomaton.close();
		theConnectionAutomaton.close();
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONNECTION-STATUS:
	// /////////////////////////////////////////////////////////////////////////

	@Override
	public void open( String aConnection ) throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "open" );
		}
		if ( !isOpenable( aConnection ) ) {}
	}

	@Override
	public void close() throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "close" );
		}
	}

	@Override
	public boolean isOpenable( String aConnection ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "isOpenable" );
		}
		return aConnection.equals( ACCEPTED );
	}

	@Override
	public boolean isOpened() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	@Override
	public boolean isClosable() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	@Override
	public boolean isClosed() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	@Override
	public ConnectionStatus getConnectionStatus() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}
}