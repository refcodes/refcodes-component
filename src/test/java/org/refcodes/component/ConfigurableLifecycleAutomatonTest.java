// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.component.ConfigurableLifecycleComponent.ConfigurableLifecycleAutomaton;
import org.refcodes.data.Text;
import org.refcodes.runtime.SystemProperty;

/**
 * The Class ConfigurableLifecycleAutomatonTest.
 */
public class ConfigurableLifecycleAutomatonTest implements ConfigurableLifecycleComponent<String>, ConfigurableLifecycleAutomaton<String> {

	private static final String ACCEPTED = "ACCEPTED";
	private static final String REJECTED = "REJECTED";

	/**
	 * Test sunny day life cycle.
	 *
	 * @throws ConfigureException the configure exception
	 * @throws StartException the start exception
	 * @throws PauseException the pause exception
	 * @throws ResumeException the resume exception
	 * @throws StopException the stop exception
	 */
	@Test
	public void testSunnyDayLifecycle() throws InitializeException, StartException, PauseException, ResumeException, StopException {

		// ---------------------------------------------------------------------
		// ZOMBIE:
		// ---------------------------------------------------------------------

		final ConfigurableLifecycleAutomatonImpl<String> theConfigurableLifecycleAutomaton = new ConfigurableLifecycleAutomatonImpl<>( this );
		assertTrue( theConfigurableLifecycleAutomaton.isInitalizable( ACCEPTED ) );
		assertFalse( theConfigurableLifecycleAutomaton.isInitialized() );
		assertFalse( theConfigurableLifecycleAutomaton.isStartable() );
		assertFalse( theConfigurableLifecycleAutomaton.isRunning() );
		assertFalse( theConfigurableLifecycleAutomaton.isPausable() );
		assertFalse( theConfigurableLifecycleAutomaton.isPaused() );
		assertFalse( theConfigurableLifecycleAutomaton.isResumable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStoppable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStopped() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyable() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyed() );

		// ---------------------------------------------------------------------
		// INITIALIZED:
		// ---------------------------------------------------------------------

		theConfigurableLifecycleAutomaton.initialize( ACCEPTED );
		assertFalse( theConfigurableLifecycleAutomaton.isInitalizable( ACCEPTED ) );
		assertTrue( theConfigurableLifecycleAutomaton.isInitialized() );
		assertTrue( theConfigurableLifecycleAutomaton.isStartable() );
		assertFalse( theConfigurableLifecycleAutomaton.isRunning() );
		assertFalse( theConfigurableLifecycleAutomaton.isPausable() );
		assertFalse( theConfigurableLifecycleAutomaton.isPaused() );
		assertFalse( theConfigurableLifecycleAutomaton.isResumable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStoppable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStopped() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyable() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyed() );

		// ---------------------------------------------------------------------
		// STARTED:
		// ---------------------------------------------------------------------

		theConfigurableLifecycleAutomaton.start();
		assertFalse( theConfigurableLifecycleAutomaton.isInitalizable( ACCEPTED ) );
		assertFalse( theConfigurableLifecycleAutomaton.isInitialized() );
		assertFalse( theConfigurableLifecycleAutomaton.isStartable() );
		assertTrue( theConfigurableLifecycleAutomaton.isRunning() );
		assertTrue( theConfigurableLifecycleAutomaton.isPausable() );
		assertFalse( theConfigurableLifecycleAutomaton.isPaused() );
		assertFalse( theConfigurableLifecycleAutomaton.isResumable() );
		assertTrue( theConfigurableLifecycleAutomaton.isStoppable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStopped() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyable() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyed() );

		// ---------------------------------------------------------------------
		// PAUSED:
		// ---------------------------------------------------------------------

		theConfigurableLifecycleAutomaton.pause();
		assertFalse( theConfigurableLifecycleAutomaton.isInitalizable( ACCEPTED ) );
		assertFalse( theConfigurableLifecycleAutomaton.isInitialized() );
		assertFalse( theConfigurableLifecycleAutomaton.isStartable() );
		assertFalse( theConfigurableLifecycleAutomaton.isRunning() );
		assertFalse( theConfigurableLifecycleAutomaton.isPausable() );
		assertTrue( theConfigurableLifecycleAutomaton.isPaused() );
		assertTrue( theConfigurableLifecycleAutomaton.isResumable() );
		assertTrue( theConfigurableLifecycleAutomaton.isStoppable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStopped() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyable() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyed() );

		// ---------------------------------------------------------------------
		// RESUMED:
		// ---------------------------------------------------------------------

		theConfigurableLifecycleAutomaton.resume();
		assertFalse( theConfigurableLifecycleAutomaton.isInitalizable( ACCEPTED ) );
		assertFalse( theConfigurableLifecycleAutomaton.isInitialized() );
		assertFalse( theConfigurableLifecycleAutomaton.isStartable() );
		assertTrue( theConfigurableLifecycleAutomaton.isRunning() );
		assertTrue( theConfigurableLifecycleAutomaton.isPausable() );
		assertFalse( theConfigurableLifecycleAutomaton.isPaused() );
		assertFalse( theConfigurableLifecycleAutomaton.isResumable() );
		assertTrue( theConfigurableLifecycleAutomaton.isStoppable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStopped() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyable() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyed() );

		// ---------------------------------------------------------------------
		// STOPPED:
		// ---------------------------------------------------------------------

		theConfigurableLifecycleAutomaton.stop();
		assertFalse( theConfigurableLifecycleAutomaton.isInitalizable( ACCEPTED ) );
		assertFalse( theConfigurableLifecycleAutomaton.isInitialized() );
		assertTrue( theConfigurableLifecycleAutomaton.isStartable() );
		assertFalse( theConfigurableLifecycleAutomaton.isRunning() );
		assertFalse( theConfigurableLifecycleAutomaton.isPausable() );
		assertFalse( theConfigurableLifecycleAutomaton.isPaused() );
		assertFalse( theConfigurableLifecycleAutomaton.isResumable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStoppable() );
		assertTrue( theConfigurableLifecycleAutomaton.isStopped() );
		assertTrue( theConfigurableLifecycleAutomaton.isDestroyable() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyed() );

		// ---------------------------------------------------------------------
		// DESTROYED:
		// ---------------------------------------------------------------------

		theConfigurableLifecycleAutomaton.destroy();
		assertTrue( theConfigurableLifecycleAutomaton.isInitalizable( ACCEPTED ) );
		assertFalse( theConfigurableLifecycleAutomaton.isInitialized() );
		assertFalse( theConfigurableLifecycleAutomaton.isStartable() );
		assertFalse( theConfigurableLifecycleAutomaton.isRunning() );
		assertFalse( theConfigurableLifecycleAutomaton.isPausable() );
		assertFalse( theConfigurableLifecycleAutomaton.isPaused() );
		assertFalse( theConfigurableLifecycleAutomaton.isResumable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStoppable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStopped() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyable() );
		assertTrue( theConfigurableLifecycleAutomaton.isDestroyed() );
	}

	@Test
	public void testRainyDayLifecycle() throws InitializeException, StartException, PauseException, ResumeException, StopException {

		// ---------------------------------------------------------------------
		// ZOMBIE:
		// ---------------------------------------------------------------------

		final ConfigurableLifecycleAutomatonImpl<String> theConfigurableLifecycleAutomaton = new ConfigurableLifecycleAutomatonImpl<>( this );
		assertTrue( theConfigurableLifecycleAutomaton.isInitalizable( ACCEPTED ) );
		assertFalse( theConfigurableLifecycleAutomaton.isInitialized() );
		assertFalse( theConfigurableLifecycleAutomaton.isStartable() );
		assertFalse( theConfigurableLifecycleAutomaton.isRunning() );
		assertFalse( theConfigurableLifecycleAutomaton.isPausable() );
		assertFalse( theConfigurableLifecycleAutomaton.isPaused() );
		assertFalse( theConfigurableLifecycleAutomaton.isResumable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStoppable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStopped() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyable() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyed() );
		// Annoy the life-cycle:
		try {
			theConfigurableLifecycleAutomaton.start();
			fail( "Component must not be startable." );
		}
		catch ( StartException e ) {}
		try {
			theConfigurableLifecycleAutomaton.pause();
			fail( "Component must not be pausable." );
		}
		catch ( PauseException e ) {}
		try {
			theConfigurableLifecycleAutomaton.resume();
			fail( "Component must not be resumable." );
		}
		catch ( ResumeException e ) {}
		try {
			theConfigurableLifecycleAutomaton.stop();
			fail( "Component must not be stoppable." );
		}
		catch ( StopException e ) {}

		// ---------------------------------------------------------------------
		// INITIALIZED:
		// ---------------------------------------------------------------------

		theConfigurableLifecycleAutomaton.initialize( ACCEPTED );
		assertFalse( theConfigurableLifecycleAutomaton.isInitalizable( ACCEPTED ) );
		assertTrue( theConfigurableLifecycleAutomaton.isInitialized() );
		assertTrue( theConfigurableLifecycleAutomaton.isStartable() );
		assertFalse( theConfigurableLifecycleAutomaton.isRunning() );
		assertFalse( theConfigurableLifecycleAutomaton.isPausable() );
		assertFalse( theConfigurableLifecycleAutomaton.isPaused() );
		assertFalse( theConfigurableLifecycleAutomaton.isResumable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStoppable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStopped() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyable() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyed() );
		// Annoy the life-cycle:
		try {
			theConfigurableLifecycleAutomaton.initialize( ACCEPTED );
			fail( "Component must not be initializeable." );
		}
		catch ( ConfigureException e ) {}
		try {
			theConfigurableLifecycleAutomaton.pause();
			fail( "Component must not be pausable." );
		}
		catch ( PauseException e ) {}
		try {
			theConfigurableLifecycleAutomaton.resume();
			fail( "Component must not be resumable." );
		}
		catch ( ResumeException e ) {}
		try {
			theConfigurableLifecycleAutomaton.stop();
			fail( "Component must not be stoppable." );
		}
		catch ( StopException e ) {}

		// ---------------------------------------------------------------------
		// STARTED:
		// ---------------------------------------------------------------------

		theConfigurableLifecycleAutomaton.start();
		assertFalse( theConfigurableLifecycleAutomaton.isInitalizable( ACCEPTED ) );
		assertFalse( theConfigurableLifecycleAutomaton.isInitialized() );
		assertFalse( theConfigurableLifecycleAutomaton.isStartable() );
		assertTrue( theConfigurableLifecycleAutomaton.isRunning() );
		assertTrue( theConfigurableLifecycleAutomaton.isPausable() );
		assertFalse( theConfigurableLifecycleAutomaton.isPaused() );
		assertFalse( theConfigurableLifecycleAutomaton.isResumable() );
		assertTrue( theConfigurableLifecycleAutomaton.isStoppable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStopped() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyable() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyed() );
		// Annoy the life-cycle:
		try {
			theConfigurableLifecycleAutomaton.initialize( ACCEPTED );
			fail( "Component must not be initializable." );
		}
		catch ( ConfigureException e ) {}
		try {
			theConfigurableLifecycleAutomaton.start();
			fail( "Component must not be startable." );
		}
		catch ( StartException e ) {}
		try {
			theConfigurableLifecycleAutomaton.resume();
			fail( "Component must not be resumable." );
		}
		catch ( ResumeException e ) {}

		// ---------------------------------------------------------------------
		// PAUSED:
		// ---------------------------------------------------------------------

		theConfigurableLifecycleAutomaton.pause();
		assertFalse( theConfigurableLifecycleAutomaton.isInitalizable( ACCEPTED ) );
		assertFalse( theConfigurableLifecycleAutomaton.isInitialized() );
		assertFalse( theConfigurableLifecycleAutomaton.isStartable() );
		assertFalse( theConfigurableLifecycleAutomaton.isRunning() );
		assertFalse( theConfigurableLifecycleAutomaton.isPausable() );
		assertTrue( theConfigurableLifecycleAutomaton.isPaused() );
		assertTrue( theConfigurableLifecycleAutomaton.isResumable() );
		assertTrue( theConfigurableLifecycleAutomaton.isStoppable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStopped() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyable() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyed() );
		// Annoy the life-cycle:
		try {
			theConfigurableLifecycleAutomaton.initialize( ACCEPTED );
			fail( "Component must not be initializeable." );
		}
		catch ( ConfigureException e ) {}
		try {
			theConfigurableLifecycleAutomaton.start();
			fail( "Component must not be startable." );
		}
		catch ( StartException e ) {}
		try {
			theConfigurableLifecycleAutomaton.pause();
			fail( "Component must not be pausable." );
		}
		catch ( PauseException e ) {}

		// ---------------------------------------------------------------------
		// RESUMED:
		// ---------------------------------------------------------------------

		theConfigurableLifecycleAutomaton.resume();
		assertFalse( theConfigurableLifecycleAutomaton.isInitalizable( ACCEPTED ) );
		assertFalse( theConfigurableLifecycleAutomaton.isInitialized() );
		assertFalse( theConfigurableLifecycleAutomaton.isStartable() );
		assertTrue( theConfigurableLifecycleAutomaton.isRunning() );
		assertTrue( theConfigurableLifecycleAutomaton.isPausable() );
		assertFalse( theConfigurableLifecycleAutomaton.isPaused() );
		assertFalse( theConfigurableLifecycleAutomaton.isResumable() );
		assertTrue( theConfigurableLifecycleAutomaton.isStoppable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStopped() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyable() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyed() );
		// Annoy the life-cycle:
		try {
			theConfigurableLifecycleAutomaton.initialize( ACCEPTED );
			fail( "Component must not be initializeable." );
		}
		catch ( ConfigureException e ) {}
		try {
			theConfigurableLifecycleAutomaton.start();
			fail( "Component must not be startable." );
		}
		catch ( StartException e ) {}
		try {
			theConfigurableLifecycleAutomaton.resume();
			fail( "Component must not be resumable." );
		}
		catch ( ResumeException e ) {}

		// ---------------------------------------------------------------------
		// STOPPED:
		// ---------------------------------------------------------------------

		theConfigurableLifecycleAutomaton.stop();
		assertFalse( theConfigurableLifecycleAutomaton.isInitalizable( ACCEPTED ) );
		assertFalse( theConfigurableLifecycleAutomaton.isInitialized() );
		assertTrue( theConfigurableLifecycleAutomaton.isStartable() );
		assertFalse( theConfigurableLifecycleAutomaton.isRunning() );
		assertFalse( theConfigurableLifecycleAutomaton.isPausable() );
		assertFalse( theConfigurableLifecycleAutomaton.isPaused() );
		assertFalse( theConfigurableLifecycleAutomaton.isResumable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStoppable() );
		assertTrue( theConfigurableLifecycleAutomaton.isStopped() );
		assertTrue( theConfigurableLifecycleAutomaton.isDestroyable() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyed() );
		// Annoy the life-cycle:
		try {
			theConfigurableLifecycleAutomaton.initialize( ACCEPTED );
			fail( "Component must not be initializeable." );
		}
		catch ( ConfigureException e ) {}
		try {
			theConfigurableLifecycleAutomaton.pause();
			fail( "Component must not be pausable." );
		}
		catch ( PauseException e ) {}
		try {
			theConfigurableLifecycleAutomaton.resume();
			fail( "Component must not be resumable." );
		}
		catch ( ResumeException e ) {}
		try {
			theConfigurableLifecycleAutomaton.stop();
			fail( "Component must not be stoppable." );
		}
		catch ( StopException e ) {}

		// ---------------------------------------------------------------------
		// DESTROYED:
		// ---------------------------------------------------------------------

		theConfigurableLifecycleAutomaton.destroy();
		assertTrue( theConfigurableLifecycleAutomaton.isInitalizable( ACCEPTED ) );
		assertFalse( theConfigurableLifecycleAutomaton.isInitialized() );
		assertFalse( theConfigurableLifecycleAutomaton.isStartable() );
		assertFalse( theConfigurableLifecycleAutomaton.isRunning() );
		assertFalse( theConfigurableLifecycleAutomaton.isPausable() );
		assertFalse( theConfigurableLifecycleAutomaton.isPaused() );
		assertFalse( theConfigurableLifecycleAutomaton.isResumable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStoppable() );
		assertFalse( theConfigurableLifecycleAutomaton.isStopped() );
		assertFalse( theConfigurableLifecycleAutomaton.isDestroyable() );
		assertTrue( theConfigurableLifecycleAutomaton.isDestroyed() );
		// Annoy the life-cycle:
		try {
			theConfigurableLifecycleAutomaton.start();
			fail( "Component must not be startable." );
		}
		catch ( StartException e ) {}
		try {
			theConfigurableLifecycleAutomaton.pause();
			fail( "Component must not be pausable." );
		}
		catch ( PauseException e ) {}
		try {
			theConfigurableLifecycleAutomaton.resume();
			fail( "Component must not be resumable." );
		}
		catch ( ResumeException e ) {}
		try {
			theConfigurableLifecycleAutomaton.stop();
			fail( "Component must not be stoppable." );
		}
		catch ( StopException e ) {}
	}

	@Test
	public void testBadContext() throws InitializeException, StartException, PauseException, ResumeException, StopException {

		// ---------------------------------------------------------------------
		// ZOMBIE:
		// ---------------------------------------------------------------------

		final ConfigurableLifecycleAutomatonImpl<String> theConfigurableLifecycleAutomaton = new ConfigurableLifecycleAutomatonImpl<>( this );
		assertFalse( theConfigurableLifecycleAutomaton.isInitalizable( REJECTED ) );
		try {
			theConfigurableLifecycleAutomaton.initialize( REJECTED );
			fail( "Component must not be initializable." );
		}
		catch ( ConfigureException e ) {}
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFE-CYCLE:
	// /////////////////////////////////////////////////////////////////////////

	@Override
	public void stop() throws StopException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "stop" );
		}
	}

	@Override
	public void destroy() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "destroy" );
		}
	}

	@Override
	public void start() throws StartException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "start" );
		}
	}

	@Override
	public void pause() throws PauseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "pause" );
		}
	}

	@Override
	public void resume() throws ResumeException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "resume" );
		}
	}

	@Override
	public void initialize( String aContext ) throws ConfigureException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "initialize" );
		}
	}

	@Override
	public boolean isInitalizable( String aContext ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "isInitializable" );
		}
		return aContext.equals( ACCEPTED );
	}

	@Override
	public boolean isInitialized() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	@Override
	public boolean isStartable() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	@Override
	public boolean isRunning() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	@Override
	public boolean isPausable() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	@Override
	public boolean isPaused() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	@Override
	public boolean isResumable() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	@Override
	public boolean isStoppable() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	@Override
	public boolean isStopped() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	@Override
	public boolean isDestroyable() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	@Override
	public boolean isDestroyed() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	@Override
	public LifecycleStatus getLifecycleStatus() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}
}